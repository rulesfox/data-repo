Los jueces deben ser árbitros neutrales y quienes hacen cumplir las políticas y reglas.

> _Este es probablemente el concepto más importante de este documento, los jueces deben ser neutrales. Tiene que ser inconcebible que un juez esté a favor de un jugador porque sean amigos, o porque de alguna forma el juez se lleve mejor con un jugador que con el otro. Ser imparcial es un concepto fundamental del_ [_Código de Conducta de Jueces_](http://blogs.magicjudges.org/conduct/magic-judge-code/)_. Los jueces son vistos con respeto, en gran parte, porque son neutrales y hacen cumplir las políticas de manera ecuánime._

Un juez no debería intervenir en un juego a menos que crea que ocurrió una violación de reglas, un jugador con una pregunta o preocupación solicita asistencia, o el juez desea evitar que una situación empeore.

> _Los jueces están para ayudar a los jugadores. Nuestros servicios son requeridos cuando una regla ha sido violada, un jugador necesita ayuda, o hay una situación delicada como una discusión y es necesario calmar a los jugadores. Cuando su ayuda no sea requerida, los jueces no deben interferir en las partidas. Esto implica no hacer comentarios sobre acciones o decisiones del juego, no arriesgarse a dar consejos, y no interrumpir la concentración de los jugadores. Dejemos que los jugadores jueguen. Esto no implica que debas ser un robot. Siempre puedes hablar con los jugadores o hacer bromas con ellos mientras no se interrumpa el juego en curso._

Los jueces no evitan que sucedan los errores de juego, sino que lidian con errores que ocurrieron, penalizan a aquellos que violan las reglas o políticas y promueven el juego limpio y la conducta deportiva con el ejemplo y la diplomacia.

> _Como en muchos otros deportes, los jueces no previenen que ocurran errores. Sin embargo, una vez que se cometió una infracción de juego, los jueces intervienen y aplican las correcciones y penalizaciones necesarias. Los jugadores no pueden depender de un juez para prevenir que ocurran acciones ilegales, ya que los jueces no pueden predecir el futuro y las acciones durante un juego ocurren rápidamente. En la gran mayoría de los casos, arreglar una infracción luego de que haya ocurrido restaura el flujo del juego. Esta política también se mantiene cuando un juez observa un juego al final de una ronda, o durante un Top 8._
> 
> _Es importante también que los jueces den un buen ejemplo de conducta. Nuestras actitudes y acciones tienen un profundo impacto en el tono del evento. La gente debe ver en nosotros el tipo de conductas que queremos ver en nuestros torneos._
> 
> 

Los jueces pueden intervenir para prevenir o evitar errores que ocurren fuera de un juego.

> _A pesar de que es casi imposible predecir cuándo ocurrirá una infracción en un juego, a veces es posible ver que una infracción fuera del juego está por ocurrir. En dichos casos, los jueces deben intervenir y prevenir que las infracciones ocurran. El “pueden” en la oración no es “al juez se le permite elegir si intervenir o no”, sino más bien “La IPG/MTR permite a los jueces intervenir”. Esto marca la importancia de dar “atención al cliente” (customer service) y señala de manera concisa que los jueces no tienen una elección acerca de si deben intervenir o no para prevenir este tipo de errores. Fuera de un juego, los jueces siempre deben intervenir para evitar infracciones, pero también hay que aceptar que no siempre podemos predecir cuándo una infracción está por ocurrir._
> 
> _Aquí algunos ejemplos:_
> 
> *   _­Un juez observa a un jugador barajar su mazo al final del primer juego, y se percata de que una criatura previamente exiliada está en la mesa y el jugador olvidó barajar dicha carta en su mazo; el juez interviene y avisa al jugador que se está olvidando de barajar una carta._
> *   _­En un torneo de Mazo Sellado, una jugadora entrega una Lista de Mazo a un juez, quien se percata de que ella olvidó de anotar sus tierras básicas; el juez le pide que anote las tierras básicas que está usando._
> *   _­Justo antes del principio de una ronda, un jugador entrega a un juez una carta (por ejemplo, un Pacifismo – [Pacifism](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Pacifism&width=223&height=310)), que pertenece al último oponente contra el cual jugó; los jueces hacen un esfuerzo por encontrar donde está jugando el dueño de la carta, así pueden devolverla antes de que el juego empiece con un mazo ilegal._
> *   _Antes de empezar un evento, un juez reconoce que una carta tiene el arte alterado de forma un tanto cuestionable. El juez le recuerda al jugador que el_ [_Juez Principal_](https://blogs.magicjudges.org/rules/mtr1-7-es/) _necesita aprobar dichas alteraciones antes de que empiece el evento._
> 
> 

Conocer la historia o la habilidad de un jugador no cambia la infracción, pero puede ser considerado durante una investigación.

> _No cambiamos una infracción basándonos en la percepción que tenemos de un jugador. Una Violación de Regla de Juego conlleva una Advertencia independientemente de que el jugador sea nuevo o un profesional veterano. Una vez determinada la infracción, aplicamos la penalización sin ningún tipo de parcialidad preconcebida. Resolvemos la infracción de forma imparcial, aunque el involucrado sea un jugador con una reputación cuestionable o un juez nivel 3 jugando en el evento. Una vez que se reconoce cuál es la infracción, la identidad del jugador no tiene relevancia. Sin embargo, para determinar qué infracción se cometió, el historial de un jugador puede influenciar la investigación. Por ejemplo, que un jugador novato no entienda cómo funciona la mecánica de Arrollar es mucho más creíble que un jugador experimentado, con el cual ya tuvimos discusiones previas acerca de cómo funciona dicha mecánica. Siempre está la posibilidad de que el error sea legítimo, pero las preguntas que se hagan en la investigación van a ser influenciadas por este conocimiento._

El propósito de una penalización es educar al jugador para que no repita errores similares en el futuro.

> _Las penalizaciones no existen para dar la oportunidad a jueces sádicos de infringir dolor a jugadores indefensos. Las penalizaciones existen para reducir la posibilidad de que dicho error fuera a repetirse. Es menos probable que un jugador que recibe una penalización comenta el mismo error en el futuro. En general la idea es que sea algo tangible para reforzar la lección “Perdí un juego por este error, no me gustaría perder otro juego por algo que puedo evitar fácilmente, la próxima vez voy a contar que haya 60 cartas cuando escriba mi Lista de Mazo”. El propósito primario de una penalización no es dar seguimiento a los errores de un jugador; aunque es una consecuencia muy conveniente y útil, la finalidad es educar al jugador._

Esto se logra a través de la explicación de dónde fueron violadas las reglas o políticas y una penalización para reforzar la educación.

> _En la oración previa estamos dando énfasis a la educación. La penalización no logra esto por sí sola. También debemos explicar (brevemente) qué es lo que el jugador hizo mal. De otra forma este puede no entender completamente cuál fue el error._

Las penalizaciones también son una disuasión y educación para todos los otros jugadores en el evento y también sirven para llevar registro del comportamiento de un jugador en el tiempo.

> _También debemos tener en cuenta que no hace falta recibir una penalización para reconocer que no es una situación deseable. Si un amigo nuestro pierde un juego por una penalización, lo más seguro es que nosotros no queramos que nos ocurra lo mismo. Aprendemos de los errores de nuestro amigo, así como de los nuestros._
> 
> _Existe un archivo (privado) de todas las penalizaciones, así como existe un archivo (público) de los todos los resultados de las partidas. Este archivo se vuelve útil en caso de que un jugador cometa un número elevado de infracciones del mismo tipo. Si un jugador recibe una Advertencia por “Mirar Cartas Adicionales – reveló cartas de su oponente mientras barajaba el mazo de su oponente antes del comienzo de la partida” veinte veces en veinte torneos consecutivos, bueno, ¿no crees que lo está haciendo a propósito y que lo hace una sola vez por torneo porque “la primera vez es sólo una Advertencia”?_
> 
> 

Si una violación menor es corregida rápidamente por los jugadores y ambos están satisfechos, un juez no necesita intervenir.

> _Los jueces deben ser percibidos como una ayuda y un beneficio para los jugadores, y existen muchos errores pequeños/menores que los jugadores cometen y pueden corregir por cuenta propia durante el desarrollo de un juego sin la necesidad de un juez. Si el error es pequeño, los jugadores pueden resolverlo por sí mismos, y ambos están contentos con el resultado, los jueces no necesitan intervenir en dicha partida._

Si los jugadores están jugando de manera clara para ellos, pero que podría confundir a un observador externo, los jueces deberían solicitar a los jugadores que aclaren la situación, pero no asignar ninguna infracción o penalización.

> _Los jugadores toman ciertos atajos, usan piedritas para representar distintos objetos, o fichas incorrectas para representar criaturas. Si bien estas cosas pueden ser claras para los jugadores, no necesariamente lo son para un observador (jueces incluidos). Si los jugadores entienden perfectamente que está ocurriendo en el juego, y todo está bien, no hay nada que penalizar. Simplemente se les pide que jueguen de una forma más clara para todos. Muchas veces un espectador viene a nosotros con problemas que, en realidad, no son problemas. Por esta razón debemos incitar a los jugadores a que sean claros entre ellos durante una partida y asegurarnos que sus acciones sean claras para cualquier persona observando el juego._

En ambas situaciones, el juez debe asegurarse que el juego progresa normalmente.

> _Si los jugadores arreglan un pequeño error por cuenta propia de forma clara para ellos, pero no para un observador, debemos mantenernos cerca y observar la partida para asegurarnos que no ocurra nada extraño, por ejemplo, un jugador tomando ventaja de la confusión, o que el error realmente no se haya solucionado._

Las violaciones más significativas se atienden primero identificando qué infracción se aplica y luego procediendo con las correspondientes instrucciones.

> _Esta oración hace un trabajo doble. Es un recordatorio de que damos infracciones en casos mayores que los “pequeños”, y de que no empezamos con la penalización y luego evaluamos la infracción. Identificamos primero qué acciones ocurrieron, cuál fue la infracción, y luego determinamos la penalización. No damos Pérdidas de Juego porque un error nos parece que justifica una Pérdida de Juego._

Sólo el Juez principal está autorizado a dar penalizaciones que se desvían de estos lineamientos.

> _Cuando hay múltiples jueces, el Juez Principal está a cargo de todo el torneo; es el único que tiene la autoridad para determinar que una penalización específica no se aplica a la situación en curso. Los Jueces Principales son usualmente los jueces con mayor experiencia, y cuando deciden desviarse de los lineamientos suelen hacerlo por una buena razón._

El Juez principal no debe desviarse de los procedimientos de esta guía salvo bajo circunstancias significativas y excepcionales, o una situación que no tiene una filosofía que se le aplique para usar de guía.

> _Por supuesto que, si bien el Juez Principal tiene la autoridad de desviarse de los lineamientos en cualquier momento que desee, se espera que conozca cuándo es el momento apropiado para aplicar dicha desviación. La razón principal para desviarse es cuando una situación no es exactamente aplicable a ninguna de las categorías listadas en las veinte páginas de la Guía de Procedimientos de Infracciones. El único caso en el que una desviación es justificable es cuando dicha situación es significativa y excepcional._

Las circunstancias significativas y excepcionales son raras: se cae una mesa, un sobre contiene cartas de otra colección, etc.

> _Aquí vemos algunos ejemplos de “circunstancias excepcionales”. En estos casos, usamos el sentido común y tratamos de identificar cuál es la “mejor solución” con los jugadores. Ambos ejemplos son significativos y excepcionales. Hay que asegurarse que la situación posea ambas características antes de considerar una desviación._

El Nivel de imposición de reglas, la ronda del torneo, edad o experiencia del jugador, deseo de educar al jugador y nivel de certificación del juez **no** son circunstancias excepcionales.

> _En algunas de estas situaciones nos puede parecer que está bien aplicar una desviación, pero no es así. Debemos cumplir las políticas independientemente de que sea la última ronda, o de que el llamado del jugador venga de la mesa 1 o la mesa 101. Los oponentes pueden ser excepcionalmente jóvenes en términos de edad, pero la diferencia de edad no es relevante en términos de aplicar las políticas. Finalmente, ser un juez de Nivel 3 no nos otorga ningún derecho a desviarnos. De hecho, un estándar más estricto se mantiene sobre estos jueces, ya que los jueces de menor nivel observan y aprenden de sus acciones._

Si un juez cree que sería adecuado desviarse, debe consultar con el Juez Principal.

> _El hecho de que sólo el Juez Principal tenga potestad para desviarse, no significa que un Juez de Piso no pueda sugerir ese curso de acción al Juez Principal. Sin embargo, como Juez de Piso, uno nunca debe desviarse._

Los jueces son humanos y cometen errores. Cuando un juez comete un error, debería reconocer su error, pedir disculpas a los jugadores y corregirlo si no es muy tarde.

> _A pesar de todos nuestros esfuerzos para entrenar Golden Retrievers, los jueces seguimos siendo exclusivamente humanos._
> 
> _Por el momento al menos._
> 
> _Los humanos cometemos errores. Es un hecho de la vida. Nadie puede estar 100% en lo correcto todo el tiempo, y no es nada realista esperar esto de nadie. Sin embargo, cuando cometes un error, tienes que aceptarlo y corregirlo de la mejor manera posible. No se puede permitir a los jugadores continuar pensando que algo que un juez les dijo de forma incorrecta es correcta. En todos los casos, hay que pedir disculpas a los jugadores por el error. A veces es mejor hacerlo inmediatamente, otras veces es menos disruptivo hacerlo una vez que el juego haya finalizado. Hay que disculparse con ambos jugadores lo antes posible, y corregir la situación. Los jugadores por lo general tienden a entender las situaciones, aun cuando fueron perjudicados por el error._
> 
> 

Si un miembro del staff del torneo da a un jugador información errónea que hace que cometa una infracción, el Juez principal está autorizado a bajar la penalización.

> _Esperamos que los jugadores confíen en los Oficiales de Torneo, y es necesario permitirle a los jugadores actuar de acuerdo a las instrucciones/información que les dimos. No es justo penalizarlos por confiar en la gente en la que se supone tienen que poder confiar. Sin embargo, esta Disminución sigue siendo decisión del Juez Principal. Para esta cláusula necesitamos dos elementos 1) que el juez haya provisto información errónea y 2) Una infracción como resultado directo de la información errónea._

Por ejemplo, un jugador pregunta a un juez si una carta es legal para un formato y se le responde que sí. Cuando el mazo de ese jugador se descubre que es ilegal por esa carta, el Juez principal aplica los procedimientos normales para corregir la lista de mazo, pero baja la penalización a una Advertencia por el error directo del juez.

> _Otros ejemplos pueden incluir:_
> 
> *   _­Disminuir la Advertencia de una Violación de Reglas de Juego a ninguna penalización porque un juez le dijo a un jugador que puede enderezar dos tierras enderezadas con Teferi, héroe de Dominaria ([Teferi, Hero of Dominaria](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Teferi%2C+Hero+of+Dominaria&width=223&height=310))_
> *   _­Disminuir la Pérdida de Juego de un Problema de Mazo/Lista de Mazo a una Advertencia porque un juez le dijo a un jugador que es correcto registrar “Jace” en su lista de mazo._
> 
> 

Si un jugador claramente actúa sobre información erróneamente provista por un juez durante el juego, el juez principal puede considerar una vuelta atrás hasta el punto de la acción tomada, incluso si la acción no llevó a una violación.

> _Esperamos que los jugadores confíen en la información que les dan los Oficiales de Torneo, como detallamos previamente. Normalmente sólo se podría considerar una vuelta atrás si fue cometida una infracción, pero, de la misma manera que es injusto penalizar a un jugador por confiar en la información que le fue provista, es injusto también para el jugador haber tomado una decisión basada en esa información, incluso si no resulta en una infracción._