Los Errores de juego son ocasionados por un juego incorrecto o inadecuado de tal modo que resulta en una violación de las [Reglas Completas de Magic](http://blogs.magicjudges.org/rules/cr/).

> _Es la primera de las tres grandes categorías de infracciones. Cubre la violación no intencionada de las Reglas Completas – errores en el juego en sí mismo– y no violaciones de la política de torneo o algún otro comportamiento negativo._
> 
> _Estos errores son cometidos por al menos un jugador durante una partida por una violación no intencionada de las Reglas Completas. Los Errores de Juego (EJ) pueden ocurrir por distintas razones. Los jugadores se cansan, se distraen, juegan demasiado rápido, o desconocen las cartas o las reglas que se aplican a una situación compleja adecuadamente. Estas situaciones no son excepcionales, por lo cual EJ es la categoría más común para los errores._
> 
> 

Muchas ofensas entran en esta categoría y sería imposible enumerarlas todas.

> _Dada la complejidad del juego, es imposible hacer una lista de todos los tipos de errores que pueden ocurrir, así que, como jueces, no lo intentamos. Queremos que este documento sea comprensible y posible de aprender. Si enumeramos todo y detallamos todos los casos especiales, este documento sería de cientos de largas e inutilizables páginas. En vez de eso, hemos dividido estos errores en categorías generales, conocidos como los 5 EJ definidos por las IPG._

La guía a continuación está designada para dar a los jueces un marco de trabajo para saber cómo manejar un Error de juego.

> _Dado que puede ser difícil, a primera vista, percatarse a qué categoría pertenece cierta infracción en particular, leer cuidadosamente la infracción completa – la definición, la filosofía y el remedio – puede ayudarnos a determinar esto._

Se asume que la mayoría de las infracciones por Errores de juego fueron cometidas sin intención.

> _Los errores cometidos intencionalmente, por supuesto, pueden caer en una categoría distinta:_ [_Conducta Antideportiva – Hacer Trampa_](http://blogs.magicjudges.org/rules/ipg4-8-es/)_. Sin embargo, es importante darse cuenta de que no todos los errores de juego son hacer trampa. De hecho, muy pocos lo son._
> 
> _Nos gusta asumir que los jugadores son agradables, y, cuando caminamos a una mesa, no vamos acusando a la gente de tramposa. Esto puede cambiar una vez que hagamos algunas preguntas, pero, cuando comenzamos, nuestra suposición inicial es que estamos tratando con un error honesto._
> 
> 

Si el juez cree que el error fue intencional, debería considerar primero si ocurrió una infracción de [Conducta antideportiva — Hacer trampa](http://blogs.magicjudges.org/rules/ipg4-8-es/).

> _Este es el otro lado de la moneda; aunque la tarea del juez es siempre ayudar a los jugadores, nunca debemos olvidar que ellos pueden mentir o hacer trampa para obtener una ventaja. La experiencia y el consejo de jueces más experimentados pueden ayudar a manejar la situación correctamente y saber si un jugador era consciente de estar cometiendo una ofensa o no._

A excepción de [No mantener el estado de juego](http://blogs.magicjudges.org/rules/ipg2-6-es/), que nunca es aumentada, la tercera o subsiguiente penalización por ofensas de Error de juego en la misma categoría es aumentada a una Pérdida de juego. Para torneos de varios días, el aumento de penalización por reiteración para estas infracciones se reinicia cada día.

> _Queremos que los jugadores aprendan algo de sus errores y se preocupen de no cometerlos nuevamente en el futuro. Si un jugador comete errores repetidamente, la Advertencia no está haciendo su trabajo de reforzar la lección y, por lo tanto, debemos aumentar la severidad de la penalización a una Pérdida de Juego. Cuando le asignes un Error de Juego a un jugador, asegúrate de preguntarle si ha recibido esa infracción antes. Los torneos de múltiples días reinician las infracciones luego de cada corte, porque se determinó que era injusto que el límite se mantuviera en tres sin importar el número de rondas de un evento – es mucho más fácil acumular tres Errores de Juego en quince rondas de un Grand Prix que en cinco rondas de un pPTQ. Finalmente, señalar que esta es la tercera o subsiguiente penalización, no Advertencia. Si un jugador ha recibido 2 Violaciones de Reglas de Juego que han sido aumentadas a Pérdida de Juego, esta será aumentada a Pérdida de Juego también._
> 
> _Respecto a la infracción_ [_No Mantener el Estado de Juego_](http://blogs.magicjudges.org/rules/ipg2-6-es/) _prepárate para algunos jugadores que no entienden por qué reciben una Advertencia. “¡Pero juez, no hice nada malo!” Tómate unos momentos para explicarle al jugador por qué recibe la Advertencia y, si aún desea discutirlo, pueden hacerlo después de la partida. Pese a que son sancionados con una Advertencia, no aumentamos esta infracción como lo hacemos con otros errores de torneo. Esto es porque no queremos que los jugadores teman llamar a un juez. Ser recompensado con una Pérdida de Juego porque mis oponentes cometieron errores de juego y yo no me percaté se siente mal. Y, si esta es la tercera vez que mi oponente ha cometido un error de juego del que no me percaté durante el evento, podría sentirme reacio a llamar a un juez y que mi No Mantener el Estado de Juego aumente, así que decido hacer como que no me di cuenta. No queremos que nuestra política fomente hacer trampas. Si no aumentamos esta penalización, ¿para qué asignamos Advertencias entonces? Hay dos razones: la primera es que el hecho de recibir una Advertencia es generalmente suficiente para recordarle a un jugador que debe prestar más atención. La segunda es para que podamos rastrearlas. Si un jugador tiende a obtener muchos No Mantener el Estado de Juego, y el error relacionado es siempre a su favor, esto le brinda al juez la habilidad para rastrear estas infracciones – y cuando son añadidas a la inmensa base de datos de infracciones, podemos rastrearlas entre los eventos también._
> 
>