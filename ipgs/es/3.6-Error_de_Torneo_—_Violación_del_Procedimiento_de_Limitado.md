Definición
----------

Un jugador comete un error técnico durante un draft.

> 
> 
> **Penalización**
> 
> Advertencia
> 
> 

  

> 
> 
> _Un error técnico en esta situación significa cualquier error durante el proceso de draft. Esta infracción suele darse, aunque no siempre, en el contexto de un draft con tiempo. Esto significa que tendremos serios problemas con los límites de tiempos para completar el draft en curso. Errores técnicos como este causan demoras, y en situaciones como un draft en el día 2 de un Grand Prix provocará que todo el torneo se retrase._
> 
> 

Ejemplos
--------

*   **A.** Un jugador pasa un sobre a su izquierda cuando debería hacerlo a la derecha.  
    > _Si un jugador lo hace, provocará un efecto de cascada en el resto del draft: o es detenido inmediatamente por el jugador de uno de los lados del infractor, o el sobre es tomado y usado, complicando aún más el problema._
*   **B.** Un jugador se excede en el tiempo para realizar su elección de draft.  
    > _Un draft temporizado se hace para ser llevado a cabo de manera rápida y eficiente, además de los beneficios adicionales al coordinar múltiples draft de manera simultánea. Si un jugador excede el límite de tiempo, ese jugador está causando un retraso en el draft, que es algo que no debe pasar desapercibido._
*   **C.** Un jugador pone una carta en su montón de draft, y luego la toma nuevamente.  
    > _Esta es una gran preocupación, ya que cuando un jugador lo hace, puede parecer a otros jugadores, espectadores y jueces que tomó una carta de más del sobre. Esto tiene dos soluciones: al jugador se le “permite” tomar esas dos cartas, o los oficiales del torneo pierden tiempo solucionando un problema que no es tal. Ambas son malas para la integridad del torneo, pueden aumentar la posibilidad de que cartas del sobre sean mezcladas con cartas ya seleccionadas. A los jugadores se les indica que deben seleccionar una carta en un momento específico y no hacerlo en ese momento cae bajo la situación b) descrita arriba._
*   **D.** Un jugador mueve su cabeza de costado en momentos inadecuados.> _Este ejemplo le da a los jueces un poco de libertad de acción para utilizar la Violación del procedimiento de Limitado cuando ven a un jugador haciendo movimientos con su cabeza. Utiliza tu sentido común y asegúrate de considerar que ese jugador está haciendo trampa si crees que está haciendo esto para obtener información oculta._

Filosofía
---------

Los errores en el draft son disruptivos y podrían serlo aún más si no se corrigen inmediatamente. 

> _Hay muchos elementos que deben permanecer en movimiento durante un draft para lograr una transición sencilla hacia las rondas suizas. Algunos problemas pueden provocar un efecto dominó si no son detectados y corregidos a tiempo. Tales errores pueden afectar desde decenas hasta miles de jugadores en un período muy corto de tiempo. Es de vital importancia que los jugadores presten atención y sigan las instrucciones dadas durante este período crítico del evento._

Los anuncios previos al draft o las reglas específicas del torneo pueden especificar penalizaciones adicionales por Violación del procedimiento de Limitado.

> _Los avisos específicos para el formato pueden cambiar la forma en que esta penalización es aplicada; por ejemplo, puede haber un anuncio adicional indicando que se deben remover las fichas y cartas de tierras básicas. Para drafts de Modern Masters, es posible que el draft sea interrumpido por la presencia de una carta foil en cada sobre. Para formatos que involucren drafts de Innistrad y Dark Ascension, el Juez Principal puede anunciar una forma específica de cómo manipular las cartas de doble cara con respecto a esta infracción. Esto permite cierto margen de maniobra o instrucción adicional, basado en la decisión del Juez principal. Básicamente la oración previa le dá al juez el poder necesario para hacer que el draft se realice de una manera específica de ser necesario._