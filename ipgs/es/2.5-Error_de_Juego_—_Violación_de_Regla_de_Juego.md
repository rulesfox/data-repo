Definición
----------

Esta infracción cubre la mayoría de las situaciones de juego en las cuales un jugador comete un error o no sigue un procedimiento de juego correctamente. Lidia con violaciones a las [Reglas completas](http://blogs.magicjudges.org/rules/cr/) que no están cubiertas por los otros Errores de juego.

> 
> 
> **Penalización**
> 
> Advertencia
> 
> 

  

> 
> 
> _“Violación de Regla de Juego” no se refiere a ningún tipo de error en particular. Más bien, las Violaciones de Regla de Juego están específicamente definidas como errores que no son ninguna otra infracción. Los jueces más novatos suelen hablar acerca de que “resolver erróneamente un hechizo” es una Violación de Regla de Juego. En realidad, resolver un hechizo de forma equivocada puede resultar en infracciones distintas, desde_ [_Mirar Cartas Adicionales_](http://blogs.magicjudges.org/rules/ipg2-2-es/) _(por ejemplo, un jugador se olvida que su [Cazadora de Krufix](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Cazadora+de+Krufix&width=223&height=310) ya dejó el campo de batalla) hasta Error de Carta Oculta (leer equivocadamente la carta [Adivinación](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Adivinaci%C3%B3n&width=223&height=310) y robar tres cartas)._
> 
> 

**Ejemplos**
------------

*   **A.** Un jugador lanza [Ira de Dios](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Ira+de+Dios&width=223&height=310) por 3W (el coste actual es 2WW).
*   **B.** Un jugador no ataca con una criatura que debe atacar todos los turnos.
*   **C.** Un jugador se olvida de poner una criatura con daño letal en su cementerio y esto no se descubre hasta varios turnos después.
*   **D.** Un [Revocador pirexiano](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Revocador+pirexiano&width=223&height=310) está en el campo de batalla y no se eligió un nombre de carta para él.
*   **E.** Un jugador lanza [Inspiración súbita](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Inspiraci%C3%B3n+s%C3%BAbita&width=223&height=310) y se olvida de devolver dos cartas a la parte superior de su biblioteca.

**Filosofía**
-------------

Si bien las Violaciones de reglas de juego pueden atribuirse a un jugador, normalmente ocurren en una zona pública y ambos jugadores son responsables de saber qué está pasando en el juego. Es tentador intentar “arreglar” estos errores, pero es importante que sean tratados de manera consistente sin importar el impacto en el juego.

> _La consistencia es un elemento clave de la Guía de Procedimientos de Infracciones. Aún cuando existan miles de jueces trabajando en torneos alrededor del mundo, es importante que cada uno de estos torneos se realice y se hagan cumplir las reglas bajo el mismo estándar. Por dicho motivo, tratamos de manejar las penalizaciones de forma neutral. Fundamentalmente, ambos jugadores son responsables por mantener el estado de juego. Nuestro rol principal como jueces no es “corregir” o “arreglar” los errores de los jugadores, sino interpretar y aplicar las soluciones prescritas por la Guía de Procedimientos de Infracciones de forma neutral y desapasionada._

**Solución adicional**
----------------------

Primero, considera una vuelta atrás simple (ver la sección 1.4).

Si una simple vuelta atrás no es suficiente y la infracción cae en una o más de las siguientes categorías, y sólo en esas categorías, realiza la corrección parcial adecuada:

> _Aquí tenemos una lista de correcciones parciales. Intenta hacer una vuelta atrás simple (Ver_ [_1.4 Volver Atrás_](https://blogs.magicjudges.org/rules/ipg1-4-es/) _para más información acerca de estas vueltas atrás); luego, si no puedes, intenta una corrección parcial. Si no es posible aplicar una vuelta atrás simple o una corrección parcial, evalúa si necesitas hacer una vuelta atrás o dejar como está._
> 
> _Un cambio reciente aquí es que no se pueden mezclar correcciones parciales. Si el error se encuentra en más de una de estas categorías, necesitamos ir a la siguiente parte de la corrección. Si hay múltiples correcciones dentro de la misma viñeta, seguimos usando esa corrección parcial._
> 
> 

*   Si un jugador olvidó enderezar uno o más permanentes al comienzo de su turno y todavía es el mismo turno, enderézalos.

> _Las secuencias fuera de orden siempre han sido una opción viable cuando surgen este tipo de situaciones, y lo son con cierta regularidad. Los jugadores se entusiasman, o simplemente pierden el foco y olvidan enderezar todo antes de avanzar con el turno. Si eso sucede, simplemente registramos la advertencia y corregimos el error, enderezando esos permanentes ahora._

*   Si un jugador realizó una elección ilegal (incluyendo ninguna elección cuando debía hacerlo), para una habilidad estática que genera un efecto continuo todavía en el campo de batalla, ese jugador hace una elección legal.

> _Esta corrección parcial se refiere a cartas como [Némesis de nombre verdadero](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=N%C3%A9mesis+de+nombre+verdadero&width=223&height=310), [Voz de todos](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Voz+de+todos&width=223&height=310), o los asedios (Siege) de Destino Reescrito, que requieren la elección de un color o de un jugador al entrar al campo de batalla. La razón de esto es similar a por la que aplicamos acciones basadas en estado – es imposible que estas cartas estén en el campo de batalla sin una elección hecha, así que corregimos este error inmediatamente. Mientras que esto puede dar la percepción de que se otorga cierta ventaja a un jugador, este tipo de errores siempre ocurren de manera pública, así que es interés de ambos jugadores estar atentos._
> 
> _Entonces, si Noelia lanza [Cuchilla fatal](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Cuchilla+fatal&width=223&height=310) haciendo objetivo a la [Voz de todos](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Voz+de+todos&width=223&height=310) de Alicia, podemos hacer que Alicia elija un color ahora. Si Alicia elije Negro, Cuchilla fatal es ahora ilegal. Entonces, podemos hacer una Simple vuelta atrás al momento antes que la Cuchilla fatal haya sido lanzada._
> 
> 

*   Si un jugador falló al robar cartas, descartar cartas o regresar cartas de su mano a otra zona, el jugador lo hace.

> _Los jugadores generalmente pueden determinar con precisión si se olvidaron de robar o descartar cartas. Esto también incluye efectos que devuelven cartas a la mano. Esta corrección parcial no expira, aun cuando el error haya sido cometido varios turnos atrás._

*   Si un objeto está en una zona incorrecta ya sea por que se olvidó un cambio de zona requerido o por que se lo colocó en una zona incorrecta durante un cambio de zona, el objeto exacto es todavía conocido para todos los jugadores, y puede ser movido con una mínima disrupción del estado de juego actual, pon el objeto en la zona correcta.

> _Hay muchos elementos en este párrafo, así que vamos por partes._
> 
> _– “Un objeto en una zona incorrecta por un olvido de cambio de zona requerido”: Esto se aplica cuando una criatura debería haber muerto, pero sigue en el campo de batalla; o una carta que debería haber ido del tope de la biblioteca al cementerio; o una criatura que fue devuelta a la mano de su propietario pero sigue en el campo de batalla._
> 
> _\-“Se lo colocó en una zona incorrecta durante el cambio de zona”: Comúnmente esto es cuando una carta se supone debe ir al cementerio pero se exilia por error, o viceversa. Técnicamente no se aplica a cartas que van del tope de la biblioteca al cementerio por la sentencia que está a continuación._
> 
> _– “La identidad del objeto es conocida por todos los jugadores”: Generalmente esto significa que un objeto va de una zona pública a otra zona, pero puede incluir cartas reveladas del tope de la biblioteca, o cartas reveladas por efectos como [Coacción](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Coacci%C3%B3n&width=223&height=310). Todos los jugadores deben conocer la carta. No sólo dónde estaba, sino también qué carta era._
> 
> _– “Puede ser movido con una mínima disrupción del estado de juego”: Objetos desapareciendo del campo de batalla suelen generar disrupción. Siempre hay que considerar qué decisiones se tomaron basadas en que dicha carta se encontrase en el campo de batalla. Esta corrección parcial no es aplicable cuando algo que no debería haber cambiado de zona lo hizo. Por ejemplo, una 4/4 con 3 de daño es puesta en el cementerio, y luego se descubre que esto no debería haber ocurrido. Esta corrección parcial no incluye regresarla al campo de batalla._
> 
> 

*   Si el orden de asignación de daño no fue declarado, el jugador apropiado elige ese orden.

> _Esto suele ser raramente relevante. La mayoría de las veces, cuando hay múltiples bloqueadores, es clara cuál es la intención y no hay ninguna interacción real. Por otro lado, para los casos en los que es relevante tenemos esta corrección parcial. Nicole puede enojarse por el hecho de que Alberto pueda declarar el orden de bloqueadores en el medio de la resolución de hechizos; sin embargo, la mayoría de las veces dicho orden se vuelve relevante como resultado de un hechizo lanzado por Nicole. Si Nicole hace algo que haga que el orden de bloqueadores se vuelva relevante, no puede asumir que el orden que le convenga a ella es el orden que debe aplicarse. Ella tiene la responsabilidad de clarificar la ambigüedad antes de que se vuelva relevante._

Para cada uno de estas soluciones, una vuelta atrás simple puede aplicarse antes si hará que aplicar la solución sea más simple. Las habilidades disparadas son generadas por estas correcciones parciales sólo si hubieran ocurrido si la acción se hubiera realizado en el momento correcto.

> _Una vuelta atrás simple está revirtiendo la última acción que se realizó (o la que está actualmente en progreso) y se utiliza en algunos casos como porción de la solución indicada para hacerla más simple. Una vuelta atrás simple no debería involucrar ningún elemento aleatorio. Además, mientras aplicas una corrección parcial ten en cuenta las habilidades disparadas si hubieran ocurrido como parte de realizar la acción en el momento correcto._

De otro modo, puede considerarse una vuelta atrás o dejar el estado de juego como está.

> _Entonces, analizamos si se aplican una de estas correcciones; en caso contrario analizamos si aplicamos una Vuelta atrás o no. Ver sección_ [_1.4-Volver Atrás_](http://blogs.magicjudges.org/rules/ipg1-4-es/) _para más información sobre cuándo es aplicable una Vuelta Atrás._

Para la mayoría de los Errores de juego no descubiertos en un tiempo razonable para que un jugador lo descubra, los oponentes reciben una penalización por [Error de juego — No mantener el estado de juego](http://blogs.magicjudges.org/rules/ipg2-6-es/).

> _Esta es la definición de_ [_Error de juego — No mantener el estado de juego_](http://blogs.magicjudges.org/rules/ipg2-6-es/)_, y refuerza el concepto de que mantener el estado de juego en forma clara y legal es responsabilidad de ambos jugadores._

Si el juez cree que ambos jugadores fueron responsables por la Violación de regla de juego, por ejemplo por la existencia de efectos de reemplazo o porque un jugador realizó una acción siguiendo una indicación de otro jugador, ambos jugadores reciben un [Error de juego – Violación de regla de juego](http://blogs.magicjudges.org/rules/ipg2-5-es/). Por ejemplo, si un jugador lanza [Camino al exilio](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Camino+al+exilio&width=223&height=310) sobre la criatura de un oponente y el oponente pone la criatura en el cementerio, ambos jugadores cometieron esta infracción.

> _Como siempre, ambos jugadores son responsables de mantener un estado de juego claro. Si mi carta te instruye a tomar una acción, pero tú la realizas en forma incorrecta, ¿de quién es la culpa? ¿tuya por ejecutar la acción de forma incorrecta o mía por no asegurarme de que mi hechizo se resuelva de forma correcta? En este caso, es razonable decir que ambos están igualmente en falta. Es importante destacar que esto sólo se aplica a efectos activos. Si el jugador A no pagó 1 maná adicional al lanzar [Choque](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Choque&width=223&height=310) por haberse olvidado del efecto de [Thalia, guardiana de Thraben](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Thalia%2C+guardiana+de+Thraben&width=223&height=310) que controla el jugador N, esto no se considera un efecto activo para el jugador N. En ese caso, el error recae sobre el jugador A, y el jugador N debe recibir una Advertencia por_ [_No mantener el estado de juego_](http://blogs.magicjudges.org/rules/ipg2-6-es/)_._