Definición
----------

Un jugador actúa de manera amenazante hacia otros o su propiedad.

> 
> 
> **Penalización**
> 
> Descalificación
> 
> 

  

> _Conducta Antideportiva — Comportamiento Agresivo es bastante claro. A diferencia de Conducta Antideportiva_ [_Menor_](https://blogs.magicjudges.org/rules/ipg4-1-es/) _o_ [_Mayor_](http://blogs.magicjudges.org/rules/ipg4-2/)_, donde asignamos infracciones basadas en gente siendo incomodada u ofendida o por uso de lenguaje ofensivo (además de otras cosas), Conducta Antideportiva — Comportamiento Agresivo se basa en acciones físicas o lenguaje que sugiera la intención de causar daño físico._

Ejemplos
--------

*   **A.**Un jugador amenaza con golpear a otro jugador si no le concede.
*   **B.**Un jugador mueve la silla de otro jugador, causando que éste caiga al suelo.
*   **C.** Un jugador amenaza a un juez después de recibir una decisión de reglas.
*   **D.** Un jugador rompe una carta de otro jugador.
*   **E.** Un jugador intencionalmente vuelca una mesa.

> _Estos ejemplos dejan muy claro qué puede constituir Conducta Antideportiva — Comportamiento Agresivo. También incluye amenazas sutiles, tales como “Te estaré esperando en el estacionamiento cuando salgas”. No es necesario buscar palabras específicas – puedes reconocer una amenaza cuando la oyes. También es importante tener en cuenta que, si un jugador daña su propiedad, aunque pueda resultar intimidante, probablemente no caiga en Conducta Antideportiva — Comportamiento Agresivo sino en_ [_Conducta Antideportiva – Menor_](http://blogs.magicjudges.org/rules/ipg4-1-es/)_, a menos que lo esté haciendo de una manera que constituya una amenaza._

Filosofía
---------

La seguridad de todas las personas en el torneo es extremadamente importante. No se tolerará ningún abuso físico o intimidación.

> _La razón por la que otorgamos una descalificación por este tipo de infracción es porque todo el mundo debe sentirse seguro en cualquier evento sancionado, y permitir que alguien continúe en un evento después de tal comportamiento podría desalentar a los jugadores de volver a participar en un evento._

**Solución Adicional**
----------------------

El organizador debería pedirle al ofensor que abandone el salón.

> _Ten en cuenta que tu primera prioridad es calmar la situación. Cómo hacer esto depende de la situación. Sin embargo, no eres un oficial de policía y no estás obligado a involucrarte físicamente. Cuando se produce esta infracción, el día de ese jugador en el evento ha terminado. Él o ella será descalificado/a. Asegúrate de recoger una declaración para_ [_el Comité de Investigaciones_](https://blogs.magicjudges.org/o/spheres-es/investigations-committee-es/) _si el jugador está dispuesto a dar una, y asegúrate de escribir la tuya propia. También se aconseja que el Organizador del Torneo pida al jugador que deje el lugar tan pronto como sea posible. Es importante distinguir que es el Organizador del Torneo quien debería tomar esta decisión. Podemos alentar al Organizador del Torneo a tomar esta decisión, pero nuestra jurisdicción no llega más allá del alcance del evento. Una vez que el jugador deja de estar involucrado en el evento, deja de ser alguien con el que nosotros, como jueces, debamos lidiar._