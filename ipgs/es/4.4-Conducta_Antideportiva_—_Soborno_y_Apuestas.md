Definición
----------

Un jugador ofrece un incentivo para que su oponente acepte conceder, empatar o cambiar el resultado de una partida, incita un incentivo de ese tipo, o acepta uno. Referirse a la [sección 5.2](http://blogs.magicjudges.org/rules/mtr5-2/) de las Reglas de Torneos de Magic para una descripción detallada de qué constituye soborno.

> 
> 
> **Penalización**
> 
> Pérdida de partida
> 
> 

  

> 
> 
> _Cuando la IPG dice “referirse a las Reglas de Torneo de Magic (MTR)”, significa “todo lo referente a_ [_sobornos_](http://blogs.magicjudges.org/rules/mtr5-2/)_, excepto la penalización, está en las MTR”. Así que vamos a resumir los puntos clave aquí (por favor, consulta las MTR para una explicación completa del tema). Se han escrito artículos enteros sobre qué es o no legal. Aquí sólo lo estamos resumiendo._
> 
> _No se debe abandonar el torneo, conceder o acordar un empate intencionado a cambio de cualquier tipo de recompensa o incentivo. Declaraciones como “Te concedo si me das tu promo de FNM” o “Hey, si entro en el top 8, entraré en los premios, y puedo ser bastante generoso” son inaceptables. “Hey, tengo hambre, vamos a empatar y así podemos comer algo antes de la siguiente ronda” no es una oferta o incentivo, y como tal es aceptable._
> 
> _En las finales, hay una excepción a las reglas de_ [_soborno_](http://blogs.magicjudges.org/rules/mtr5-2/) _que permite a los jugadores dividir los premios como quieran, siempre y cuando no incluyan incentivos fuera de los premios del torneo. Si la final tiene un premio que no es divisible, como una Invitación al Pro Tour, entonces el jugador que no recibe el premio debe abandonar el torneo – no conceder, sino abandonar._
> 
> _Los jugadores pueden usar información sobre los resultados de otras partidas para determinar si quieren acordar un empate. Sin embargo, no pueden consultar esas partidas ni alcanzar un acuerdo con los jugadores de las mismas._
> 
> _En algunos eventos, durante la eliminación simple, los jugadores pueden decidir dividir todos los premios uniformemente. Esto requiere el consentimiento del organizador del torneo y el acuerdo unánime entre los jugadores._
> 
> 

Existen [apuestas](http://blogs.magicjudges.org/rules/mtr5-3/) cuando un jugador o espectador de un torneo hace u ofrece hacer una apuesta sobre el resultado de un torneo, partida o cualquier parte de un torneo o partida. La apuesta no necesita ser monetaria ni es relevante si un jugador está o no apostando sobre su propia partida.

> _Apuesta es más fácil de entender. Si apuestas algo en el evento, estás jugando al azar, y eso no está permitido en eventos sancionados. Una vez realizada una apuesta sobre el resultado de un juego en el evento, si los jueces no actúan inmediatamente, corren el riesgo de dañar la imagen de Magic en su conjunto, más allá de ese evento. Ha habido casos en los que eventos como FNMs fueron cerrados por autoridades locales porque sentían que se estaban quebrantando las leyes sobre apostar. Este es el tipo de cosas que Wizards of the Coast no quiere que la gente tenga en mente cuando piensan en su juego, y por esta razón hay una política de tolerancia cero._
> 
> _Apostar requiere que 2 personas estén arriesgando algo. Ofrecer una recompensa por un jugador no es apostar. “Te doy un booster si le ganas a mi amigo” es una sola persona arriesgando algo, e incluso está esperando tener que dar el booster._
> 
>   
Si el jugador sabía que lo que estaba haciendo era contrario a las reglas, la infracción es [Conducta antideportiva – Hacer trampa](https://blogs.magicjudges.org/rules/ipg4-8/).

> _Esta infracción asume que los jugadores no tengan conocimiento previo de que los sobornos y las apuestas van en contra de las reglas. Tener conocimiento de esto va contra las reglas y por ende la infracción pasa a ser_ [_Conducta Antideportiva – Hacer Trampa_](https://blogs.magicjudges.org/rules/ipg4-8/)

**Ejemplos**
------------

*   **A.**Un jugador en una ronda Suiza ofrece a su oponente $100 para que conceda la partida.

> _Este es un ejemplo de comprar una victoria, y no está permitido. Es una ventaja injusta que básicamente permitiría a la persona con mayor dinero la habilidad de comprar victorias._

*   **B.** Un jugador ofrece a su oponente una carta a cambio de un empate.

> _Este es un ejemplo de un incentivo para determinar el resultado. No es tan atractivo como un billete de cien dólares, pero sigue sin ser justo para todos los demás que alguien pueda usar un elemento externo para influenciar el resultado de las partidas._

*   **C.**Un jugador pide una concesión a cambio de un reparto de premios.

> _Recuerda que los jugadores pueden acordar dividir los premios – tal vez porque son amigos, o se sienten mal por algo que le ha pasado a otra persona en el evento – pero no si el reparto está condicionado por el resultado de una partida. Decir que concederás por un precio es ilegal. Sin embargo, este es un escenario legal: ofreces repartir el premio, luego, una vez aceptado, preguntas a tu oponente si quiere conceder o concedes. En este escenario, el reparto del premio no depende de la concesión, a pesar del hecho de que una vez se acordó el reparto un jugador no quería jugar más._

*   **D.** Dos jugadores acuerdan que el ganador de una partida podrá elegir una carta rara del mazo de la otra persona después de la partida.

> _Incluso algo como “un refresco por el empate” o “un masaje en los pies luego en casa” está afectando la decisión de los jugadores, independientemente de la intención de dicha oferta. Podría ser una “broma” entre dos jugadores, pero nunca sabremos si eso es cierto o no._

*   **E.** Dos espectadores hacen una apuesta sobre la cantidad de juegos que harán falta para definir una partida.

> _Este es un gran ejemplo de una apuesta en la que podrías no haber pensado inmediatamente._

**Filosofía**
-------------

El soborno y las apuestas son muy disruptivos para la integridad del torneo y están estrictamente prohibidos.

> _Ofreciendo cualquier incentivo por el resultado de la partida, los jugadores han manchado la integridad del evento y creado un ambiente de juego injusto en el que los resultados son decididos por algo que no son juegos de M__agic. Como esto puede ser muy dañino y difícil de descubrir, es penalizado._
> 
> _Versiones anteriores de las IPGs penalizaban esta infracción con una Descalificación, dada la severidad de la disrupción a la integridad del evento, lo cual sigue siendo el caso, usando [Conducta Antideportiva – Hacer Trampa](https://blogs.magicjudges.org/rules/ipg4-8/), para aquellos casos en los que el jugador tenía conocimiento acerca de las políticas de Soborno y Apuestas. Es mucho más difícil educar a un jugador que no estaba al tanto de que una pequeña apuesta o un una división de premios que él pensaba era válida en realidad van contra las reglas mientras estamos llenando el papeleo por Descalificación, mientras dar la misma explicación luego de que reciban la Pérdida de Partida_
> 
>