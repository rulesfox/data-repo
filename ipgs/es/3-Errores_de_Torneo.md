Los errores de torneo son violaciones de las [Reglas de Torneos de Magic](https://blogs.magicjudges.org/rules/mtr-es/).

> _Tal y como los Errores de Juego son violaciones de las Reglas Completas, los errores de torneo son violaciones de las Reglas de Torneo de Magic. Sin embargo, no toda violación de las reglas de torneo resulta en una infracción._

Si el juez cree que el error fue intencional, debería considerar [Conducta antideportiva — Hacer trampa](http://blogs.magicjudges.org/rules/ipg4-8-es/).

> _Esta aclaración cubre casos donde el jugador está violando una regla de torneo intencionalmente. El jugador puede estar haciendo trampa, pero su conducta podría no coincidir con la definición de Hacer Trampa en la Guía de Procedimientos de Infracciones. Asegúrate de revisar la sección_ [_Hacer Trampa_](http://blogs.magicjudges.org/rules/ipg4-8-es/) _antes de tomar una decisión._

Si un jugador viola las Reglas de torneo de **Magic** de una manera no cubierta por las infracciones aquí mencionadas, el juez debería explicar el procedimiento adecuado al jugador, pero no asignar una penalización.

> _Sólo las Reglas de Torneo de Magic más serias justifican una penalización. Estas reglas pueden ser bastante disruptivas para el desarrollo fluido del torneo, causando que el evento dure más de lo necesario y/o dando una ventaja significativa a un jugador. Si una violación a las Reglas de Torneo de Magic no es considerada perjudicial para el desarrollo del evento, entonces una penalización no es necesaria y el juez simplemente educará al jugador con una acción correctiva._

Ignorar repetida o voluntariamente estas reglas puede requerir una investigación adicional.

> _Aunque no todas las Reglas de Torneo de Magic son penalizadas, incluso la más pequeña disrupción repetida una y otra vez puede retrasar un evento. Si esto sucede, una investigación puede ser necesaria para ver si se está interrumpiendo intencionalmente el torneo o quizás un jugador simplemente necesita mayor asistencia._

La segunda o subsiguiente Advertencia por ofensas de Error de torneo en la misma categoría es aumentada a Pérdida de juego.

> _Los Errores de Torneo son considerados más disruptivos y menos “fáciles de cometer” que los Errores de Juego. Como resultado de esto, la segunda advertencia por un Error de Torneo en la misma categoría es aumentada a Pérdida de Juego (Game Loss). Todas las demás instancias en la misma categoría son también Pérdidas de Juego. No seguimos aumentando hasta llegar a la Descalificación._

Para torneos de varios días, el aumento de penalización por reiteración para estas infracciones se reinicia cada día.

> _Tal y como para los Errores de Juego, hay una mayor probabilidad de que alguien repita el mismo error en un Grand Prix de 15 rondas que en un PPTQ de 5 rondas, y reiniciar el contador de penalizaciones refleja esto. Nótese que el reinicio es entre cortes, en lugar de entre días, debido a que un corte puede llevarse a cabo en el mismo día como otra porción del evento._