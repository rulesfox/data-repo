La procédure supplémentaire de certaines infractions mentionne de mélanger la partie aléatoire de la bibliothèque.

> _Dans l’IPG, la procédure supplémentaire de certaines infractions indiquent qu’il faut mélanger la partie aléatoire de la bibliothèque ; pour ce faire, suivez les instructions ci-dessous._

En premier lieu cela requiert de déterminer s’il existe des parties non aléatoires de la bibliothèque, telles que des cartes qui ont été manipulées et placées au-dessus ou au-dessous de la bibliothèque, et de les séparer. Consultez les deux joueurs pour vérifier tout cela, et scrutez le cimetière, l’exil et le champ de bataille à la recherche de cartes de manipulation de la bibliothèque, telles que [Remue-méninges](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Remue-m%C3%A9ninges&width=223&height=310) ou des cartes avec la mécanique « regard ».

> _Quand nous demandons à un joueur de mélanger sa bibliothèque, nous ne voulons pas que cela affecte les parties de la bibliothèque qui ont été vues et ordonnées intentionnellement. Demandez aux joueurs si certaines cartes sont connues. Dans le même temps, étudiez l’état de la partie pour essayer d’établir si certains effets ont pu ordonner certaines parties de la bibliothèque ou au contraire, l’avoir mélangée. N’oubliez pas de demander si les joueurs ont effectué des Mulligans._

Une fois la bibliothèque mélangée, les cartes manipulées sont remises aux bons endroits.

> _Mettez de côté les cartes dont l’emplacement était connu, dites au joueur de mélanger la partie inconnue, puis replacez les cartes connues là où elles sont censées se trouver._

Les mélanges effectués par un arbitre en tant qu’élément d’une procédure supplémentaire ne sont pas considérés comme des mélanges aux yeux du jeu.

> _Navré [Escroc de Cosi](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Escroc+de+Cosi&width=223&height=310). De plus, il vaut mieux demander au joueur de mélanger que le faire soi même. Si le joueur n’aime pas ce qu’il pioche par la suite, il risque de le reprocher à l’arbitre. Laissez aux joueurs le soin de mélanger la bibliothèque._