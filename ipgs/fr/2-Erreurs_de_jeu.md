Les erreurs de jeu (Game Play Errors) sont dues à des actions de jeu incorrectes ou inexactes qui engendrent une violation des [règles complètes de Magic](http://blogs.magicjudges.org/rules/cr/) (Magic Comprehensive Rules).

> _Les erreurs de jeu sont la première des trois grandes catégories d’infractions. Ce sont les violations involontaires des règles complètes de Magic — les erreurs internes au jeu, pas celles liées au non-respect des règles du tournoi ou à d’autres comportements indésirables.
> 
> _
> 
> _Il s’agit des erreurs commises par au moins un joueur pendant un match en enfreignant involontairement les règles du jeu. Les Game Play Errors (GPE) peuvent se produire pour de nombreuses raisons : les joueurs sont fatigués ou distraits, ils jouents trop vite, ils ne connaissent pas assez bien les cartes ou les règles qui s’appliquent à une situation complexe. Ces situations n’ont rien d’exceptionnel, c’est pourquoi les GPE sont très courantes._
> 
> 

Beaucoup d’infractions rentrent dans cette catégorie et il serait impossible d’en tenir une liste exhaustive.  

> _Étant donnée la complexité de Magic, il est impossible de dresser la liste de toutes les erreurs susceptibles de se produire. Ce document doit être compréhensible, et il doit être possible de l’apprendre. Si nous listions tout, et que nous traitions chaque cas particulier, ce document contiendrait des centaines de pages et serait inutilisable. À la place, nous séparons les GPEs en catégories générales, à savoir les six erreurs relatives au jeu définies dans l’IPG._

Les sections ci-dessous ont pour but de donner aux arbitres des instructions générales permettant de résoudre les Game Play Errors.  

> _Il peut sembler difficile, au premier regard, de voir à quelle catégorie appartiennent certaines infractions particulières. Une lecture attentive et exhaustive de l’infraction — la définition, la philosophie et les procédures supplémentaires — peut aider à déterminer la bonne catégorie._

On part du principe que la plupart des Game Play Errors ont été commises de manière non intentionnelle.

> Si un joueur commet une erreur de jeu de façon intentionnelle, il faut d’abord envisager l’infraction [Conduite antisportive — Triche](http://blogs.magicjudges.org/rules/ipg4-8-fr/). Cependant, il est important de remarquer que toutes les erreurs de jeu ne sont pas de la triche. En pratique, très peu en sont.  
> Nous partons du principe que les joueurs sont honnêtes et bien intentionnés : quand nous arrivons à une table, nous n’accusons pas les joueurs de tricher. Il se peut que ça change après avoir posé quelques questions mais notre hypothèse de départ est que nous avons affaire à une erreur commise de bonne foi.

Si l’arbitre estime que l’erreur a été commise intentionnellement, il doit se demander tout d’abord si une infraction [Conduite antisportive — Triche](http://blogs.magicjudges.org/rules/ipg4-8-fr/) a eu lieu.

> C’est le revers de la médaille ; bien que la tâche d’un arbitre soit toujours d’aider les joueurs, nous ne devons jamais oublier qu’ils peuvent mentir ou tricher pour en tirer avantage. L’expérience et les conseils d’arbitres plus expérimentés peuvent aider à interpréter correctement la situation et à découvrir si le joueur était conscient ou non de commettre une infraction.

À l’exception de l’infraction [Échec à conserver un état de la partie légal](http://blogs.magicjudges.org/rules/ipg2-6-fr/) (Failure to maintain Game state ou FTMGS), dont la pénalité n’est jamais augmentée, la troisième pénalité pour Game Play Error dans une même catégorie et toutes les pénalités suivantes sont augmentées en Game Loss. Pour les tournois se déroulant sur plusieurs jours, le décompte des pénalités pour ces infractions est remis à zéro chaque jour.

> _Nous voulons que les joueurs retiennent quelque chose de leurs erreurs et qu’ils prennent garde à ne pas les réitérer à l’avenir. Si un joueur commet des erreurs de façon répétée, le Warning ne fait pas son travail d’appuyer la leçon, et donc nous devons augmenter la sévérité de la pénalité à un Game Loss. Quand vous donnez une GPE à un joueur, assurez-vous de lui demander s’il a déjà commis cette infraction auparavant. Les tournois qui se déroulent sur plusieurs journées réinitialisent ce compteur d’un jour à l’autre parce qu’on a déterminé qu’il était injuste de maintenir le parcours d’augmentation à trois sans tenir compte du nombre de rondes d’un événement — il est beaucoup plus facile d’accumuler trois GPEs au cours des quinze rondes d’un Grand Prix que dans les cinq rondes d’un PPTQ. Enfin notez bien qu’on parle de la troisième pénalité (ou plus), pas du troisième Warning. Si un joueur a déjà reçu deux GRVs dont la pénalité a été augmentée en Game Loss à chaque fois, et qu’il commet une troisième GRV, la pénalité de celle-ci est également augmentée en Game Loss._
> 
> _Concernant l’infraction [Échec à conserver un état de la partie légal](http://blogs.magicjudges.org/rules/ipg2-6-fr/), préparez-vous à ce que certains joueurs ne comprennent pas pourquoi ils reçoivent un Warning. “Mais m’sieur l’arbitre, je n’ai rien fait de mal ?” Prenez quelques instants pour expliquer au joueur pourquoi il reçoit ce Warning, et s’il souhaite toujours en discuter, vous pouvez lui en reparler après le match. Notez que la pénalité pour cette infraction n’est pas augmentée comme le sont celles des autres GPEs. C’est parce que nous ne voulons pas que les joueurs craignent d’appeler l’arbitre. Recevoir un Game Loss parce que l’on n’a pas détecté les erreurs de jeu de son adversaire n’a pas de sens. Et, si c’est la troisième fois pendant le tournoi que le joueur n’a pas repéré une erreur commise par son adversaire, celui-ci pourrait être réticent à appeler l’arbitre et voir sa FTMGS augmentée, donc il choisira de faire semblant de n’avoir rien remarqué. Nous ne voulons pas que notre règlement encourage à tricher. Si nous n’augmentons pas cette pénalité, pourquoi perdre du temps à mettre des Warnings ? Il y a deux raisons : la première est que recevoir un Warning est généralement suffisant pour rappeler au joueur qu’il doit faire plus attention. La seconde est de pouvoir les enregistrer. Si un joueur a tendance à recevoir beaucoup de FTMGS, et que l’erreur associée est toujours en sa faveur, cela confère aux arbitres la capacité de garder la trace ces infractions – et quand elles s’ajoutent à la grande base de données des infractions, nous pouvons ainsi en garder la trace au fil de plusieurs événements._
> 
>