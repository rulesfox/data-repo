Définition
----------

Cette infraction couvre la majorité des situations de jeu dans lesquelles un joueur fait une erreur ou ne suit pas la procédure de jeu correctement. Elle est utilisée pour gérer les violations des [règles complètes](http://blogs.magicjudges.org/rules/cr/) (Magic Comprehensive Rules) non couvertes par les autres Game Play Errors.

> 
> 
> **Pénalité**
> 
> Warning
> 
> 

  

> _L’expression « Game Rule Violation » (violation d’une règle du jeu) ne fait pas référence à un quelconque type d’erreur bien particulier. Au contraire, les GRVs sont spécifiquement définies comme les erreurs qui ne sont pas déjà couvertes par une autre infraction. Les nouveaux arbitres disent parfois que « mal résoudre un sort » est une GRV. En réalité, résoudre un sort de façon incorrecte peut résulter en différentes infractions, comme [Looking at Extra Cards](http://blogs.magicjudges.org/rules/ipg2-2-fr/) (ex : oublier la mort d’une [Coursière de Kruphix](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Coursi%C3%A8re+de+Kruphix&width=223&height=310)) voire [Hidden Card Error](http://blogs.magicjudges.org/rules/ipg2-3-fr/) (ex : lire de travers [Divination](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Divination&width=223&height=310) et piocher trois cartes)._

Exemples
--------

*   **A.** Un joueur lance [Colère de Dieu](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Col%C3%A8re+de+Dieu&width=223&height=310) pour 3W (au lieu de 2WW).
*   **B.** Un joueur n’attaque pas avec une créature qui doit attaquer à chaque tour.
*   **C.** Un joueur ne met pas une créature avec des blessures mortelles dans le cimetière et l’erreur n’est remarquée que plusieurs tours après.
*   **D.** [Révocateur phyrexian](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=R%C3%A9vocateur+phyrexian&width=223&height=310) est sur le champ de bataille, mais son contrôleur a oublié de nommer une carte à son arrivée en jeu.
*   **E.** Un joueur lance [Remue-méninges](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Remue-m%C3%A9ninges&width=223&height=310) et oublie de mettre deux cartes sur le dessus de sa bibliothèque.

Philosophie
-----------

Les violations des règles du jeu sont généralement le fait d’un seul joueur, elles se passent en général dans une zone publique, et l’état de la partie est de la responsabilité des deux joueurs. Il est tentant d’essayer de « réparer » ces erreurs mais il est important de les traiter de manière cohérente, quel que soit leur impact sur la partie.

> _La cohérence est au cœur de tout l’IPG. Bien qu’il y ait des milliers d’arbitres qui jugent des tournois partout dans le monde, il est important que chacun de ces tournois soit géré et arbitré selon les mêmes normes. Pour cette raison, nous nous efforçons de traiter les pénalités de façon neutre. Le principe fondamental est que les deux joueurs sont responsables de maintenir le jeu dans un état correct. Notre rôle central en tant qu’arbitres n’est pas de « corriger » ni de « réparer » les erreurs des joueurs, mais d’interpréter et d’appliquer les procédures décrites par l’IPG de manière impartiale._

Procédure supplémentaire
------------------------

Envisagez tout d’abord un retour en arrière simple (cf section 1.4).

Si un retour en arrière simple n’est pas suffisant et que l’erreur rentre dans exactement une des catégories suivantes, appliquez la correction partielle appropriée :

> _Ci-après se trouve une liste de réparations partielles. C’est par elles qu’il faut commencer. Si c’est impossible, alors on évalue s’il vaut mieux revenir en arrière ou laisser la partie en l’état. Cependant si le retour en arrière est trivialement simple (ex : une seule action) et qu’il permet une réparation plus élégante, le Head-Judge peut autoriser ce retour en arrière. La section [1.4 Revenir en arrière](http://blogs.magicjudges.org/rules/ipg1-4-fr/) contient toutes les informations sur les retours en arrière._
> 
> Il y a récemment eu une modification à ce paragraphe : on ne peut pas appliquer plusieurs réparations partielles différentes. Si l’erreur correspond à plusieurs de ces catégories, il faut passer à l’étape suivante de la procédure supplémentaire. Par contre s’il y a plusieurs actions à effectuer pour une seule et même réparation partielle, alors vous pouvez l’appliquer sans problème. Remarquez que les réparations partielles n’expirent pas, même si l’erreur a eu lieu plusieurs tours auparavant.
> 
> 

*   Si un joueur a fait un choix illégal (y compris oublier de faire un choix requis) pour une capacité statique produisant un effet continu, encore sur le champ de bataille, ce joueur fait un choix légal. On peut envisager un retour en arrière simple pour résoudre les problèmes engendrés par le choix illégal.
    
    > Cette réparation partielle fait référence à des cartes telles que [Némésis de l’identité](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=N%C3%A9m%C3%A9sis+de+l%26%238217%3Bidentit%C3%A9&width=223&height=310), [Voix du Grand Tout](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Voix+du+Grand+Tout&width=223&height=310) ou encore une des cartes [Siege](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Si%C3%A8ge&width=223&height=310) de Destin reforgé™, qui demandent un choix au moment où elles arrivent sur le champ de bataille. Si le joueur a oublié de déclarer son choix, il le fait maintenant. Bien que cela puisse être perçu comme à l’avantage d’un joueur, de telles erreurs surviennent toujours aux yeux de tous donc c’est dans l’intérêt des deux joueurs de rester vigilant. La deuxième phrase nous permet d’éviter que les conséquences d’un choix retardé soient trop dramatiques: si Nicolas lance [Lame du destin](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Lame+du+destin&width=223&height=310) sur la [Voix du Grand Tout](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Voix+du+Grand+Tout&width=223&height=310) d’Alice, cette dernière choisit une couleur immédiatement. Si Alice choisit le noir, [Lame du destin](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Lame+du+destin&width=223&height=310) est illégale à présent. Dans ce cas, on peut effectuer un retour en arrière simple jusqu’à juste avant le lancement de [Lame du destin](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Lame+du+destin&width=223&height=310).
*   Si un joueur a oublié de piocher des cartes, de défausser des cartes ou de renvoyer des cartes de sa main dans une autre zone, il le fait.
    
    > Généralement les joueurs seront capables de déterminer avec une grande précision s’ils ont oublié de piocher ou de défausser des cartes. Sinon, utilisez les [Card Counting Challenge](https://blogs.magicjudges.org/conferences/2017/09/26/some-news-on-the-card-counting-challenge/) pour vous entraîner à vérifier s’il y a eu une erreur.
*   Si un objet se trouve dans une mauvaise zone – soit parce qu’un changement de zone obligatoire a été manqué soit parce qu’il a été mis dans la mauvaise zone lors d’un changement de zone –, que l’identité de cet objet est connue de tous les joueurs et qu’il peut être déplacé en ne perturbant que peu l’état de la partie, mettez l’objet dans la zone adéquate.
    
    > Cette phrase contient beaucoup d’éléments, aussi déconstruisons-la.  
    > – « dans une mauvaise zone – soit parce qu’un changement de zone obligatoire a été manqué » : cela s’applique par exemple à une créature qui était censée mourir mais est restée en jeu, ou à un permanent qui devait remonter en main et qui est resté là où il était.  
    > – « soit parce qu’il a été mis dans la mauvaise zone lors d’un changement de zone » : le plus souvent, une carte qui aurait dû aller en exil, et a été mise au cimetière à la place.  
    > – « l’identité de cet objet est connue de tous les joueurs » : en règle générale cela implique d’aller d’une zone publique à une quelconque autre zone mais cela peut aussi concerner des cartes révélées depuis le dessus de la bibliothèque ou par une [Contrainte](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Contrainte&width=223&height=310). Il faut que tous les joueurs sachent quelle était la carte. Pas seulement où elle était mais ce qu’elle était.  
    > – « il peut être déplacé en ne perturbant que peu l’état de la partie » : quand quelque-chose disparaît du champ de bataille au milieu du combat, cela a tendance à perturber la partie. Prenez soin de déterminer quelles décisions ont été prises en se basant sur la présence de cette carte sur le champ de bataille.
    > 
    > Notez bien que cette réparation ne concerne pas les objets qui n’auraient pas dû être déplacés, mais qui l’ont été quand même. Si une 4/4 est mise au cimetière alors qu’elle n’a reçu que 3 blessures, on ne peut pas la remettre en jeu avec cette réparation partielle.
    > 
    > 
*   Si l’ordre d’assignation des blessures n’a pas été déclaré, le joueur approprié en déclare l’ordre.
    
    > C’est rarement pertinent. La plupart du temps l’intention est claire quand il y a plusieurs bloqueurs, et il n’y a pas vraiment d’interaction. Toutefois cela peut devenir pertinent, et désormais nous pouvons appliquer cette réparation partielle. Il se peut que Nestor soit furieux qu’Achille puisse choisir l’ordre des bloqueurs alors que plusieurs sorts sont en pile, mais il ne peut pas simplement supposer que l’ordre est celui qui lui convient le plus. Nestor doit lever l’ambiguïté avant que cet ordre ne devienne pertinent.

Pour chacune de ces corrections, un retour en arrière simple peut être effectué avant la correction, si cela rend la correction plus naturelle. Lors d’une correction partielle, une capacité ne peut se déclencher que si elle existait au moment de l’erreur.

> Un retour en arrière simple défait la dernière action effectuée (ou en cours d’exécution) et est parfois utilisé pour simplifier une autre partie de la procédure supplémentaire. Un retour en arrière simple ne devrait pas faire intervenir des éléments aléatoires.

Sinon, un retour en arrière peut être envisagé ou la partie peut être laissée en l’état.  

> _Donc nous vérifions si certaines des réparations partielles s’appliquent, et si ce n’est pas le cas, soit nous revenons en arrière soit nous ne faisons rien. Veuillez vous référer au chapitre [1.4 Revenir en arrière](http://blogs.magicjudges.org/rules/ipg1-4-fr/) pour plus d’informations sur s’il est approprié ou non de revenir en arrière._

Pour la plupart des Game Play Errors qui n’ont pas été repérées dans un délai pendant lequel on pourrait raisonnablement attendre d’un joueur qu’il les remarque, l’adversaire reçoit une pénalité d’erreur de jeu — [échec à conserver un état de la partie légal](http://blogs.magicjudges.org/rules/ipg2-6-fr/) (Failure to Maintain Game State).

> _C’est la définition même de l’infraction [Game Play Error — Failure to Maintain Game State](http://blogs.magicjudges.org/rules/ipg2-6-fr/). Il est de la responsabilité des deux joueurs de conserver la partie dans un état clair et légal._

Si l’arbitre estime que les deux joueurs sont responsables d’une violation des règles du jeu, par exemple à cause de la présence d’effets de remplacement ou parce qu’un joueur a effectué une action demandée par l’instruction d’un autre joueur, les deux joueurs reçoivent [Game Play Error — Game Rule Violation](http://blogs.magicjudges.org/rules/ipg2-5-fr/). Par exemple, si un joueur lance [Chemin vers l’exil](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Chemin+vers+l%26%238217%3Bexil&width=223&height=310) sur une créature adverse et que l’adversaire met la créature au cimetière, les deux joueurs ont commis cette infraction.

> Comme toujours, les deux joueurs sont responsables de maintenir un état du jeu clair. Si ma carte vous dit d’effectuer une action, et que vous le faites de façon incorrecte, de qui est-ce la faute ? La vôtre pour avoir effectué l’action de façon incorrecte, ou la mienne pour ne pas m’être assuré que mon sort s’était résolu correctement ? Dans ce cas-là, il s’avère qu’il est raisonnable de dire que nous sommes tous les deux en tort de façon égale. Notez qu’il faut que les deux joueurs aient soit une part active dans l’erreur, soit qu’un joueur ait à appliquer un effet de remplacement contrôlé par son adversaire. Si Achille oublie de payer la taxe de la [Thalia, gardienne de Thraben](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Thalia%2C+gardienne+de+Thraben&width=223&height=310) de Nestor, l’erreur n’est que le fait d’Achille. Nestor reçoit [Game Play Error — Failure to Maintain Game State](http://blogs.magicjudges.org/rules/ipg2-6-fr/).