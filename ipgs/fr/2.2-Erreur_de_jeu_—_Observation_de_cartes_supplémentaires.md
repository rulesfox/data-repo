Définition
----------

Un joueur réalise une action qui pourrait lui permettre de voir la face de cartes dans un deck alors qu’il n’en a pas le droit.

> 
> 
> **Pénalité**
> 
> Warning
> 
> 

  

> Il est facile de commettre cette infraction. Chaque fois qu’un joueur touche une bibliothèque, il y a une chance qu’il fasse tomber une carte ou qu’il en retourne une face visible. En particulier, il arrive souvent de prendre deux cartes lors d’une pioche et de voir le recto de la carte du dessous.
> 
> Généralement, quand on parle de Looking at Extra Cards (L@EC), on parle d’un joueur qui a vu le recto d’une carte de son propre deck. Toutefois, il peut aussi voir des cartes du deck de son adversaire, par exemple lorsqu’il le mélange. Il est important de reporter certaines informations sur la feuille de résultat : De quel deck provient la carte qui a été vue ? Où en sommes-nous dans la partie? S’agit-il d’une erreur de dextérité ou d’une action volontaire due à une mauvaise lecture de l’état du jeu. L’[article de Guillaume Beuzelin](https://blogs.magicjudges.org/whatsupdocs/2015/05/06/reporting-a-looking-at-extra-cards-penalty/) donne de bonnes bases pour rédiger ces pénalités.
> 
> Prenez garde à ne pas confondre cette infraction avec une erreur liée à une carte cachée (Hidden Card Error). Faites tout spécialement attention au fait que l’infraction [Hidden Card Error](http://blogs.magicjudges.org/rules/ipg2-3-fr/) concerne les situations où un joueur manipule un certain nombre de cartes du dessus de sa bibliothèque et en prend trop. Dans ce cas particulier, le joueur observe des cartes supplémentaires mais il ne commet pas l’infraction d’observation de cartes supplémentaires (Looking at Extra Cards). Avez-vous mal au crâne ? Moi oui.
> 
> Faire tomber une carte au cours du mélange de votre propre bibliothèque n’est pas une L@EC. Remettez simplement la carte dans le paquet et continuez de le mélanger.
> 
> Voir le recto d’une carte que votre adversaire a fait tomber ou qu’il a renversée n’est pas non plus une L@EC. Il y a deux raisons à cela : 1) si un joueur pouvait faire tomber une carte pour que son adversaire reçoive un Warning, il pourrait se frayer un chemin jusqu’au top 8 en mélangeant maladroitement ; 2) Le [chapitre 3.12 des MTR](http://blogs.magicjudges.org/rules/mtr3-12/) autorise un joueur à révéler des informations cachées à son adversaire, du moment qu’il est en droit de les connaître lui-même.
> 
> Cette infraction ne s’applique qu’aux cartes de la bibliothèque. Si vous voyez accidentellement les cartes exilées par un [Messager de Bomat](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Messager+de+Bomat&width=223&height=310), il ne s’agit pas d’une L@EC, mais d’une Game Rule Violation.  
> 
> 
> 

Cette pénalité n’est à appliquer qu’une seule fois quel que soit le nombre de cartes vues au cours de la même action ou séquence d’actions.

> Nous ne pénalisons pas un joueur une fois par carte supplémentaire observée quand toutes les cartes sont vues pendant la même action de jeu ou la même séquence d’actions de jeu.

Exemples
--------

*   **A.** Une joueuse révèle accidentellement (en laissant tomber ou en retournant) une carte en mélangeant le deck de son adversaire.
*   **B.** Un joueur soulève une carte supplémentaire en piochant une carte depuis sa bibliothèque.

> On peut trouver d’autres exemples: un joueur révèle une carte de trop pendant la résolution d’une capacité de Cascade; une joueuse met une carte de trop dans son cimetière pendant qu’elle « meule » son deck et s’en rend compte immédiatement.

Philosophie
-----------

Il est facile pour un joueur de regarder accidentellement des cartes supplémentaires. Cette infraction concerne les situations où une erreur de dextérité ou sur les règles a conduit un joueur à voir des cartes qu’il n’aurait pas dû voir.

> Les joueurs passent beaucoup de temps à manipuler leurs decks : pour le mélanger, pour piocher, pour y chercher un terrain, etc. À chaque fois que quelqu’un touche un deck, il y a un risque qu’il voie quelque chose qu’il ne devrait pas. Puisque c’est facile à faire, facile à remarquer, et facile à corriger, la pénalité adéquate est le Warning.

Les cartes sont considérées comme appartenant à la bibliothèque, jusqu’à ce qu’elles touchent des cartes d’un autre ensemble. Une fois que ces cartes ont rejoint un autre ensemble, l’infraction est traitée comme une erreur liée à une carte cachée (Hidden Card Error) ou une violation des règles du jeu (Game Rule Violation).

> Nous savons tous que piocher n’est pas la même chose que regarder mais la frontière n’est pas toujours facile à voir. Ici la frontière est quand la carte regardée est ajoutée à un autre ensemble de cartes, comme la main ou encore à sept autres cartes mises de côté pour résoudre [Fouille temporelle](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Fouille+temporelle&width=223&height=310). Gardez à l’esprit que pour cette infraction, vous êtes invité à exercer quelque peu votre jugement quand vous serez confronté à « J’avais l’intention de prendre une seule carte mais j’en ai pris deux par erreur ». La carte n’a pas encore été « ajoutée à un ensemble » ici.

Les joueurs ne doivent pas se servir de cette pénalité pour obtenir des « mélanges gratuits » ou pour remélanger dans le jeu des cartes qu’ils ne souhaitent pas piocher. Ce serait un cas de [Conduite antisportive — Triche](http://blogs.magicjudges.org/rules/ipg4-8-fr/).

> Ici on parle de la capacité de l’arbitre à comprendre qu’un joueur lui ment. Certains joueurs savent bien que la procédure supplémentaire pour une LEC est un mélange, donc ils pourraient “accidentellement” jeter un œil à une carte afin de tenter d’obtenir un mélange gratuit. C’est de la triche, et ça met en lumière que nous avons besoin de déterminer si des cartes dans la bibliothèque sont connues avant d’appliquer la procédure supplémentaire. Quand vous arrivez à la table, posez des questions aux deux joueurs pour tenter de comprendre ce qu’il se passe vraiment.

Les joueurs ne doivent pas non plus être autorisés à utiliser cette pénalité comme un moyen d’abuser de la limite de temps.

> Mélanger un deck prend du temps, et les joueurs le savent. En tant qu’arbitres nous devons empêcher les joueurs de tirer avantage de la limite de temps et nous devons comprendre quand un joueur essaie de le faire. On considère aussi cela comme de la triche, mais c’est très difficile à détecter.

L’ordre des cartes dans la bibliothèque est déjà aléatoire, donc mélanger les cartes révélées ne devrait pas demander beaucoup d’effort.

> Cela signifie que le joueur n’a pas besoin de perdre du temps à mélanger, contrairement à ce qu’il doit faire pendant la procédure avant de commencer la partie. Quelques mélanges « mash » (sur le côté) ou 3 à 5 mélanges « riffle » devraient suffire. L’objectif est de « perdre la trace » dans le deck des cartes qui ont été vues.

Procédure supplémentaire
------------------------

Mélangez les cartes qui n’étaient pas connues auparavant dans la portion aléatoire de la bibliothèque, puis remettez aux bons endroits les cartes connues.

> Consultez la [section 1.3 de l’IPG](http://blogs.magicjudges.org/rules/ipg1-3-fr/) pour tout savoir sur comment rendre un deck aléatoire. En résumé : déterminez quelles cartes sont légalement connues grâce à des effets tels que le regard ou la cascade puis, en veillant à laisser ces cartes là où elles sont censées être, mélangez toutes les autres cartes. Si la carte qui a été vue accidentellement était déjà connue auparavant (par exemple avec [Remue-méninges](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Remue-m%C3%A9ninges&width=223&height=310)), il n’est même pas nécessaire de mélanger le deck.