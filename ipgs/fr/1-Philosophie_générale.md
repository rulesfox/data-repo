Les arbitres se doivent d’être neutres et de faire respecter les règles et les règlements.

> _C’est probablement le concept le plus important de tout le document : les arbitres se doivent d’être neutres. Il devrait être inconcevable qu’un arbitre rende une décision en faveur d’un joueur parce qu’ils sont amis ou qu’il a plus d’estime pour lui que pour son adversaire. L’impartialité est une notion fondamentale du [Code de conduite de l’arbitre de Magic](http://blogs.magicjudges.org/translatedrules/2016/02/10/mjc-20151226-fr/). Les arbitres sont généralement considérés avec respect en grande partie parce qu’ils sont neutres et parce qu’ils appliquent le règlement équitablement._

Un arbitre ne devrait pas intervenir dans une partie à moins qu’il ne pense que les règles viennent d’être enfreintes, qu’un joueur avec une question ou un souci ne demande son assistance, ou qu’il souhaite empêcher une situation de se dégrader.

> _Les arbitres sont là pour les joueurs. On a besoin de nos services quand une règle est enfreinte, qu’un joueur a une question ou une requête, ou encore qu’une situation délicate survient (comme une dispute) et qu’il s’avère nécessaire de calmer les protagonistes. Les arbitres ne doivent pas intervenir dans les parties tant que leur assistance n’est pas requise. Cela implique de s’abstenir de tout commentaire à propos des actions de jeu, afin de ne pas risquer de donner accidentellement des conseils ou de perturber la concentration des joueurs. Laissez les joueurs jouer. Ça ne veut pas dire que vous devez agir comme un robot. Vous avez le droit de discuter avec les joueurs et de plaisanter avec eux, tant que vous ne dérangez pas leurs parties._

Les arbitres n’empêchent pas les erreurs de jeu de se produire : ils réparent la partie lorsqu’une erreur a été commise et pénalisent ceux qui enfreignent les règles ou les règlements. Ils encouragent le fair-play et la sportivité en donnant l’exemple et en se montrant diplomates.

> _Comme dans beaucoup d’autres sports, les arbitres n’empêchent pas les erreurs. Par contre, dès qu’une infraction de jeu se produit, les arbitres interviennent et appliquent les corrections et pénalités nécessaires. Les joueurs ne doivent pas compter sur les arbitres pour les empêcher de commettre des actions illégales : les arbitres ne peuvent pas anticiper toutes les actions des joueurs, a fortiori quand celles-ci sont effectuées rapidement. Dans l’immense majorité des cas, réparer une infraction après qu’elle a eu lieu rétablit le cours normal de la partie. Ces règles sont aussi applicables lorsque vous regardez un match à la fin d’une ronde ou pendant un Top 8.  
> Il est également important que les arbitres montrent le bon exemple par leur comportement. Votre attitude et vos actes ont un fort impact sur l’ambiance de l’événement. Les gens devraient voir dans votre comportement celui-là même que vous attendez d’eux pendant les tournois._

Un arbitre peut toutefois intervenir pour prévenir ou corriger des erreurs en dehors des parties.

> _S’il est difficile d’anticiper une infraction imminente pendant une partie, il est parfois possible de le faire en dehors d’une partie. Dans ce cas, l’arbitre doit intervenir et empêcher l’infraction. Le «peut» dans cette phrase ne signifie pas «l’arbitre a le droit de choisir» mais «L’IPG et le MTR autorisent les arbitres à intervenir». En dehors d’une partie, un arbitre doit toujours intervenir pour empêcher les infractions qu’il remarque, même s’il est indéniable que certaines peuvent lui échapper.  
> Quelques exemples :
> 
> *   Un arbitre voit qu’un joueur mélange son deck après la fin de la première partie, remarque qu’une créature qui a été exilée traîne encore sur la table, et réalise que le joueur a oublié de la remettre dans son deck ; l’arbitre intervient et dit au joueur qu’il a oublié de mélanger une carte.
> *   Dans un tournoi en paquet scellé, un joueur donne sa liste de deck à un arbitre, et l’arbitre remarque que ce joueur a oublié de noter ses terrains de base ; l’arbitre demande au joueur d’inscrire les terrains de base qu’il joue.
> *   Juste avant le début d’une ronde, un joueur apporte à la table d’arbitrage un [Pacifisme](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Pacifisme&width=223&height=310) qui appartient à son adversaire précédent ; les arbitres font de leur mieux pour identifier le propriétaire de la carte et déterminer à quelle table il joue afin de la lui rendre avant qu’il ne commence à jouer avec un deck illégal.
> *   Avant le tournoi, un arbitre voit une carte altérée au dessin d’un goût douteux. L’arbitre rappelle au joueur qu’il doit obtenir l’accord préalable du [Head-Judge](http://blogs.magicjudges.org/rules/mtr1-7-fr/) pour jouer avec cette carte altérée.
> 
> _
> 
> 

La connaissance des antécédents ou des compétences d’un joueur ne peut en aucun cas modifier la manière de traiter une infraction, mais elle peut être prise en considération pendant une enquête.

> _On ne modifie pas l’infraction en fonction de la perception du niveau du joueur. Une GRV appelle un Warning, peu importe que le joueur soit un débutant ou un professionnel chevronné. Une fois que vous avez déterminé quelle infraction a été commise, appliquez les pénalités sans aucun préjugé. On traite la GRV d’un joueur à la réputation louche de la même façon que l’on traite celle d’un arbitre de niveau 3. Cependant, les antécédents d’un joueur peuvent vous aider à déterminer quelle infraction a été commise. Par exemple un nouveau joueur qui ne comprend pas comment le piétinement fonctionne est bien plus crédible qu’un joueur expérimenté, avec qui vous avez déjà discuté du piétinement plusieurs fois. Il reste possible que l’erreur ait été commise de bonne foi mais vos connaissances influenceront les questions posées au cours de l’enquête._

Le but d’une pénalité est d’apprendre au joueur à ne pas refaire la même erreur plus tard.

> _Les pénalités ne sont pas là pour permettre à des arbitres sadiques de torturer des joueurs sans défense. Leur but est de réduire les chances que les erreurs se reproduisent. Après avoir reçu une pénalité pour une action, un joueur est moins susceptible de réitérer cette erreur à l’avenir. Ce sont des choses tangibles, généralement destinées à renforcer la leçon «j’ai perdu une partie à cause de cette erreur, et je ne veux pas en perdre une autre pour quelque chose que je peux éviter facilement, je compterai jusqu’à 60 chaque fois que j’écrirai une liste de deck». L’objectif premier d’une pénalité n’est pas d’être enregistrée (même si c’est appréciable), c’est d’éduquer le joueur fautif._

Pour ce faire, il faut à la fois expliquer comment les règles ou les règlements ont été enfreints et infliger une pénalité pour appuyer la leçon.

> _La phrase ci-dessus insiste sur l’importance de l’éducation. La pénalité seule ne suffit pas. Vous devez aussi expliquer (brièvement) ce que le joueur a fait de mal. Sinon, il pourrait ne comprendre que partiellement son erreur._

Les pénalités servent également d’exemple pour les autres joueurs du tournoi, afin de les dissuader de faire des erreurs. Elles peuvent aussi servir à suivre l’évolution de l’attitude d’un joueur sur une période de temps.

> _Il n’y a pas besoin de recevoir une pénalité pour se rendre compte que l’on n’en veut pas. Si un ami perd une partie importante à cause d’une pénalité, nous n’avons certainement pas envie de perdre de la même façon. Nous apprenons des erreurs de nos amis autant que des notres._
> 
> Il existe une archive (privée) de toutes les pénalités, de même qu’il existe une archive (publique) de tous les résultats de matchs. Cette archive nous sert à détecter les cas où un joueur commet la même infraction un grand nombre de fois. Si un joueur reçoit un Warning pour «Looking at Extra Cards – Il a fait tomber une carte du deck de son adversaire pendant qu’il le mélangeait avant le début du match» vingt fois en vingt tournois consécutifs, on peut légitimement se demander s’il ne le fait pas à dessein, parce qu’il sait que «la première fois, c’est Warning».
> 
> 

Si une infraction mineure est gérée rapidement par les joueurs d’une façon qui les satisfait, l’arbitre ne doit pas intervenir.

> _Les arbitres doivent être perçus comme une aide apportée aux joueurs. Il y a beaucoup d’erreurs mineures que les joueurs commettent et corrigent par eux-mêmes au fil d’un match sans avoir besoin d’un arbitre. Si l’erreur est minime, que les joueurs la corrigent d’eux-mêmes, et qu’ils en sont tous deux satisfaits, il n’y a pas de raison que l’arbitre intervienne dans leur partie._

Si les joueurs jouent d’une manière qui leur paraît claire à tous les deux, mais qui pourrait sembler confuse pour un observateur extérieur, les arbitres sont invités à demander aux joueurs de jouer de manière plus claire, sans pour autant infliger de pénalités.

> _Certains joueurs ont des raccourcis qui leur sont propres, utilisent les mêmes dés pour représenter différents types de compteurs, ou utilisent les mauvais jetons de créatures. Toutes ces choses peuvent leur sembler claires alors qu’elles ne le sont pas pour des observateurs extérieurs (y compris les arbitres). Si les deux joueurs sont bien d’accord sur l’état de la partie et que tout va bien, n’attribuez pas de pénalité. Demandez-leur simplement de jouer de façon plus claire. Il arrive souvent que des spectateurs viennent nous trouver pour des problèmes qui n’en sont pas vraiment. C’est pourquoi nous devons inciter les joueurs à s’assurer que leurs actions soient claires pour quiconque regarde leur partie, pas seulement leur adversaire._

Dans ces situations, l’arbitre doit s’assurer que la partie continue normalement.

> _Si les joueurs réparent une erreur minime par eux-mêmes, assurez-vous que celle-ci ne vienne pas à s’aggraver. S’ils jouent d’une façon qui n’est claire que pour eux, vérifiez que l’un des deux ne profite pas de la confusion pour en tirer un avantage illégitime._  
> 

Des infractions plus importantes se gèrent en identifiant tout d’abord quelle infraction s’applique, puis en suivant les instructions correspondantes.

> _Cette phrase expose deux notions importantes. D’une part, un arbitre doit intervenir dès que l’erreur ne peut être qualifiée de « minime ». D’autre part, il ne faut pas commencer par la pénalité pour tenter d’en déduire l’infraction. On identifie quelles actions ont eu lieu, puis quelle est l’infraction, et enfin on détermine la pénalité. On ne donne pas un Game Loss à un joueur parce que son erreur semble mériter un Game Loss._

Seul le Head-Judge a le droit d’infliger des pénalités différentes de celles décrites dans ce guide.

> _Quand il y a plusieurs arbitres, le Head-Judge est celui qui a la responsabilité finale du tournoi. Il est le seul à pouvoir décider qu’une pénalité particulière ne convient pas à la situation présente. C’est le plus souvent l’arbitre le plus expérimenté ; quand il décide de dévier, c’est pour une bonne raison._

Le Head-Judge ne doit pas dériver des procédures indiquées dans ce Guide à moins que des circonstances exceptionnelles n’aient fortement contribué à créer le problème ou qu’aucune philosophie définie n’existe pour gérer la situation rencontrée.

> _Le Head-Judge peut dévier quand il le souhaite, mais on attend de lui qu’il sache quand c’est pertinent. La plupart du temps, c’est lorsque la situation ne correspond vraiment à aucune des catégories listées dans les pages du Guide des procédures et infractions de Magic. Le Head-Judge ne devrait dévier que si la situation est à la fois grave et exceptionnelle._

Ces situations sont rares : une table s’effondre, un booster contient des cartes d’une autre extension, etc.

> _Voilà quelques exemples de «circonstances exceptionnelles». Dans ces cas-là, le Head-Judge doit chercher la «meilleure solution» pour les joueurs. Les exemples ci-dessus sont tous deux graves et exceptionnels. Assurez-vous que la situation que vous rencontrez remplit bien ces deux critères avant d’envisager de dévier._

Le niveau d’application des règles, l’avancée du tournoi, l’âge ou le niveau d’expérience du joueur, la volonté d’éduquer le joueur, le niveau de certification de l’arbitre ne sont **en aucun cas** des circonstances exceptionnelles.

> _Pour certaines de ces situations, on peut être tenté de dévier, mais il ne faut pas le faire. Vous devez appliquer les règles, qu’il s’agisse de la première ou de la dernière ronde, que l’appel vienne de la table 1 ou de la table 1001. Il se peut que l’un des joueurs soit très jeune, mais la différence d’âge ne change pas les règles et règlements. C’est peut-être un tout nouveau joueur, qui ignore qu’il est interdit de lancer un dé pour déterminer le vainqueur d’une partie : il doit être disqualifié, même si cela ne vous semble pas très «juste». Enfin, être un arbitre de niveau 3 ou plus ne donne pas le droit de dévier. Ces arbitres sont tenus de respecter encore plus strictement les critères, parce que des arbitres de moindre niveau les observent et apprennent de leurs actions._

Si un arbitre pense qu’il serait approprié de dévier de ce guide, il doit en référer au Head-Judge.

> _Seul le Head-Judge est autorisé à dévier, mais un arbitre de salle peut le lui suggérer. Par contre, un arbitre de salle ne doit jamais dévier, sauf sur décision expresse du Head-Judge._

Les arbitres sont des êtres humains et font des erreurs. Lorsqu’un arbitre fait une erreur, il doit la reconnaître, s’en excuser auprès des joueurs, et la corriger s’il n’est pas trop tard.

> _En dépit de tous les efforts pour dresser des Golden Retrievers, les arbitres sont encore tous humains._
> 
> Pour l’instant.
> 
> Les humains commettent des erreurs. C’est la vie. Personne ne peut avoir raison 100% du temps et il n’est pas réaliste de l’espérer. Quand vous commettez une erreur, vous devez l’assumer et la réparer si vous le pouvez. On ne peut pas laisser un joueur avoir une fausse information parce qu’un arbitre s’est trompé. A minima, vous devez présenter vos excuses aux joueurs pour votre erreur. Parfois il vaut mieux le faire sur-le-champ, parfois il vaut mieux le faire après la fin du match. La plupart du temps, les joueurs se montreront compréhensifs, même quand votre erreur était en leur défaveur.  
> 
> 
> 

Si un officiel du tournoi donne à un joueur une information erronée qui le fait commettre une infraction, le Head-Judge est autorisé à diminuer la pénalité.

> _Nous voulons que les joueurs fassent confiance aux officiels du tournoi. Ils doivent pouvoir suivre les instructions et les informations qui leur sont données. Il serait injuste de les pénaliser pour leur confiance envers des gens à qui ils sont justement censés faire confiance. Toutefois, la décision de diminuer la pénalité appartient toujours au Head-Judge. Deux conditions doivent être remplies : 1) un arbitre ou un autre officiel de tournoi donne une information erronée et 2) une infraction est commise en conséquence directe de cette information erronée._

Par exemple, un joueur demande à un arbitre si une carte est légale dans un format et se voit répondre que oui. Lorsqu’on découvre que le deck de ce joueur est illégal à cause de cette carte, le Head-Judge applique la procédure normale pour corriger la liste de deck, mais peut diminuer la pénalité en un Warning du fait de l’erreur directe de l’arbitre.

> _Voici d’autres exemples possibles :  
> – Réduire une Game Rule Violation de Warning à aucune pénalité si un arbitre dit à un joueur qu’il peut jouer [Mise en danger](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Mise+en+danger&width=223&height=310) sur la [Foudre](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Foudre&width=223&height=310) de son adversaire pour la rediriger sur un planeswalker de cet adversaire.  
> – Réduire un Deck/Decklist Problem de Game Loss à Warning si un arbitre a dit au joueur qu’il pouvait se contenter d’écrire «Jace» sur sa liste de deck en Legacy._