Les erreurs de tournoi (Tournament Errors) sont des infractions aux [règles de tournoi de **Magic**](http://blogs.magicjudges.org/rules/mtr/) (Magic Tournament Rules ou MTR).

> De même que les erreurs de jeu (Game Play Errors) sont des infractions aux Règles Complètes, les erreurs de tournoi (Tournament Errors) sont des infractions aux Magic Tournament Rules. Cependant toute violation des règles de tournoi ne constitue pas forcément une infraction.

Si l’arbitre estime que l’erreur a été commise de manière intentionnelle, il doit envisager l’infraction [Conduite antisportive — Triche](http://blogs.magicjudges.org/rules/ipg4-8-fr/). Les révisions précédentes de ce document indiquaient dans chaque section comment gérer une erreur intentionnelle ; à l’exception du [rythme de jeu trop lent](http://blogs.magicjudges.org/rules/ipg3-3-fr/) (Slow Play), toutes les erreurs intentionnelles doivent être potentiellement considérées comme des infractions de la [Triche](http://blogs.magicjudges.org/rules/ipg4-8-fr/).

> _Ce passage parle des cas où le joueur enfreint intentionnellement une règle de tournoi. Le joueur pourrait être en train de tricher mais cela pourrait ne pas correspondre à la définition de la triche dans l’IPG. Prenez la peine de réviser la [section 4.8 sur USC – Cheating](http://blogs.magicjudges.org/rules/ipg4-8-fr/) avant d’agir._

Si un joueur enfreint les règles de tournoi de Magic d’une manière qui n’est pas prise en compte par l’une des infractions listées ci-dessous, l’arbitre doit expliquer au joueur quelle est la procédure appropriée mais il ne doit pas attribuer de pénalité.

> _Seules les plus graves violations des règles de tournoi de Magic méritent une pénalité. Un manquement envers ces règles peut sérieusement pertuber le déroulement global du tournoi, en engendrant un retard anormal ou en conférant à un joueur un avantage significatif. Si une infraction au règles de tournoi de Magic n’est pas considérée comme nuisible par rapport au déroulement global de l’évènement, alors une pénalité n’est pas nécessaire : l’arbitre éduquera simplement le joueur par une action correctrice.  
> _

Un mépris persistant ou volontaire de ces règles peut nécessiter de plus amples investigations.

> _Bien que toutes les violations des règles de tournoi de Magic ne soient pas pénalisées, même la plus infime perturbation peut quand même ralentir un évènement si elle se répète encore et encore. Si cela se produit, il peut être nécessaire d’enquêter pour déterminer si la perturbation est faite pour gêner intentionnellement l’évènement ou si un joueur a peut-être simplement besoin de davantage d’encadrement.  
> _

Le deuxième Warning pour Tournament Error dans une même catégorie et tous les Warnings suivants sont augmentés en Game Loss.

> _Les erreurs relatives au tournoi sont généralement plus gênantes et moins « facile à commettre » que les erreurs relatives au jeu. En conséquence, la deuxième occurrence d’un Warning pour une Tournament Error de même catégorie est augmentée en Game Loss. Toutes les occurrences suivantes dans la même catégorie seront aussi des Game Loss. Nous ne continuons pas à augmenter la pénalité jusqu’à la Disqualification.  
> _

Lors des tournois qui durent plusieurs jours, le décompte des pénalités pour ces infractions est remis à zéro chaque jour.

> _Comme pour les erreurs relatives au jeu (Game Play Errors), il y a plus de risques que quelqu’un réitère une erreur de tournoi (Tournament Error) au cours des 15 rondes d’un Grand Prix que lors des 5 rondes d’un PPTQ. La réinitialisation du décompte des pénalités reflète cela.  
> _