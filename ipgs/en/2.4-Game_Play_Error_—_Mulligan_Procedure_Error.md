Definition
----------

A player makes an error in one of the steps of the mulligan process. Once the mulligan process is complete and the game begins, any excess cards arising from an improper mulligan should be handled as Game Play Error – Hidden Card Error.

> 
> 
> **Penalty**
> 
> Warning
> 
> 

  

> _While this infraction is called Mulligan Procedure Error, it can apply before any mulligans have been taken._

Unintentional process errors that provide no advantage, such as declaring an intent to mulligan early or exiling too many cards to [Serum Powder](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Serum+Powder&width=223&height=310), are not an infraction.

> _Try not to overthink this infraction. If a player does something wrong that isn’t listed in the examples, compare the impact of that error and decide if it’s trivial or not._  
> _**Examples:**_
> 
> *   A player on the draw declares that they will keep before the player on the play
> *   One player puts a Leyline onto the battlefield before their opponent finishes their mulligans
> 
> Technically these are procedural errors, but at this time in the match the impact is very low and should not be treated as an infraction.  
> 
> 
> 

Examples
--------

*   **A.** A player draws eight cards at the start of the game (instead of seven).
*   **B.** A player chooses to not take a mulligan then takes a mulligan after seeing their opponent choose to take a mulligan.

Philosophy
----------

Errors prior to the beginning of the game have a less disruptive option—a forced mulligan—that is not available at any other point during the game.

> _Even with the new London Mulligan it can still be easy to make an error in the pre-game procedure, drawing too many cards the most common one, so we have the player take an additional mulligan.  
> Note that counting cards face down onto the table does not count as drawing, encourage this practice to count the cards before drawing, so any extra cards counted onto the table can be returned to the top of the library._  
> 

Additional Remedy
-----------------

The player takes an additional mulligan.

> _If the error were examples C or D, the player does have the correct number of cards in hand, so the only option for the player is to mulligan again. The other examples will result in too many cards, so the player can choose which of the remedies to take. The two options are pretty straightforward: reveal their hand and have the opponent choose the excess cards to shuffle back, or take another mulligan._
> 
> _This is self explanatory, the player shuffles their hand and redraws, which means they will be returning an additional card to the bottom of the library._  
> 
> 
>