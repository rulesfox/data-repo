Definition
----------

This infraction covers the majority of game situations in which a player makes an error or fails to follow a game procedure correctly. It handles violations of the [Comprehensive Rules](http://blogs.magicjudges.org/rules/cr/) that are not covered by the other Game Play Errors.

> 
> 
> **Penalty**
> 
> Warning
> 
> 

  

> _“Game Rule Violation” does not refer to any particular type of error. Rather, Game Rule Violations are specifically defined as errors that aren’t another infraction. Newer judges will sometimes talk about how “mis-resolving a spell” is a Game Rule Violation. In actuality, resolving a spell improperly could result in one of several different infractions, such as [Looking at Extra Cards](http://blogs.magicjudges.org/rules/ipg2-2/) (e.g. forgetting that [Courser of Kruphix](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Courser+of+Kruphix&width=223&height=310) left the battlefield) to Hidden Card Error (e.g. mis-reading [Divination](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Divination&width=223&height=310) and drawing three cards).  
> _

Examples
--------

*   **A.** A player casts [Wrath of God](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Wrath+of+God&width=223&height=310) for 3W (actual cost 2WW).
*   **B.** A player does not attack with a creature that must attack each turn.
*   **C.** A player fails to put a creature with lethal damage into a graveyard and it is not noticed until several turns later.
*   **D.** A [Phyrexian Revoker](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Phyrexian+Revoker&width=223&height=310) is on the battlefield that should have had a card named for it.
*   **E.** A player casts [Brainstorm](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Brainstorm&width=223&height=310) and forgets to put two cards back on top of thier library.

Philosophy
----------

While Game Rule Violations can be attributed to one player, they usually occur publicly and both players are expected to be mindful of what is happening in the game. It is tempting to try and “fix” these errors, but it is important that they be handled consistently, regardless of their impact on the game.

> _Consistency is a core principle of the IPG. Even though there are thousands of judges adjudicating tournaments across the world, it is important for each of these tournaments to be run and judged to the same standard. For this reason, we strive to handle penalties neutrally. Fundamentally, both players are responsible for maintaining a proper game state. Our core role as judges is not to “correct” or “fix” the players’ mistakes, but to dispassionately interpret and apply the fixes prescribed by the IPG._

Additional Remedy
-----------------

First consider a simple back up (see section [1.4](http://blogs.magicjudges.org/rules/ipg1-4/))

If a simple backup is not sufficient and the infraction falls into one or more of the following categories, and only into those categories, perform the fix specified unless a simple backup is possible:

> So right here we are going to have a list of partial fixes. Try to do a simple backup (see [Backing Up 1.4](http://blogs.magicjudges.org/rules/ipg1-4/) for more information about back ups) then, if you can’t, you can try to do a partial fix. If you cannot apply a simple backup or a partial fix  you evaluate if you need to do a back up or leave alone. If the error fits more than one of these categories, it’s ok to partial fix, but be careful that the error only fits into those categories. If there are multiple fixes within the same bullet point, you’re still fine to use that partial fix.

*   If a player forgot to untap one or more permanents at the start of their turn and it is still the same turn, untap them.> _Out-of-Order-Sequencing has always been a viable option when this type of situation arises, and it does so with some regularity. Players get excited, or simply lose focus and forget to untap everything before advancing into the turn. If that happens, we simply issue the warning and fix the mistake, by untapping those permanents now._
*   If a player made an illegal choice (including no choice where required) for a static ability generating a continuous effect still on the battlefield, that player makes a legal choice.  
    > _This partial fix refers to cards such as [True-Name Nemesis](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=True-Name+Nemesis&width=223&height=310), [Voice of All](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Voice+of+All&width=223&height=310) or a [Siege](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Outpost+Siege&width=223&height=310) from Fate Reforged™, which require the choice of a player or color as they enter the battlefield. The reasoning for this is similar to why we apply state-based actions — it is impossible for these cards to exist on the battlefield without a choice being made for them, so we correct that immediately. While this could lead to the perception of advantage for one player, such errors always occur publicly, so it is in both player’s interest to be attentive.__So, if Nick casts [Doom Blade](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Doom+Blade&width=223&height=310) on Albert’s [Voice of All](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Voice+of+All&width=223&height=310), we can have Albert chose a color now. If Albert chooses Black, Doom Blade is now illegal. Then, we can do a simple backup to just before Doom Blade was cast.  
    > _
*   If a player failed to draw cards, discard cards, or return cards from their hand to another zone, that player does so.  
    > _Players will generally be able to determine with high accuracy if they failed to draw or discard cards. This also includes bounce-type effects. Note that this partial fix does not expire, even if the error was many turns ago._
*   If an object is in an incorrect zone either due to a required zone change being missed or due to being put into the wrong zone during a zone change, the exact object is still known to all players, and it can be moved with only minor disruption to the current state of the game, put the object in the correct zone.  
    > _There is a lot going on in this sentence, so let’s deconstruct it._  
    > _– “Object in the incorrect zone due to a required zone change being missed”: This is for where a creature was supposed to die and didn’t, or a card was supposed to be milled and didn’t, Or a creature that is supposed to be bounced and isn’t._  
    > _– “or due to being put into the wrong zone during a zone change”: Most commonly this is when a card is supposed to be put in the graveyard, but is put in exile, or vice versa._  
    > _– “the identity of the object was known to all players”: This means that all players know what the object is. This could be because the card was revealed or it could be because the card is in a location known by both players such as the top of a library._  
    > _– “and it can be moved with only minor disruption to the current game state”: Things disappearing from the battlefield mid-combat tends to be disruptive. Be sure to look at what decisions and actions are currently being made based on that card being on the battlefield. __Additionally, this partial fix does not include fixing things that shouldn’t have moved but did. For example, a 4/4 with 3 damage is put into the graveyard, and later discovered that it shouldn’t have been. This partial fix does not include returning it to the battlefield._
*   If damage assignment order has not been declared, the appropriate player chooses that order.  
    > _This is seldom relevant. Most times, when there are multiple blockers, it’s clear what the intent is, and there is no real interaction. However, it can become relevant, and we now have this partial fix. Ned may get upset that Abe gets to declare blocking order in the middle of resolving spells; when it suddenly becomes relevant, however, most of the time it becomes relevant as a result of Ned casting a spell. If Ned is doing something that makes blocking order relevant, they cannot assume that whatever blocking order favors him the most is the correct one. They have the responsibility to clarify the ambiguity prior to it becoming relevant._

For each of these fixes, a simple backup may be performed beforehand if it makes applying the fix smoother. Triggered abilities are generated from these partial fixes only if they would have occurred had the action been taken at the correct time.

> A simple backup is backing up the last action completed (or one currently in progress) and is sometimes used to make another portion of the prescribed remedy smoother. A simple backup should not involve any random elements. Also while applying a partial fix consider the triggered abilities if they would have occurred while taking the action at the correct time.
> 
> 

Otherwise, a full backup may be considered or the game state may be left as is.

> _So, we look to see if any of the partial fixes apply, and if not, we either rewind or don’t. Please see [1.4 Backing Up](http://blogs.magicjudges.org/rules/ipg1-4/) for information on if it’s appropriate to back up._

For most Game Play Errors not caught within a time that a player could reasonably be expected to notice, opponents receive a [Game Play Error — Failure to Maintain Game State](http://blogs.magicjudges.org/rules/ipg2-6/) penalty.

> _This is simply the definition of [Game Play Error – Failure to Maintain Game State](http://blogs.magicjudges.org/rules/ipg2-6/), and goes back to the concept that keeping the game in a legal and clear state is both players responsibility._

If the game has proceeded past a point where an opponent could reasonably be expected to notice the error, the opponent has also committed an infraction. In most cases, the infraction is Game Play Error — Failure to Maintain Game State. However, if the judge believes that both players were responsible for the Game Rule Violation, such as due to the opponent controlling the continuous effect modifying the rules of the game that led to the Game Rule Violation or a player taking action based on another player’s instruction, they have instead committed a Game Play Error — Game Rule Violation. For example, if a player casts [Path to Exile](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Path+to+Exile&width=223&height=310) on an opponent’s creature and the opponent puts the creature into the graveyard, once the game has continued both players will have committed a Game Rule Violation.

> _As always, both players are responsible for maintaining a clear game state. If my card tells you to take an action, and you do it incorrectly, whose fault is it? Yours for doing the action incorrectly, or mine for not making sure my spell resolved correctly? Turns out, in this case, it’s reasonable to say we are both equally at fault. Fundamentally if the judge thinks that both players share the responsibility for the error, because of continuous effects or both participating in the action, then both players receive this penalty._
> 
> _Note that this double GRV only applies \*after\* the game has moved on from the point of the error. If, in the example above, one player puts the creature into the graveyard, it’s not a double GRV until the game continues on. Instead, if one player points out the error, play can continue on with the error corrected and no infraction assessed._
> 
>