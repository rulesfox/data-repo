Game Play Errors are caused by incorrect or inaccurate play of the game such that it results in violations of the [Magic Comprehensive Rules](http://blogs.magicjudges.org/rules/cr/).

> _This is the first of three broad categories of infractions. This covers unintentional violations of the Comprehensive Rules — errors in actually playing the game, and not violations of tournament policy or other negative behavior._
> 
> _These are errors committed by at least one player during a match by unintentionally violating a Comprehensive Rule. Game Play Errors can occur for many reasons. Players get tired, get distracted, play too fast, or don’t know the cards or the rules that apply to a complex situation well enough. These situations are not exceptional, which is why Game Play Errors are a common category of error._
> 
> 

Many offenses fit into this category and it would be impossible to list them all.

> _Given the complexity of the game, it is impossible to make a list of all the types of errors that can occur, so we, as judges, don’t try. We want this document to be understandable and learnable. If we list everything and handle all special cases, this document would be hundreds of pages long and unusable. Instead, we divide these errors into general categories, known as the 5 Game Play Errors defined by the IPG._

The guide below is designed to give judges a framework for assessing how to handle a Game Play Error.

> _While it may be difficult, at first glance, to see which category that some particular infraction falls into, careful reading of the entire infraction — the definition, philosophy, and remedy, can help make the determination._

Most Game Play Error infractions are assumed to have been committed unintentionally.

> _Errors committed intentionally, of course, may fall into a different category altogether: [Unsporting Conduct — Cheating](http://blogs.magicjudges.org/rules/ipg4-8/). However, it’s important to notice that not all game errors are cheating. In fact, very few actually are._
> 
> _We like to assume players are nice, and when we walk up to a table, we aren’t accusing people of cheating. That might change once we ask a few questions, but when we start out, our baseline assumption is that we are dealing with an honest mistake._
> 
> 

If the judge believes that the error was intentional, they should first consider whether an [Unsporting Conduct — Cheating](http://blogs.magicjudges.org/rules/ipg4-8/) infraction has occurred.

> _This is the other side of the coin; even if the task of a judge is always to help the players, we must never forget that they can lie or cheat to get an advantage. The experience and advice of more experienced judges can help to frame the situation correctly and to find out if a player was aware of committing an offense or not._

With the exception of [Failure to Maintain Game State](http://blogs.magicjudges.org/rules/ipg2-6/), which is never upgraded, the third or subsequent penalty for a Game Play Error offense in the same category should be upgraded to a Game Loss. For multi-day tournaments, the penalty count for these infractions resets between days.

> _We want players to learn something from their mistakes and take care to not commit them again in the future. If a player repeatedly makes mistakes, the Warning is not doing its job of reinforcing the lesson, and therefore we must increase the severity of the penalty to a Game Loss. When giving a Game Play Error to a player, be sure to ask if they have received the infraction before. Note that this is the third or subsequent penalty, not Warning. If a player has received two Game Rule Violations that have been upgraded to Game Losses, and they commit a third Game Rule Violation, that is still upgraded to a Game Loss as well._
> 
> _Regarding the [Failure to Maintain Game State](http://blogs.magicjudges.org/rules/ipg2-6/) infraction, be prepared for some players to not understand why they are getting a Warning. “But judge, I didn’t do anything wrong?” Take a few seconds to explain to the player why they are getting the Warning, and if they still wishes to discuss it, you can talk about it after the match. While they still get the Warning, we do not upgrade this infraction as we do other tournament errors. This is because we do not want players to fear calling a judge. Being awarded a Game Loss because my opponents made play mistakes and I didn’t catch doesn’t make sense. And, if this is the third time that my opponent has made a play mistake that I didn’t catch over the course of the tournament, I might be reluctant to call a judge and have my Failure to Maintain Game State upgraded, so I choose to pretend that I didn’t notice. We don’t want our policy to encourage cheating. If we don’t upgrade this penalty, though, why give Warnings at all? There are two reasons: the first is that the act of receiving a Warning is generally enough to remind a player to pay more attention. The second is so we can track them. If a player tends to get Failure to Maintain Game State a lot, and the related error is always in their favor, this gives judges the ability to track these infractions — and when added to the larger infraction database, we can track across events too._
> 
> 

For multi-day tournaments, the penalty count for these infractions resets between days.

> _Multi-day tournaments reset between days because it was determined to be unreasonable that the upgrade path was held at three regardless of the number of rounds in an event — it’s much easier to accumulate three GPEs over a fifteen round Grand Prix than a five round MCQ._