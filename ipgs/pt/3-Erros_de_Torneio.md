_Erros de torneio são violações das Regras de Torneio de Magic._

> _Assim como Erros de Jogo são violações das Regras Abrangentes, os Erros de Torneio são violações das Regras de Torneio. No entanto, nem toda violação das regras de torneio resulta em uma infração._

_Se o juiz entender que o erro foi intencional, ele deve considerar Conduta Antidesportiva – Trapaça. (Versões anteriores do IPG faziam referência em cada seção à forma como lidar com uma violação intencional. Com a exceção de Jogo Lento, todas as violações intencionais serão agora interpretadas como potencial Conduta Antidesportiva – Trapaça.)_

> _Este parágrafo contempla casos em que um jogador quebra uma regra de torneio intencionalmente. O jogador pode estar cometendo uma infração (até mesmo propositalmente), porém esta pode não constituir Trapaça segundo as definições do IPG. Certifique-se de revisar a seção 4.8 Conduta Antidesportiva – Trapaça antes de tomar alguma atitude._

_Se um jogador viola as Regras de Torneio de Magic de uma forma que não está prevista neste documento, o Juiz deve explicar o procedimento correto para o jogador mas não aplicar nenhuma penalidade._

> _Somente as Regras de Torneio de Magic mais importantes exigem uma penalidade quando quebradas. Violações dessas regras podem atrapalhar o andamento geral do torneio, provocando atrasos, ou podem conferir vantagem indevida a algum jogador. Se uma violação das Regras de Torneio de Magic não é considerada prejudicial ao andamento do torneio (e, portanto, não estiver coberta neste documento), então nenhuma penalidade se faz necessária, e o juiz deve adotar uma postura puramente educativa._

_Se o comportamento continuar inadequado ou se o jogador desdenhar as instruções, uma investigação pode ser necessária._

> _Ainda que nem toda violação das Regras de Torneio de Magic seja passível de penalização, mesmo o menor dos deslizes praticado de maneira repetitiva pode atrapalhar o evento. Se isto acontecer, uma investigação pode ser necessária para verificar se a ação está sendo praticada de forma deliberada, de maneira a atrapalhar o evento, ou se um jogador apenas necessita de um melhor direcionamento._

_A segunda ou subsequente Advertências por ocasião de um Erro de torneio da mesma categoria é aumentada para uma Perda de Jogo._

> _Erros de Torneio são tipicamente mais prejudiciais e mais difíceis de se cometer que Erros de Jogo. Como resultado, a segunda instância de uma Advertência originada do mesmo Erro de Torneio deve ser convertida em uma Perda de Jogo. Todas as instâncias subsequentes serão penalizadas por Perda de Jogo. O aumento da penalidade não progride até se chegar a uma Desqualificação._

_Para eventos realizados em mais de um dia, a contagem de penalidade para essas infrações é zerada entre os dias_

> _Assim como se verifica com Erros de Jogo, existe uma chance maior de que um jogador repita um Erro de Torneio em um Grand Prix de 15 rodadas do que em um PPTQ de 5. Reiniciar a contagem de penalidades entre dias reflete esse entendimento._