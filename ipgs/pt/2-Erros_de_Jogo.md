Erros de Jogo são causados por jogadas incorretas ou imprecisas de tal forma que resulta em violações das Regras Abrangentes de Magic.

> _Esta é a primeira de três amplas categorias de infrações. Cobre violações não intencionais das regras abrangentes – erros ao jogar o jogo, propriamente dito, e não violações das políticas de torneio ou outros comportamentos negativos._
> 
> _Estes são erros cometidos durante uma partida, quando pelo menos um jogador viola involuntariamente as regras abrangentes. Erros de Jogo podem ocorrer por várias razões: jogadores ficam cansados, distraídos, jogam muito rápido, não conhecem algum card ou não compreendem completamente como as regras se aplicam a uma situação mais complexa. São situações nada excepcionais, e por isso mesmo os Erros de Jogo são uma categoria comum de erros._
> 
> 

Muitas infrações se enquadram nesta categoria e seria impossível enumerar todas.

> _Dada a complexidade do jogo, seria impossível listar todos os tipos diferentes de erros que podem ocorrer, então nós, como juízes, nem tentamos. Queremos que este documento seja compreensível e fácil de se aprender. Se nós listássemos tudo que há para listar e tratássemos todos os casos especiais, este documento teria centenas de páginas e ninguém conseguiria usá-lo. Em vez disso, nós dividimos esses erros em categorias gerais, conhecidas como os 6 Erros de Jogo definidos pelo IPG._

O guia abaixo foi projetado para dar aos juízes uma estrutura de como proceder com um erro de jogo.

> _A despeito da dificuldade de descobrir à primeira vista em qual categoria uma infração específica se encaixa, a leitura cuidadosa da infração no IPG – definição, filosofia e solução – pode ajudar com essa identificação._

A maioria das infrações de erro de jogo é tratada como se tivessem sido cometidas sem a intenção de cometê-las.

> _Erros cometidos intencionalmente podem, é claro, pertencer a uma categoria bem diferente: Conduta Antidesportiva – Trapaça. No entanto, é importante notar que nem todos os erros ocorridos durante um jogo são trapaça. Na verdade, a grande maioria não é._
> 
> _Nós gostamos de acreditar que jogadores são honestos e, quando atendemos a uma mesa, não acusamos os jogadores de Trapaça. Isso pode mudar assim que fizermos algumas perguntas, mas quando nós começamos, nossa hipótese inicial é a de que estamos lidando com um erro não intencional._
> 
> 

Se um juiz acredita que o erro foi intencional, ele deve primeiro considerar se ocorreu uma infração por Conduta antidesportiva – Trapaça.

> _Este é o outro lado da moeda; mesmo que a tarefa dos juízes seja de sempre ajudar os jogadores, nós não podemos nos esquecer de que eles podem mentir ou trapacear para tentar ganhar uma vantagem. A experiência e o conselho de juízes mais experientes podem nos ajudar a enquadrar a situação corretamente e a descobrir se o jogador estava ciente ou não de ter cometido uma ofensa._

Com exceção de Falha em Manter o Estado de Jogo, que nunca é aumentada, a terceira ou subsequente infração de Erro de Jogo da mesma categoria é aumentada para Perda de Jogo. Para eventos em vários dias, as penalidades que contam para estas infrações reiniciam entre os dias.

> _Nós queremos que os jogadores aprendam algo com seus erros e que cuidem de não cometê-los novamente no futuro. Se um jogador comete erros repetidamente, quer dizer que a Advertência não está fazendo seu trabalho de reforçar a lição. Assim, a gravidade da penalidade deve ser aumentada para uma Perda de Jogo. Quando for aplicar uma infração a um jogador, lembre-se de perguntar a se ele já foi penalizado por aquela infração antes, no torneio atual. Em torneios que duram mais de um dia, essa contagem se reinicia de um dia para o outro, pois verificou-se que era injusto que o caminho para o aumento de penalidade fosse o mesmo (três infrações na mesma subcategoria de Erro de Jogo), sem levar em consideração o número de rodadas do torneio – é muito mais fácil acumular três Erros de Jogo em um Grand Prix de 15 rodadas do que um PPTQ de 5._
> 
> _Por fim, lembre-se de que são a terceira e as subsequentes **penalidades** – não Advertências – que ensejam o aumento. Se um jogador cometeu duas Violações de Regras de Jogo que foram aumentadas para uma Perda de Jogo cada, e aquele jogador comete, na sequência, uma outra Violação de Regra de Jogo, a penalidade desta também será aumentada para uma Perda de Jogo._
> 
> _A respeito da infração Falha em Manter o Estado de Jogo, esteja preparado para alguns jogadores que não vão entender o porquê de estarem recebendo uma penalidade. “Mas juiz, eu não fiz nada errado!” Use alguns momentos para explicar ao jogador a razão de ele estar recebendo uma Advertência, e, se ele ainda quiser argumentar ou discutir a respeito, diga a ele que vocês podem conversar depois da partida. Ainda que haja a aplicação de uma Advertência, nós nunca aumentamos a penalidade por Falha em Manter o Estado de Jogo. Isto se dá porque nós não queremos desencorajar os jogadores de chamar um juiz. Receber uma Perda de Jogo porque meu oponente cometeu Erros de Jogo e eu não percebi causa uma sensação péssima. E, se essa for a terceira vez durante o torneio em que um de meus oponentes cometeu um Erro de Jogo e eu não notei a tempo, eu posso ficar relutante em chamar o juiz que vai aumentar a penalidade da minha Falha em Manter o Estado de Jogo para uma Perda de Jogo, e, assim, escolher fingir que eu não vi nada. Nós não queremos que a nossa política estimule Trapaça. Mas, então, se nós não aumentamos essa infração, por que nós aplicamos Advertências? Por dois motivos: o primeiro é que o simples fato de o jogador receber uma Advertência costuma ser suficiente para que este passe a prestar mais atenção a seus jogos. O segundo é que essas infrações são registradas e monitoradas. Se um jogador vem cometendo repetidas Falhas em Manter o Estado do Jogo, e os erros correlatos são usualmente em seu favor, então registrá-las fornece aos juízes uma ferramenta para rastrear esse comportamento – e quando nós as adicionamos a um banco de dados maior, tal rastreamento passa a ser possível entre múltiplos eventos._
> 
>