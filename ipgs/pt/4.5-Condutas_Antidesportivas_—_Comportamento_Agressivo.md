**_Definição_**
---------------

_  
Um jogador age de maneira ameaçadora com outros ou sua propriedade._

> 
> 
> **Penalidade**
> 
> Desqualificação
> 
> 

  

> _Conduta Antidesportiva – Comportamento Agressivo é algo muito claro. Ao contrário de Conduta Anti-Desportiva Menor e Maior, aonde aplicamos infrações com base em coisas que deixam pessoas desconfortáveis ou pessoas usando linguagem que pode insultar/magoar (além de outras coisas), Conduta Antidesportiva – Comportamento Agressivo é baseado em ações físicas ou linguagem usada para sugerir uma intenção de causar prejuízo físico ou de intimidar fisicamente._

**_Exemplos_**
--------------

1.  _Um jogador ameaça bater em outro que se recusa a conceder para ele._
2.  _Um jogador puxa uma cadeira debaixo de outro jogador, fazendo com que ele caia._
3.  _Um jogador ameaça um juiz após receber uma regra._
4.  _Um jogador rasga um card pertencente a outro._
5.  _Um jogador intencionalmente derruba uma mesa._

> _Estes exemplos tornam bastante claro o que é Conduta Antidesportiva – Comportamento Agressivo. Também inclui ameaças sutis, como “Eu vou estar esperando por você no estacionamento quando sair esta noite”. Você não precisa ficar muito preso a palavras específicas – você sabe o que é uma ameaça quando você vê uma. Também é importante notar que, se um jogador prejudica a sua própria propriedade, enquanto que isso seja possivelmente assustador, isto muito provavelmente não é abrangido por Comportamento Agressivo e preferencialmente se encaixa em Conduta Antidesportiva – Menor – a menos que ele esteja fazendo isso de uma maneira que constitui uma ameaça._

**_Filosofia_**
---------------

_A segurança de todas as pessoas num torneio é de extrema importância. Não haverá tolerância para abuso ou intimidação física._

> _A razão pela qual aplicar uma desqualificação para este tipo de infração é porque todo mundo deve se sentir seguro em qualquer evento sancionado, e permitir que alguém continue em um evento após tal comportamento seria desencorajar os jogadores de querer participar de um evento novamente._

**_Solução Adicional_**
-----------------------

_O infrator deve ser pedido para se retirar do local pelo organizador._

> _Tenha em mente que a sua primeira prioridade é desarmar a situação. Como fazer isso depende da situação. No entanto, você não é um policial, e não é obrigado a se envolver fisicamente. Quando esta infração ocorre, o dia daquele jogador no evento acabou. Ele será desclassificado. Certifique-se de recolher uma declaração para o comitê de investigação se o jogador estiver disposto a dar uma, e não se esqueça de escrever a sua própria. É também aconselhável que o Organizador do Torneio peça ao jogador para deixar o local o mais rápido possível. Uma distinção importante aqui é que o Organizador do Torneio deve ser o único a realmente tomar esta decisão. Podemos incentivar o Organizador do Torneio a tomar essa decisão, mas nossa jurisdição não alcança além do âmbito do evento. Uma vez que o jogador não está mais envolvido com o evento ele já não é alguém que nós, como juízes, devemos estar lidando._