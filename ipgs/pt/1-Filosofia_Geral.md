**Juízes são árbitros neutros que seguem as políticas e regras.**

> _Este é, provavelmente, o conceito mais importante presente neste documento – juízes devem ser neutros. Um juiz favorecer algum dos jogadores durante uma chamada, seja por esse jogador ser seu amigo ou porque o juiz de alguma maneira gosta mais de um dos jogadores é algo impensável. Ser imparcial é um conceito fundamental no Código de Conduta dos Juízes. Juízes são vistos com respeito pois atuam de maneira neutra e aplicam as regras de forma igual a todos._

**Um juiz não deve intervir em um jogo a não ser que ele acredite que uma violação de regras tenha ocorrido, um jogador com uma preocupação ou dúvida peça assistência, ou o juiz deseje prevenir que uma situação se intensifique**

> _Os juízes estão presentes para os jogadores. Nossos serviços são necessários quando uma regra é violada, um jogador precisa de ajuda ou quando alguma situação delicada está ocorrendo e é necessário acalmar os jogadores. Quando sua assistência não é necessária, juízes não devem interferir em jogos. Ou seja, nenhum comentário sobre ações de jogo, nenhum tipo de conselho e nenhuma distração para os jogadores. Deixe os jogadores jogarem. Tenha em mente que isso não significa que você tenha que se comportar como um robô. Você ainda pode conversar com os jogadores, fazer piadas e dar risadas, só não interrompa os jogos._

**Juízes não impedem que erros de jogo aconteçam, ao invés disso, eles lidam com aqueles que ocorreram, penalizam quem violou regras ou políticas e promovem o jogo justo, a conduta desportiva e a diplomacia.**

> _Como em diversos esportes, juízes não previnem erros. No entanto, assim que uma infração ocorre, os juízes devem se apresentar e aplicar as correções e punições necessárias. Os jogadores não devem depender dos juízes para que algo ilegal não aconteça, pois juízes não podem prever o futuro e as ações de jogo acontecem de maneira acelerada. Na maioria dos casos, solucionar uma infração após ela ter ocorrido restaura o andamento correto do jogo. Esta política também se aplica quando se está observando uma partida no fim da rodada ou durante o Top 8. Também é importante que os juízes sirvam de bom exemplo de comportamento. Sua atitude e suas ações tem um impacto profundo no tom do evento. As pessoas devem ver em você o tipo de comportamento a ser espelhado._

**Juízes podem intervir de modo a prevenir ou antecipar que erros ocorram fora de um jogo.**

> _Apesar de ser quase impossível prever que uma infração dentro do jogo esteja para acontecer, algumas vezes é possível notar que uma infração fora do jogo está para acontecer. Nestes casos os juízes devem intervir e evitar que a infração ocorra. O “podem” na sentença não quer dizer que “o juiz pode fazer uma escolha” mas quer dizer que “o IPG/MTR permite que o juiz intervenha”. Isto demonstra a importância do serviço de atendimento ao consumidor e deixa bem claro que os juízes não tem uma escolha de quando intervir e prevenir este tipo de erro. Fora de um jogo, juízes sempre devem intervir para prevenir infrações, porém entendemos que nem sempre os juízes as percebem._
> 
> _Aqui estão alguns exemplos:_
> 
> *   _Um juiz vê um jogador embaralhando seu deck após o primeiro jogo, percebe que uma criatura que havia sido exilada se encontra na mesa e percebe que o jogador havia esquecido de retorná-la ao seu deck. O juiz intervém e lembra ao jogador de colocar a carta de volta no deck._
> *   _Em um torneio de formato selado, um jogador entrega uma lista de deck para o juiz, o juiz percebe que o jogador esqueceu de listar seus terrenos básicos; o juiz pede que o jogador liste os terrenos que estão sendo utilizados em seu deck._
> *   _Antes do início da rodada, um jogador vai aos juízes e entrega uma carta (como Pacifismo) que pertence ao seu oponente da rodada anterior, os juízes se esforçam para encontrar o dono do card porque assim poderá ser evitado que o jogador apresente um deck ilegal._
> *   _Antes do evento, um juiz percebe uma carta com uma alteração de arte questionável. O juiz lembra ao jogador que todas as cartas alteradas devem ter aprovação do juiz-mor antes do evento._
> 
> 

**O conhecimento do histórico de um jogador ou da habilidade do mesmo não altera a infração, mas pode ser considerado durante uma investigação.**

> _Nós não mudamos a infração baseados em quão bem um jogador é visto pela comunidade. Uma violação de regra de jogo gera uma advertência independente do jogador ser um veterano ou um iniciante. Uma vez que determinamos a infração, as punições são aplicadas sem nenhum enviesamento prévio. Um jogador que tem uma má reputação tem sua Violação de Regra de Jogo tratada da mesma maneira que um Juiz nível 3 jogando no evento. Uma vez reconhecida a infração, quem é o jogador não deve influenciar em nada. Porém o histórico do jogador pode influenciar na investigação. Por exemplo, um novato entendendo de maneira errônea como atropelar funciona é muito mais crível do que um jogador com experiência com o qual você já discutiu diversas vezes sobre como atropelar funciona. É possível que erros legítimos aconteçam, porém, as questões a serem levantadas durante a investigação poderão ser influenciadas pelo seu conhecimento do jogador._

**O propósito de uma penalidade é educar o jogador a não cometer erros similares no futuro.**

> _Penalidades não existem para que juízes sádicos usem suas habilidades de infringir dor em jogadores indefesos. Elas existem para reduzir a chance de um mesmo erro acontecer novamente. Um jogador que recebe uma penalidade por uma infração apresenta menor probabilidade de cometer o mesmo erro no futuro. De maneira geral, elas tem o intuito de atuarem de maneira tangível a reforçar a lição “eu perdi um jogo uma vez por este erro e não quero que isto ocorra novamente por algo que eu posso evitar facilmente, eu vou contar 60 toda vez que escrever minha lista de deck”. O intuito principal de uma penalidade não é acompanhamento, apesar de isto ser conveniente e aproveitável, a proposta é educativa._

**Isto é feito através de uma explicação de onde as regras ou políticas foram violadas e de uma penalidade para reforçar a educação.**

> _Na sentença acima nós estamos reforçando a importância da educação. Uma penalidade sozinha não alcança este objetivo. Você deve explicar brevemente o que o jogador fez de errado. De outra maneira, eles talvez não entendam o que ocorreu._

**Penalidades servem também para inibir que outros jogadores façam o mesmo, para educar cada outro jogador no evento e também são utilizadas para rastrear o comportamento de um jogador ao longo do tempo.**

> _É importante notar que você não necessita receber uma penalidade para reconhecer que você não quer recebê-la. Se um amigo perde um jogo importante por conta de uma penalidade, nós com certeza não queremos perder da mesma maneira. Nós aprendemos com os erros de nossos amigos assim como aprendemos com os nossos._
> 
> _Existe um arquivo, privado, de todas as penalidades. Assim como um arquivo, público, de todos os resultados. Este arquivo se torna interessante nos casos onde o jogador comete um número excessivo da mesma infração. Se um jogador recebe uma advertência por “Olhar Cards Extra – ele revelou os cards do deck do oponente enquanto cortava o deck antes do início da partida” vinte vezes consecutivas em um torneio, bom, você não acreditaria que ele está realizando isso de propósito e abusando da “a primeira vez é apenas uma advertência”?_
> 
> 

**Se uma pequena violação é rapidamente resolvida pelos jogadores com a satisfação de ambos, o juiz não precisa intervir.**

> _Juízes devem ser vistos como benefícios para os jogadores e existem pequenos erros que ocorrem durante os jogos que os próprios jogadores podem arrumar sem a ajuda de um juiz. Se o erro é pequeno e os jogadores o arrumam e se sentem satisfeitos, então os juízes não devem interferir no jogo._

**Se os jogadores estão jogando de uma maneira que é clara a eles mas pode causar confusão para um observador externo, o juiz pode pedir que os jogadores tornem a situação clara, mas sem aplicar nenhuma penalidade.**

> _Jogadores podem utilizar certos atalhos, ou usar marcadores para representar coisas estranhas, ou fichas erradas para determinada criatura. Estas ações podem ser claras para eles, mas não para os observadores (incluindo juízes). Se um jogador entende o que está ocorrendo e tudo está certo, não aplique uma penalidade, apenas peça que os jogadores joguem de maneira mais clara. É comum espectadores relatarem problemas que não são problemas de verdade. Por esta razão nós devemos encorajar os jogadores a serem claros não somente entre eles, mas também para qualquer pessoa que esteja observando o jogo._

**Em ambas as situações, o juiz deve assegurar que o jogo flua normalmente.**

> _Se os jogadores arrumam um pequeno erro por conta própria de maneira que seja clara para eles, mas não seja clara para os observadores, fique perto e garanta que nada fora do esperado esteja ocorrendo e que um dos jogadores não esteja tomando vantagem da confusão ou o erro por si só não esteja a causando._

**Violações mais graves são tratadas primeiro identificando-se qual a infração é aplicável e então procedendo com as instruções correspondentes.**

> _Esta sentença requer atenção dobrada. É um lembrete para que analisemos bem a situação antes de aplicar a penalidade. Nós identificamos as ações ocorridas, qual foi a infração e somente então determinamos a penalidade. Nós não aplicamos uma perda de jogo somente porque o erro de alguém parece valer uma Perda de Jogo._

**Somente o Juiz-Mor é autorizado a aplicar penalidades que desviam dessas diretrizes.**

> _Quando na presença de diversos juízes, o Juiz-Mor é o juiz responsável por todo o torneio; é o único com autoridade para determinar se uma penalidade ou solução especifica não se aplica bem na situação atual. Juizes-Mor, geralmente, são os juízes com maior experiência e quando decidem desviar do IPG geralmente é por uma boa razão._

** O Juiz-Mor não pode desviar dos procedimentos deste guia exceto em situações significativas e excepcionais ou em uma situação que não possui filosofia aplicável para orientação.**

> _Mesmo o juiz-mor tendo autoridade para desviar do IPG a qualquer momento que ele queria, ele também deve saber quando é apropriado realizar o desvio. O principal motivo para desvios é quando uma situação específica não se encaixa bem em nenhuma das categorias listadas nas vinte páginas do IPG. O único caso onde um desvio é justificável e aceitável é quando uma situação é significante e excepcional._

**Circunstâncias significativas e excepcionais são raras – uma mesa vira, um booster possui cards de uma coleção diferente, etc.**

> _Aqui você tem alguns exemplos de “circunstâncias excepcionais”. Nestes casos, usamos o senso comum e tentamos encontrar a “melhor solução” possível com os jogadores. Ambos exemplos são significantes e excepcionais. Tenha certeza que a situação que você está lidando seja ambas antes de considerar um desvio._

**O Nível de Aplicação de Regras, a rodada do torneio, a idade ou nível de experiência de um jogador, o desejo de educar o jogador, nível de certificação de um juiz NÃO são circunstâncias excepcionais.**

> _Em alguns destes casos pode parecer ok realizar um desvio, mas não é. Nós devemos manter as políticas independente de ser a primeira ou última rodada, se é uma chamada na mesa 1 ou na mesa 101. O oponente pode ser excepcionalmente jovem, mas a diferença de idade não é significante em termos de politica. O jogador pode ser novato e não sabe que rolar um dado para determinar o resultado de um jogo é proibido; aquele jogador ainda será desqualificado independente se você pensa se é justo ou não. E finalmente, ser um juiz nível 3 não dá o direito absoluto de desviar. Na verdade, eles estão associados a padrões mais estritos, pois juizes de nível menor estão observando e aprendendo com suas ações._

**Se outro juiz acredita que desviar seja adequado, ele deve consultar o Juiz-Mor.**

> _Só porque o único autorizado a desviar seja o juiz-mor, isso não significa que os juizes de salão não possam sugerir um desvio. Porém, como um juiz de salão, você nunca deve desviar._

**Juízes são humanos e cometem erros. Quando um juiz cometer um erro, ele deve reconhecer o erro, pedir desculpas aos jogadores e corrigi-lo, se não for tarde demais**

> _Apesar de todos os esforços para sermos perfeitos, nós juízes ainda somos apenas humanos. Por enquanto._
> 
> _Humanos cometem erros. É um fato da vida. Ninguém consegue ser 100% correto o tempo todo, e é surreal esperar o contrário. Porém, quando você cometer um erro você precisa assumi-lo e corrigi-lo, se possível. Os jogadores não podem pensar que algo incorreto que o juiz lhes disse é correto. Em todos casos, você precisa se desculpar pelo seu erro. Em alguns casos é melhor fazer isso imediatamente, em outros é melhor esperar o fim da partida. Mas se desculpe com ambos jogadores o mais rápido possível e corrija a situação. Jogadores, de maneira geral, são bem compreensíveis mesmo quando recebem o pior lado de uma ação errada._
> 
> 

**Se um membro da organização do torneio fornecer informação errônea que leve um jogador a cometer uma violação, o Juiz-Mor é autorizado a diminuir a penalidade.**

> _Nós esperamos que os jogadores confiem nos organizadores do torneio e sejam capazes de seguir as instruções/informações que fornecemos. Seria injusto penalizá-los por confiar nas pessoas que dizem a eles o que eles devem fazer. Porém, a decisão de diminuir a penalidade cabe apenas ao juiz-mor. Para esta cláusula nós necessitamos de duas coisas: 1) o juiz/oficial que forneceu a informação errada; 2) uma violação derivada diretamente de uma informação errônea._

**Por exemplo, se um jogador pergunta a um juiz se um card é legal em um formato e lhe é dito que sim. Quando se descobre que o deck daquele jogador é ilegal por causa deste card, o Juiz-Mor aplica o procedimento comum para corrigir a lista de deck, mas pode diminuir a penalidade para uma Advertência por causa do erro direto de um juiz.**

> _Outros exemplos incluem:_
> 
> *   _Diminuir uma Violação de Regra de Jogo para nenhuma penalidade se um juiz diz para uma jogadora que ela pode usar_ [_Harm’s Way_](http://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Harm%26%238217%3Bs+Way&width=223&height=310) _em um_ [_Lightning Bolt_](http://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Lightning+Bolt&width=223&height=310) _do oponente para redirecionar o dano a um planinalta do oponente._
> *   _Diminuir uma perda de jogo originada de um problema de Deck/Listagem de Deck para uma advertência se um juiz anterior disse que registrar somente “Jace” em sua lista era ok em um formato com mais de um card lendário com nome “Jace”._
> 
>