**_Definição_**
---------------

_  
Um jogador toma uma ação que possa ter lhe dado a oportunidade de ver as faces de cards em um grimório que ele não deveria poder ver.  
_

> 
> 
> **Penalidade**
> 
> Advertência
> 
> 

  

> _Esta infração é muito fácil de ser cometida. A todo momento que os jogadores mexem no baralho, é possível que um card caia, ou vire; ou, ao comprar, dois cards podem ficar presos ou serem pegos juntos._
> 
> _Geralmente, quando estamos falando de Olhar Cards Extras (OCE), estamos falando de um jogador ver a face de algum card de seu próprio grimório. No entanto, também existe a possibilidade de você ver alguns cards no grimório do seu oponente, enquanto o embaralha._
> 
> _Tenha cuidado para não confundir esta infração com a infração Erro de Card Oculto. Em particular, a infração de Erro de Card Oculto abrange situações em que você deveria olhar um determinado número de cards no topo de seu grimório, mas acaba olhando um número excessivo destes. Assim, neste caso específico, você está olhando cards extras, mas você não está a Olhar Cards Extras. Será que o seu cérebro dói? O meu sim._
> 
> _Para esclarecimento, derrubar um card enquanto você estiver embaralhando seu próprio baralho não é Olhar Cards Extras. Basta colocar o card de volta e continuar a embaralhar._
> 
> _Observar a face de um card caído ou virado de seu oponente também não é Olhar Cards Extras. Há duas razões para isso: 1) Se jogadores pudessem derrubar um card e fazer com que seu o seu oponente levasse uma Advertência por isso, eles iriam embaralhar desajeitadamente rumo a um top 8. 2) MTR 3.12 permite aos jogadores revelar informação oculta – como, por exemplo, os cards da mão – a seus oponentes, desde que tais jogadores tenham, eles próprios, acesso legal a essa informação._
> 
> _É importante notar que essa infração apenas se aplica para cards em um grimório. Se você acidentalmente olhar cards exilados por um_ [_Bomat Courier_](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Bomat+Courier&width=223&height=310)_, isso não é Olhar Cards Extra – isso é uma Violação de Regra de Jogo._
> 
> _Como mencionado na seção acima, Erro de Card Oculto tomou uma parte do espaço que Olhar Cards Extras costumava ter. Quando você manipula um conjunto de cards do topo do grimório, mas acessa um número excessivo destes, esta infração passa a ser considerada ECO. Para efeitos de ECO, uma vez que os cards comprados deixem significativamente o grimório como parte de uma compra, a infração não pode mais ser considerada Olhar Cards Extras._
> 
> _Olhar Cards Extras dá conta de quando você derrubar um card, deixar cair um card enquanto embaralha, iniciar a compra de um card indevidamente, ou mover cards em excesso do topo do grimório para o cemitério. Olhar Cards Extras não abrange olhar 8 cards durante um Revirar o Tempo, ou usar vidência 2 quando o correto seria usar apenas um vidência 1. Mover cartas do topo do grimório para o cemitério cabe em Olhar Cards Extras porque os cards estão sendo colocados em uma zona pública, enquanto Revirar o Tempo é coberto por Erro de Card Oculto porque os cards não estão em uma zona pública._
> 
> _Se você viu indevidamente a face de um card, mas não o adicionou a um outro conjunto, a infração é Olhar Cards Extras. O “significativamente separado do deck” está lá para que o juiz não tenha que lidar com a pergunta “eles viram ou não viram o card?”. Se houve uma separação “significativa” entre carta e deck, então estamos diante de Olhar Cards Extras. Pela explicação publicada com a mais recente mudança para essa infração, é enunciado que ‘’a chave \[para determinar o limite entre OCE e ECO\] é olhar para se o oponente tinha a intenção (por engano ou não) de pegar aquela quantidade de cards, ou se ele estava tentando pegar o número correto e falhou em fazê-lo graças a má destreza (em oposição à contagem errada!)’’_
> 
> _Conjuntos estão definidos na seção 1.5, então siga para lá se você precisa refrescar a memória. Basicamente, uma vez que o card que você está olhando é adicionado a um conjunto de cards, você não está mais olhando para ele, e a infração não é mais OCE, mas ECO ao invés disso._
> 
> _Às vezes jogadores colocam cards na mesa com a face virada para baixo antes de comprá-los, com o propósito de contar os cards ou para pensar antes de colocar os cards na mão. Isso não é proibido e juízes não devem penalizar isso. Nessa situação a diferença é que o jogador faz isso intencionalmente e toma cuidado para não ver nenhum card impropriamente. Essa afirmação também se aplica para a infração de Erro de Card Oculto._
> 
> 

_Essa penalidade é aplicada apenas uma vez se um ou mais cards forem vistos na mesma ação ou sequência de ações._

> _Nós não penalizamos um jogador para cada card visto se todos os cards forem vistos durante a mesma ação de jogo ou sequência de ações de jogo._
> 
> 

**_Exemplos_**
--------------

1.  _Um jogador acidentalmente revela (deixa cair, vira pra cima) o card enquanto embaralha o deck do seu oponente._
2.  _Um jogador pega um card extra enquanto está comprando de seu deck._
3.  _Um jogador vê o card do fundo de seu deck enquanto o apresenta para seu oponente para o corte/embaralhamento._

> 
> 
> _No Exemplo B, por favor considere “pega” como “levanta, mas não o bastante para separar significativamente do grimório”._
> 
> _Outros exemplos poderiam ser: Um jogador revela um card extra ao resolver uma habilidade de cascata. Um jogador revela cards extras ao mover cards do topo de seu deck para o cemitério, e o erro é notado imediatamente._
> 
> 

**_Filosofia_**
---------------

_É muito fácil um jogador acidentalmente olhar cards extras e essa infração lida com situações onde um erro de destreza ou de regras levou um jogador a ver cards que não deveria._

> _Jogadores mexem muito em seus decks. Mexem para embaralhar, mexem para comprar, mexem para procurar. A cada vez que um deck é tocado, surge a possibilidade de que alguém veja algo que não deveria. Considerando que é fácil de acontecer, fácil de perceber e fácil de corrigir, uma advertência é a penalidade adequada._

_Cards são considerados como estando em um grimório até que eles toquem em cards de outro conjunto. Uma vez que os cards se juntaram a outro conjunto, esta infração é considerada um Erro de Card Oculto ou Violação de Regras de jogo._

> _Todos sabemos que comprar é diferente de olhar, mas às vezes esta linha não é fácil de determinar. A linha aqui é quando o card que estamos olhando é adicionado a um outro conjunto de cards, seja a mão, sejam os outros sete cards de um Revirar o Tempo. Tenha em mente que, para esta infração, você é autorizado a se valer um pouco do seu julgamento quando se trata de “Eu quis pegar um card e em vez disso, peguei dois”. Isso ainda não é “acrescentar a um conjunto”._

_Jogadores não podem usar essa penalidade para obter um “embaralhamento gratuito” ou para forçar o embaralhamento de cards que ele não deseja comprar, podendo dessa forma caracteriza-se como Conduta Antidesportiva – Trapaça._

> _Este ponto envolve a capacidade do juiz para investigar se um jogador está mentindo. Alguns jogadores sabem que a correção adicional para Olhar Cards Extras é um embaralhamento, então eles podem “acidentalmente” olhar um card, a fim de tentar obter um embaralhamento. Isto é Trapaça, e ajuda a enfatizar o ponto que precisamos determinar se existem cards com posições conhecidas antes de aplicar a correção adicional. Quando você chegar à mesa faça algumas perguntas a ambos os jogadores para ter uma boa noção do que está realmente acontecendo._

_Os jogadores também não podem permitir o uso dessa penalidade como uma estratégia para gastar o tempo._

> _Embaralhar um grimório requer algum tempo, e os jogadores sabem disso. Como juízes, devemos evitar que os jogadores tirem proveito do limite de tempo, assim como temos que entender quando um jogador está tentando fazer isso. Isto também é considerado trapaça (Protelar, a bem da verdade), mas será muito difícil descobrir. Se a rodada estiver terminando, mantenha-se próximo e observe, caso você suspeite que este possa ser o caso._

_O deck já está aleatório, então embaralhar os cards revelados não deve envolver um esforço excessivo._

> _Isto significa que o jogador não tem que perder tempo embaralhando como ele faria nos procedimentos antes do jogo. Alguns embaralhamentos simples devem ser suficientes. O objetivo é “perder de vista” no grimório os cards ilegalmente vistos._

**_Solução Adicional_**
-----------------------

Embaralhar quaisquer cards desconhecidos na parte aleatória do grimório, e depois colocar cada card conhecido de volta em seus locais corretos.

> _Veja IPG 1.3 para detalhes sobre como embaralhar um deck. Mas o resumo é: descobrir quais cards são legalmente conhecidos por meio de coisas como vidência ou cascata, e, deixando esses cards de lado, embaralhe todos os cards que sobraram. Se os cards vistos acidentalmente eram previamente conhecidos (como os colocados de volta por uma Tempestade Cerebral), você não deve fazer este procedimento._