Turnierfehler sind Verstöße gegen die [**Magic**\-Turnierregeln](http://blogs.magicjudges.org/rules/mtr/).

> _So, wie Game Play Errors Verletzungen des Ausführlichen Regelwerks sind, sind Tournament Errors Verletzungen der Magic-Turnierregeln. Jedoch ist nicht jede Verletzung der Turnierregeln ein Vergehen.  
> _

Glaubt der Judge, dass der Verstoß absichtlich begangen wurde, sollte er zunächst überprüfen, ob es sich um [Unsporting Conduct — Cheating](http://blogs.magicjudges.org/rules/ipg4-8-de/) handelt. (Frühere Versionen des MIPG verwiesen in jedem Abschnitt, wie ein absichtlicher Regelverstoß zu behandeln ist. Mit Ausnahme von [Slow Play](http://blogs.magicjudges.org/rules/ipg3-3-de/) sollten nun alle absichtlichen Regelverstöße auf Unsporting Conduct — Cheating überprüft werden.)

> _Dieser Hinweis deckt Fälle ab, in denen ein Spieler vorsätzlich eine Turnierregel bricht. Der Spieler schummelt möglicherweise, aber es erfüllt unter Umständen nicht die Definition von Unsporting Conduct – Cheating des IPG. Überprüfe den Abschnitt zu [IPG 4.8 Cheating](http://blogs.magicjudges.org/rules/ipg4-8-de/), bevor du eine Entscheidung triffst._

Wenn ein Spieler gegen die **Magic**\-Turnierregeln auf eine Art und Weise verstößt, die nicht mit den unten aufgelisteten Verstößen abgedeckt ist, dann sollte der Judge dem Spieler den richtigen Ablauf erklären, aber keinen Penalty vergeben.

> _Nur die wichtigeren Magic-Turnierregeln rechtfertigen einen Penalty. Verstöße gegen diese Regeln können den Ablauf des gesamten Turniers stören, sodass die Veranstaltung unnötig in die Länge gezogen wird und/oder ein Spieler einen deutlichen Vorteil erlangt. Ist eine Verletzung der Magic-Turnierregeln für den Gesamtablauf des Turniers nicht nachteilig, ist kein Penalty nötig und der Judge klärt den Spieler lediglich über die korrekte Verhaltensweise auf.  
> _

Wenn diese Regeln wiederholt oder absichtlich nicht eingehalten werden, ist eine weitergehende Untersuchung nötig.

> _Auch wenn nicht jede Verletzung der Magic-Turnierregeln mit einem Penalty geahndet wird, kann selbst die kleinste Störung eine Veranstaltung verlangsamen, falls sie wiederholt vorkommt. In solch einem Fall sollte eine Untersuchung durchgeführt werden, um festzustellen, ob die Störung mit dem Ziel verübt wird, das Turnier absichtlich zu stören, oder ob der Spieler lediglich weitere Aufklärung benötigt.  
> _

Ein zweites oder weiteres Warning für einen Tournament Error in derselben Kategorie wird auf ein Game Loss heraufgestuft.

> _Tournament Errors sind normalerweise störender und nicht so “einfach zu begehen” wie Game Play Errors. Darum wird das zweite Warning für einen Tournament Error in derselben Kategorie zu einem Game Loss heraufgestuft. Alle weiteren Vorkommen in derselben Kategorie sind ebenfalls ein Game Loss. Wir stufen nicht weiter herauf, bis wir eine Disqualifikation erreicht haben.  
> _

Bei mehrtägigen Turnieren wird dieser Zähler für jeden Tag zurückgesetzt.

> _Wie bei Game Play Errors ist die Wahrscheinlichkeit, dass jemand einen Tournament Error wiederholt, bei einem Grand Prix mit 15 Runden größer als bei einem Grand Prix Trial mit nur 5 Runden; das Zurücksetzen des Penalty-Zählers soll dies berücksichtigen.  
> _