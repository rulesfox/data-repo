Definition
----------

Eine Person verstößt gegen eine Regel, die in den Turnierunterlagen festgelegt ist, lügt einen Turnier-Offiziellen an, oder bemerkt einen Regelverstoß, der  in seinem Match (oder in einem Match seiner Teamkameraden) begangen wurde und macht darauf nicht aufmerksam.

> 
> 
> **Penalty**
> 
> Disqualifikation
> 
> 

  

> _Dieser Satz enthält eine vollständige Liste aller Situationen, die für Unsporting Conduct – Cheating in Betracht kommen. Falls die zu Grunde liegende Situation in keine dieser drei Kategorien fällt, liegt kein Cheating vor. Zudem fallen alle absichtlichen Verletzungen der Turnierregeln unter Cheating (mit Ausnahme von Slow Play, das als [Stalling](http://blogs.magicjudges.org/rules/ipg4-7-de/) geahndet wird), und sollten nicht als bloße heraufgestufte Versionen anderer Verstöße behandelt werden._
> 
> _„Verstößt gegen eine Regel“ und „bemerkt einen Regelverstoß“ beinhalten Verstöße gegen das [Ausführliche Regelwerk](http://blogs.magicjudges.org/rules/cr/) und die Magic-[Turnierregeln](http://blogs.magicjudges.org/rules/mtr/). Spieler sind verpflichtet, einen Judge zu rufen, wenn sie einen Fehler machen. Da beide Spieler für den Spielzustand verantwortlich sind, wird von Spielern außerdem erwartet, dass sie einen Judge rufen, wenn sie einen Verstoß ihres Gegners bemerken._
> 
> _Der IPG legt fest, dass Spieler in Team Turnieren auch auf Verstöße in den Matches ihrer Teamkameraden aufmerksam machen müssen. Der Grund ist, dass jedes Team als Gruppe am Turnier teilnimmt und Preise gewinnt. Daher sind Mitglieder eines Teams dafür verantwortlich, Fehler sowohl in ihren eigenen Matches als auch denen ihrer Teamkameraden zu melden.  
> _
> 
> 

Damit ein Vergehen als Cheating betrachtet wird, müssen die folgenden Kriterien erfüllt sein:

*   Der Spieler muss versuchen, aus seiner Handlung einen Vorteil zu erlangen.
*   Dem Spieler muss bewusst sein, dass er etwas Illegales tut.

> _In Magic ist Betrug immer eine vorsätzliche Handlung oder das bewusste Unterlassen einer Handlung. „Versehentliches Betrügen“ gibt es nicht. Unbeabsichtigte Fehler sollten durch die jeweiligen Vergehen in den anderen Abschnitten des IPG geahndet werden._
> 
> _Außerdem muss der Spieler versuchen, einen Vorteil aus seinen Handlungen zu ziehen, damit der Regelverstoß als USC – Cheating gilt. Dieser Punkt wirkt auf den ersten Blick merkwürdig, da der erste Gedanke ist, dass ein Spieler immer versucht einen Vorteil zu erlangen, wenn er betrügt, und genau darum geht es hier. Gibt es keinen Vorteil zu erlangen, gibt es kein USC – Cheating. Ein Beispiel: Abe hat 6 Lebenspunkte und greift mit einer Kreatur an, die er diesen Zug gewirkt hat. Ned bemerkt das und entschließt sich, den Schaden einfach zu nehmen, da er 2 Blitzschläge auf der Hand hat und Abe noch einen Angriff geben möchte, bevor Ned ihn in seinem Versorgungssegment tötet. In diesem Szenario begeht Ned keinen Regelverstoß. Beachte jedoch, dass diese Regel für Spieler zugänglich ist; einige könnten auf den Gedanken kommen, ihre Geschichten so darzustellen, dass sie einfach nur als „freundlicher Typ“ erscheinen. Es ist deine Aufgabe, so viel von der Wahrheit wie möglich in Erfahrung zu bringen._
> 
> _Ein dritter Punkt, der nicht auf der Liste steht, aber in der Definition erwähnt wird, ist, dass der Spieler tatsächlich eine Regel brechen muss. Missversteht ein Spieler eine Regel und denkt, er tue etwas Illegales um einen Vorteil zu erlangen, seine Handlung aber tatsächlich erlaubt ist, liegt kein USC – Cheating vor._
> 
> _Früher bildete USC – Cheating eine eigene Kategorie von Vergehen, darunter Cheating – Fraud, Cheating – Hidden Information Violation und Cheating – Manipulation of Game Materials. Im Januar 2013 wurde der IPG aktualisiert und diese Vergehen wurden zu einem gemeinsamen Vergehen zusammengefasst: Unsporting Conduct – Cheating. Judges fällt es dadurch leichter, festzustellen, ob eine bestimmte Handlung USC – Cheating ist, da die Kriterien für alle Arten von Betrug gleich sind._
> 
> 

Falls nicht alle diese Kriterien erfüllt sind, handelt es sich bei dem Vergehen nicht um Cheating, und wird durch einen anderen Regelverstoß behandelt.

> _Der IPG stellt klar, welche Bedingungen erfüllt sein müssen, damit ein Verstoß USC – Cheating ist, damit Judges leichter Situation identifizieren können, in denen USC – Cheating tatsächlich vorliegt. Sind eine oder mehrere Bedingungen nicht erfüllt, sollte der Judge erwägen, ob ein anderes Vergehen anwendbar ist. Auf keinen Fall sollte eine eigene Definition verwendet werden.  
> _

Cheating sieht oftmals zunächst wie ein Game Play Error oder ein Tournament Error aus, und muss zunächst vom Judge untersucht werden, inwieweit Vorsatz vorliegt und die entsprechenden Regeln bekannt sind.

> _Die Fähigkeit, Untersuchungen durchzuführen, wird in der Judge Gemeinschaft hochgeschätzt; sie ist eine der Qualitäten von REGIONAL Judges, die sich alle Judges zu eigen machen sollten. Der IPG verlangt keine handfesten Beweise, dass ein Spieler die Absicht hatte zu betrügen; stattdessen erwartet er, dass Turnier-Offizielle ihre eigene Urteilsfähigkeit benutzen, um festzustellen, ob ein Spieler absichtlich eine Regel verletzt hat, um sich so einen Vorteil zu verschaffen. Dieser Satz dient als Erinnerung, immer wachsam zu sein und Fragen zu stellen. Diese Fähigkeit im Besonderen ist schwierig zu trainieren, da jede Situation einzigartig ist._
> 
> _Unter [diesem Artikel](http://wiki.magicjudges.org/en/w/Basics_of_Investigations) oder [diesem Podcast](http://judgecast.com/?p=408) findet sich Grundlagenmaterial. Beachte, dass diese Quellen entstanden sind, bevor die Cheating Vergehen zusammengelegt wurden.  
> _
> 
> 

Beispiele
---------

*   **A.** Eine Spielerin ändert unwissentlich von ihrer Gegnerin ein Matchergebnis auf einem Ergebniszettel.
*   **B.** Ein Spieler belügt einen Turnier-Offiziellen darüber, was in einer Partie passiert ist, um die Angelegenheit als für ihn vorteilhaft darzustellen.
*   **C.** Eine Spielerin lässt ihre Gegnerin eine Kreatur auf ihren Friedhof legen, obwohl ihr kein tödlicher Schaden zugefügt wurde.
*   **D.** Ein Spieler bemerkt, dass sein Gegner nur die eine Hälfte der ausgelösten Fähigkeit von seinem Schwert aus Schmaus und Hunger verrechnet hat, und macht auf den Fehler nicht aufmerksam.
*   **E.** Eine Spielerin schaut beim Draften, welche Karten ihre Nachbarn nehmen.
*   **F.** Ein Spieler fügt zusätzliche Karten zu seinem Sealed-Kartenpool hinzu.
*   **G.** Eine Spielerin bemerkt, dass sie eine zusätzliche Karte gezogen hat, ruft aber keinen Judge um einen Penalty zu vermeiden.

> _Dies sind alles Beispiele für einen Spieler, der absichtlich die Spielregeln oder Magic-Turnierregeln bricht, oder lügt. Die Liste ist nicht vollständig, aber sie deckt die häufigsten Fälle ab. Eine Sache, die durch ihre Abwesenheit hervorsticht, ist das Verschweigen eines [Missed Trigger](http://blogs.magicjudges.org/rules/ipg2-1-de/) des Gegners. Dies ist **niemals** USC – Cheating, selbst wenn der Spieler zu einem Zeitpunkt auf die ausgelöste Fähigkeit aufmerksam macht, der für ihn von Vorteil ist._