Bei Turnieren sind Judges die unparteiische Instanz, welche die Regeln und die zu Grunde liegenden Prinzipien durchsetzt.

> _Dies ist wahrscheinlich das wichtigste Konzept dieses Dokuments – Judges müssen neutral sein. Es sollte undenkbar sein, dass ein Judge zugunsten eines Spielers entscheidet, weil er mit diesem befreundet ist oder diesen mehr mag als den anderen. Unparteiisch zu sein ist ein fundamentales Konzept des [Magic-Judge Verhaltenskodex](http://blogs.magicjudges.org/conduct/magic-judge-code/). Judges werden vor allem respektvoll behandelt, weil sie neutral sind und die Prinzipien gerecht durchsetzen.  
> _

In laufende Partien sollten Judges nicht eingreifen, es sei denn sie glauben, ein Regelverstoß ist passiert, ein Spieler hat ein Anliegen oder eine Frage, oder der Judge möchte verhindern, dass eine Situation eskaliert.

> _Judges sind für die Spieler da. Unsere Dienste werden benötigt, wenn gegen eine Regel verstoßen wird, ein Spieler ein Anliegen hat oder eine heikle Situation, wie ein Streit, die Beruhigung der Spieler verlangt. Wenn die Hilfe eines Judges nicht benötigt wird, sollte dieser nicht in das Match eingreifen. Das heißt: keine Kommentare zu Spielaktionen, kein Risiko eingehen jemandem zu helfen und nicht die Konzentration der Spieler unterbrechen. Lass die Spieler spielen. Das heißt allerdings nicht, dass du ein Roboter sein sollst. Du kannst dich immer noch mit den Spielern unterhalten und mit ihnen Witze reißen; nur unterbrich ihre Partien nicht.  
> _

Insbesondere verhindern Judges keine Spielfehler, sondern behandeln Spielfehler, nachdem sie passiert sind und bestrafen jene, die gegen Regeln oder Prinzipien verstoßen. Außerdem fördern sie Fair Play und Sportsgeist durch gutes Beispiel und Diplomatie.

> _Wie in vielen anderen Sportarten verhindern Judges keine Fehler verhindern. Sobald aber ein Spielregelverstoß passiert, schreiten Judges und wenden die nötigen Korrekturen und Penaltys. Spieler können sich nicht darauf verlassen, dass ein Judge sie von einer illegalen Aktion abhält, weil Judges die Zukunft nicht voraussehen können und Spielaktionen zu häufig ausgeführt werden. In den meisten Fällen stellt die Korrektur eines Spielregelverstoßes den korrekten Spielfluss wieder her. Dieses Prinzip gilt auch beim Beobachten einer Partie am Ende der Runde oder während den Top 8._
> 
> _Außerdem ist es wichtig, dass Judges mit ihrem Verhalten ein gutes Beispiel geben. Deine Einstellung und Handlungen haben einen ausgesprochenen Einfluss auf die Stimmung der Veranstaltung. Leute sollen in dir das Verhalten sehen, das auch du auf deinen Veranstaltungen willst._
> 
> 

Ein Judge darf einschreiten, um Fehler außerhalb von Partien zu verhindern oder ihnen vorzubeugen.

> _Obwohl es nahezu unmöglich ist, einen Spielregelverstoß vorauszusehen, ist es manchmal möglich, einen Fehler außerhalb der Partie zu erkennen, der gleich geschehen wird. In diesen Fällen sollten Judges einschreiten und Verstöße am Eintreten hindern. Das „darf“ in diesem Satz heißt nicht, „dass der Judge darüber entscheiden darf“, sondern, „dass der Leitfaden zur Behandlung von Regelverstößen/die Magic-Turnierregeln Judges erlauben einzuschreiten“. Dies unterstreicht die Wichtigkeit des Kundendienstes und macht deutlich, dass Judges keine Wahl haben, ob sie eingreifen und diese Art Fehler verhindern sollen. Außerhalb einer Partie sollen Judges immer eingreifen um Verstöße zu verhindern, allerdings ist es entschuldbar, dass sie nicht jeden Verstoß, der kurz vor dem Eintreten ist, erkennen._
> 
> _Hier sind einige Beispiele:_
> 
> *   _Ein Judge sieht, dass ein Spieler sein Deck nach Ende der Partie mischt und auf dem Tisch eine vorher ins Exil geschickte Kreatur liegt, und realisiert, dass der Spieler vergessen hat, diese in sein Deck zu mischen; der Judge schreitet ein, um den Spieler auf die vergessene Karte hinzuweisen._
> *   _In einem Sealed Deck Turnier gibt eine Spielerin dem Judge eine Deckliste und der Judge bemerkt, dass die Spielerin vergessen hat, ihre Standardländer aufzuschreiben; der Judge fordert die Spielerin auf, die Standardländer zu notieren._
> *   _Kurz vor Beginn der Runde geht eine Spielerin zur Judge Station und gibt eine Karte (wie [Pazifismus](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Pazifismus&width=223&height=310)) ab, die ihrem vorherigen Gegner gehört; der Judge bemüht sich herauszufinden, wo der Besitzer dieser Karte in dieser Runde spielt, damit er ihm die Karte zurückgeben kann, bevor die Partie mit einem illegalen Deck begonnen wird._
> *   _Vor dem Turnier sieht ein Judge eine Karte mit einem fragwürdig modifizierten Bild. Der Judge erinnert den Spieler daran, dass der [Head Judge](http://blogs.magicjudges.org/rules/mtr1-7/) alle künstlerisch veränderten Karten vor dem Turnier genehmigen muss._
> 
> 

Kenntnisse über die Vergangenheit eines Spielers oder über sein Können haben keinen Einfluss auf den Verstoß, können aber bei einer etwaigen Untersuchung berücksichtigt werden.

> _Wir ändern den Verstoß nicht aufgrund des scheinbaren Könnens des Spielers. Ein GRV ist ein Warning, egal ob der Spieler ein Neuling oder ein abgehärteter Profi ist. Hast du die Art des Verstoßes bestimmt, wendest du die Penaltys ohne Vorurteile an. Auf den GRV eines Spielers, der den Ruf hat, fragwürdig zu spielen, werden die gleichen Maßnahmen angewendet wie auf den GRV eines L3 Judge, der im selben Turnier spielt. Ist der Verstoß erkannt worden, spielt die Identität des Spielers keine Rolle. Allerdings kann die Vergangenheit des Spielers einen Einfluss auf die Untersuchung haben und damit auf die Beurteilung darüber, welcher Verstoß begangen wurde. Zum Beispiel ist ein neuer Spieler, der die Funktionsweise von Trampelschaden nicht versteht, glaubwürdiger als ein erfahrener Spieler, mit dem du Trampelschaden schon häufig erörtert hast. Es ist immer noch möglich, dass ein ehrlicher Fehler gemacht wurde, aber die gestellten Fragen während der Untersuchung werden von dieser Kenntnis beeinflusst._  
> 

Der Zweck eines Penaltys ist es, den Spieler aufzuklären, damit er ähnliche Fehler in Zukunft nicht mehr macht.

> _Penaltys existieren nicht, um sadistischen Judges die Fähigkeit zu geben, wehrlosen Spielern Schaden zuzufügen. Penaltys existieren, um die Wahrscheinlichkeit zu reduzieren, dass der Fehler sich wiederholt. Ein Spieler, der für eine Handlung einen Penalty erhält, wird diesen Fehler in Zukunft mit größerer Wahrscheinlichkeit nicht mehr machen. Grundsätzlich sollen Penaltys etwas Spürbares sein, um die Lektion zu unterstreichen: „Ich habe diese Partie nur wegen dieses Fehlers verloren und ich will nicht noch eine Partie wegen eines leicht vermeidbaren Fehlers verlieren. Ich werde beim Aufschreiben der Deckliste nun jedes Mal auf 60 zählen.“ Das primäre Ziel eines Penalty ist nicht das Verfolgen; auch wenn dies ein praktisches und nützliches Nebenprodukt ist, ist das primäre Ziel die Erziehung._  
> 

Dies geschieht durch eine Erläuterung, auf welche Weise gegen die Regeln oder Prinzipien verstoßen wurde, und einem Penalty, um der Belehrung Nachdruck zu verleihen.

> _In diesem Satz wird die Wichtigkeit der Erziehung betont. Ein Penalty allein kann dies nicht erreichen. Du musst auch (kurz) erklären, was der Spieler falsch gemacht hat. Sonst wird er vielleicht nicht vollständig verstehen, was falsch gelaufen ist._  
> 

Penaltys dienen außerdem zur Abschreckung und Aufklärung aller anderen Spieler des Turniers und werden dazu verwendet, das Verhalten eines Spielers über einen langen Zeitraum hinweg im Auge zu behalten.

> _Es sollte angemerkt werden, dass man nicht selber einen Penalty erhalten muss, umzu erkennen, dass man keinen weiteren will. Hat ein Freund eine wichtige Partie wegen eines Penalty verloren, wollen wir sicher nicht gleich aus dem gleichen Grunde verlieren. Wir lernen sowohl aus den Fehlern unserer Freunde als auch aus unseren eigenen._
> 
> _Es gibt ein (privates) Archiv aller Penaltys, so wie es auch ein (öffentliches) Archiv mit allen Match-Resultaten gibt. Dieses Archiv wird nützlich, wenn ein Spieler häufig den gleichen Verstoß begeht. Wenn ein Spieler 20 Mal ein Warning für „Extrakarten ansehen – Er hat eine Karte des Gegners beim Mischen des Decks vor Beginn des Matchs aufgedeckt“ in 20 aufeinander folgenden Turnieren erhält, muss man annehmen, dass er es absichtlich macht und er es nur einmal pro Turnier tut, weil er weiß, dass „das erste Mal nur ein Warning ist“._  
> 
> 
> 

Wenn ein geringfügiger Regelverstoß von den Spielern in gegenseitigem Einverständnis korrigiert wird, braucht der Judge nicht einzugreifen.

> _Judges sollten als Vorteil für die Spieler gesehen werden und es gibt viele geringfügige/kleine Fehler, die Spieler im Lauf eines Matches machen und die sie selber ohne die Hilfe eines Judges korrigieren. Ist der Fehler geringfügig und die Spieler korrigieren ihn selber und sind beide glücklich, braucht der Judge sich nicht in ihre Partie einzubringen._

Wenn die Spieler auf eine Art und Weise spielen, die für beide eindeutig ist, aber einen Beobachter verwirren könnte, sollten die Judges die Spieler anweisen die Situation klarzustellen. Dies gilt aber nicht als Regelverstoß und zieht keinen Penalty nach sich.

> _Spieler verwenden gewisse Abkürzungen oder nutzen Perlen um komische Dinge darzustellen oder verwenden die falschen Spielsteine für Kreaturen. Diese Dinge sind den Spielern vielleicht klar, wirken aber auf Zuschauer (einschließlich Judges) unklar. Verstehen die Spieler, was vor sich geht und ist alles in Ordnung, vergebe keinen Penalty. Bitte sie nur darum, auf eine eindeutige Art und Weise zu spielen. Häufig kommen Zuschauer mit Problemen zu uns, die gar keine Probleme sind. Aus diesem Grund müssen wir die Spieler dazu drängen auf eine Art und Weise zu spielen, die nicht nur für sie eindeutig ist, sondern auch für die Zuschauer._

In diesen beiden Fällen sollte der Judge dafür sorgen, dass die Partie normal fortgeführt wird.

> _Korrigieren die Spieler einen geringfügigen Fehler selber oder spielen sie auf eine Art und Weise, die nur für sie eindeutig ist, aber nicht für Außenstehende, bleibe in der Nähe, beobachte und stelle sicher, dass nichts Komisches vor sich geht, wie zum Beispiel das Ausnutzen der entstandenen Verwirrung durch einen Spieler oder die Verschlimmerung des Fehlers._

Schwerwiegendere Vergehen werden behandelt, indem zunächst der Regelverstoß ermittelt wird, um dann mit den entsprechenden Maßnahmen fortzufahren.

> _Dieser Satz gilt für zweierlei. Es ist eine Erinnerung daran, dass nicht jedes Vergehen geringfügig ist und dass wir nicht mit dem Penalty beginnen und daraus den Verstoß ableiten, sondern umgekehrt. Wir finden heraus, welche Handlungen ausgeführt wurden, welches Vergehen vorliegt und bestimmen dann den Penalty. Wir vergeben keine Game Losses, weil der Fehler den Wert eines Game Loss zu haben scheint._

Nur der Head Judge ist autorisiert Penaltys zu vergeben, die von diesem Leitfaden abweichen.

> _Bei mehreren Judges ist der Head Judge der Judge, der für das ganze Turnier zuständig ist. Er ist der Einzige, der die Autorität hat zu entscheiden, ob ein gewisser Penalty auf die vorliegende Situation nicht gut anwendbar ist. Head Judges sind von den verfügbaren Judges normalerweise diejenigen mit der meisten Erfahrung und wenn sie von den Prinzipien abweichen, dann ist dies normalerweise aus einem guten Grund._  
> 

Der Head Judge darf von den in diesem Dokument beschriebenen Vorgehensweisen nicht abweichen, außer unter erheblichen und außerordentlichen Umständen, oder wenn auf eine Situation keine der in diesem Leitfaden aufgeführten Philosophien als Leitlinie anwendbar sind.

> _Auch wenn der Head Judge die Autorität hat vom Leitfaden abzuweichen, wird von ihm natürlich erwartet zu wissen, wann es angemessen ist abzuweichen. Der Hauptgrund für eine Abweichung ist, dass eine konkrete Situation in keine der in den 20 Seiten des Leitfadens zur Behandlung von Regelverstößen aufgelisteten Kategorien passt. Der einzige Fall, der eine Abweichung rechtfertigt, ist, wenn die Situation erheblich und außerordentlich ist._  
> 

Erhebliche und außerordentliche Umstände sind selten – ein Tisch stürzt um, eine Booster-Packung enthält Karten aus einer falschen Erweiterung, usw.

> _Hier siehst du einige Beispiele für „außerordentliche Umstände“. In diesen Fällen benutzen wir unseren gesunden Menschenverstand und versuchen zusammen mit den Spielern die „beste Lösung“ zu finden. Beide oben genannten Beispiele sind erheblich und außerordentlich. Stelle sicher, dass deine Situation beides ist, bevor du eine Abweichung erwägst._

Der Rules Enforcement Level, die aktuelle Runde des Turniers, das Alter oder die Erfahrung eines Spielers, der Wunsch, einen Spieler zu belehren sowie der Level des Judges sind **KEINE** außerordentlichen Umstände.

> _Einige dieser Umstände lassen es erscheinen, als wäre eine Abweichung in Ordnung, wenn sie dies jedoch nicht ist. Du musst die Prinzipien durchsetzen, egal, ob es die letzte Runde ist, oder ob der Call am Tisch mit der Nummer 1 oder der Nummer 101 ist. Der Gegner ist vielleicht außerordentlich jung, aber dieser Altersunterschied ist im Sinne der Prinzipien nicht erheblich. Der Spieler ist vielleicht neu und weiß nicht, dass einen Würfel zu werfen um den Gewinner zu bestimmen verboten ist; dieser Spieler wird trotzdem disqualifiziert werden, egal ob du denkst, dass es fair ist oder nicht. Auch ein Level 3 hat nicht automatisch das Recht abzuweichen. Tatsächlich müssen sich diese Judges strenger an die Prinzipien halten, da Judges mit einem niedrigeren Level sie beobachten und von ihren Handlungen lernen._

Sollte ein Judge das Gefühl haben, dass eine Abweichung angebracht wäre, dann muss er sich mit dem Head Judge absprechen.

> _Nur weil der Head Judge der Einzige ist, dem es erlaubt ist abzuweichen, heißt das nicht, dass ein Floor Judge dies dem Head Judge nicht vorschlagen kann. Allerdings darf man als Floor Judge nie abweichen._

Judges sind auch nur Menschen und machen Fehler. Wenn ein Judge einen Fehler macht, sollte er sich den Fehler eingestehen, sich bei den Spielern entschuldigen und, wenn es dafür nicht zu spät ist, den Fehler beheben.

> _Trotz aller Bemühungen, Golden Retrievers auszubilden, sind wir Judges auch nur Menschen._
> 
> _Bis jetzt._
> 
> _Menschen machen Fehler. Es ist eine Tatsache des Lebens. Niemand kann immer 100% richtigliegen und es ist unrealistisch das Gegenteil zu erwarten. Allerdings solltest du, wenn du einen Fehler machst, diesen zugeben und ihn, soweit möglich, beheben. Spieler sollen nicht weiterhin etwas für richtig halten, das ihnen ein Judge falsch gesagt hat. In jedem Fall solltest du dich bei den Spielern für deinen Fehler entschuldigen. Manchmal ist es besser dies unverzüglich zu tun, manchmal führt es zu weniger Unterbrechung, wenn du dies nach dem Match tust. Entschuldige dich aber so bald wie möglich bei beiden Spielern und korrigiere die Situation. Spieler sind typischerweise sehr verständnisvoll, auch wenn sie durch deinen Fehler benachteiligt wurden._
> 
> 

Falls ein Mitglied der Turnierleitung einem Spieler falsche Informationen gibt, die dazu führen, dass der Spieler einen Regelverstoß begeht, kann der Head Judge den Penalty herunterstufen.

> _Wir erwarten, dass Spieler der Turnierleitung vertrauen und Spieler sollten aufgrund der Informationen, die wir ihnen bereitstellen, handeln können. Es ist unfair sie dafür zu bestrafen, dass sie Leuten vertrauen, denen sie vertrauen können sollten. Allerdings kann nur der Head Judge über diese Herunterstufung entscheiden. Für diesen Tatbestand müssen zwei Voraussetzungen erfüllt sein: 1) der Judge hat falsche Informationen gegeben und 2) ein Vergehen wurde aufgrund dieser falschen Information begangen._

Fragt beispielsweise ein Spieler einen Judge, ob eine Karte in einem bestimmten Format legal ist und ist dann das Deck des Spielers wegen dieser irrtümlichen Auskunft illegal, bereinigt der Head Judge den Decklisten-Fehler wie sonst auch, aber er kann wegen des direkten Fehlers des Judges den Penalty von Game Loss auf Warning herunterstufen.

> _Andere Beispiele könnten folgendes beinhalten:_
> 
> *   _Herunterstufung eines GRV Warnings zu keinem Penalty, wenn ein Judge einem Spieler sagt, dass er einen gegnerischen Blitzschlag mit einem Schicksalspfad auf einen Planeswalker des Gegners umleiten kann._
> *   _Herunterstufung eines Game Losses wegen eines Deck/Decklisten Problems zu einem Warning, weil ein Judge dem Spieler zuvor gesagt hat, dass die Registrierung eines „Jace“ genügt._
> 
>