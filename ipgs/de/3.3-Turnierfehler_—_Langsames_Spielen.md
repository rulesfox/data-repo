Definition
----------

Ein Spieler braucht für die Durchführung von Spielaktionen länger als angemessen.

> 
> 
> **Penalty**
> 
> Warning
> 
> 

  

> _Spieler müssen in einem angemessenen Tempo spielen; was aber ist ein angemessenes Tempo? Es ist nirgends festgelegt. Es gibt keine explizite Sekundenzahl, innerhalb der eine Entscheidung getroffen werden muss. Sagen wir „mehr als 30 Sekunden für eine Entscheidung zu brauchen ist Tournament Error – Slow Play“, erlauben wir Spielern, sich für jede Entscheidung 29 Sekunden Zeit zu lassen, und haben damit [Stalling](http://blogs.magicjudges.org/rules/ipg4-7-de/) legalisiert. Slow Play Entscheidungen sind subjektiv; Judges müssen daher nach ihrem besten Ermessen entscheiden, ob Slow Play vorliegt. Zwei verbreitete Richtlinien sind:  
> – Hattest du Zeit, das Spielfeld zu analysieren, einen Spielzug zu identifizieren und fängst danach an, dich zu langweilen, ist es Slow Play.  
> – Falls du dich wunderst, ob du Slow Play geben solltest, hättest du es bereits geben sollen._

Wenn ein Judge glaubt, dass ein Spieler absichtlich langsam spielt, um das Zeitlimit zu seinem Vorteil auszunutzen, dann ist das Vergehen [Unsporting Conduct — Stalling](http://blogs.magicjudges.org/rules/ipg4-7-de/).

> _Spieler können langsam spielen, ohne es zu merken oder es vorsätzlich zu tun. Spielen sie langsam mit dem Ziel, Zeit aufzubrauchen (um einen Sieg zu sichern, ein Unentschieden zu erzwingen, oder aus einem anderen Grund), ist es nicht TE – Slow Play, sondern USC – Stalling (d. h. Cheating). Beachte, dass TE – Slow Play und USC – Stalling schwer voneinander zu unterscheiden sind._

Es ist auch Slow Play, falls ein Spieler eine Schleife immer wieder durchführt, ohne eine exakte Anzahl an Durchläufen sowie den danach erwarteten Spielzustand benennen zu können.

> _Über diesen Satz können ganze Artikel geschrieben werden. Die meisten Schleifen enden nach vorhersagbar vielen Durchgängen — ich tue X höchstens zehnmal und höre sofort auf, falls Y passiert. Laut den Regeln für Abkürzungen im Turnierspiel in den Magic-Turnierregeln musst du außerdem erklären können, welchen Zustand das Spiel nach Ende der Schleife hat. Es gibt viele Schleifen, die „irgendwann“ einen bestimmten Spielzustand erreichen. Irgendwann ist keine spezifische Zahl. Die Endzustände dieser Schleifen sind auch nicht spezifisch genug. Du kannst die Schleife „ich mühle mich solange, bis die letzten zwei Karten in meiner Bibliothek Emrakuls sind“ nicht ausführen, weil du nicht sagen kannst, wie lange du brauchen wirst, um diesen Zustand zu erreichen. Du könntest die Schleife „Eine Million Mal!“ durchlaufen, und es gäbe immer noch die Chance, dass es erst bei Durchgang 1.000.001 geschieht. „Dann eben zwei Millionen Mal!“ Es gibt immer noch keine Garantie dafür, dass es passiert. Es ist egal, wie klein die Wahrscheinlichkeit für einen Misserfolg ist; solange es sie gibt, kannst du die Schleife nicht ausführen._

Beispiele
---------

*   **A.** Ein Spieler schaut immer wieder durch den Friedhof des Gegners, ohne dass sich der Spielzustand wesentlich verändert hat.
*   **B.** Eine Spielerin notiert alle Karten im Deck des Gegners während sie [Ausbluten der Gedanken](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Ausbluten+der+Gedanken&width=223&height=310) verrechnet.
*   **C.** Ein Spieler beansprucht beim Mischen seines Decks zwischen zwei Partien übermäßig viel Zeits.
*   **D.** Eine Spielerin verlässt ihren Platz, um sich die Standings anzuschauen oder geht zur Toilette, ohne die Erlaubnis eines Offiziellen einzuholen.

> _Beispiele A, B und C sind alles Fälle von unnötigen und übertriebenen Aktionen, die das Spiel nicht voranbringen. In Beispiel D sollte die Spielerin um die Erlaubnis eines Judge bitten, bevor sie die Toilette aufsucht. Nach ihrer Rückkehr wird entsprechend viel Extrazeit gegeben. Du solltest einem Spieler aber immer erlauben, die Toilette aufzusuchen. Der Penalty wird nicht dafür vergeben, dass du auf die Toilette gegangen bist, sondern dass du deinen Gegner verlassen hast, ohne einem Judge Bescheid zu geben._

Philosophie
-----------

Alle Spieler sind dafür verantwortlich, so schnell zu spielen, dass ihr Gegner nicht aufgrund des Zeitlimits einen wesentlichen Nachteil erhält.

> _In einigen Sportarten ist das Ausnutzen des Zeitlimits eine legale Strategie. Bei Magic möchten wir, dass Matches durch tatsächliches Spiel entschieden werden, und nicht durch Zeitspiel. Zeit ist eine Ressource, die von beiden Spielern geteilt wird, und wir möchten nicht, dass ein Spieler mehr Zeit verbraucht, als angemessen ist, um seine Aktionen durchzuführen. Komplexe Kombos mögen mehr Zeit für ihre Aktionen benötigen, weil mehr Aktionen durchzuführen sind, aber verschwendete Zeit ist Zeit, die dem Gegner genommen wird. Es wird von beiden Spielern erwartet, dass sie in einem Tempo spielen, mit dem das Match innerhalb der vorgesehenen Zeit beendet wird._

Spieler können langsam spielen, ohne es zu bemerken. Eine Anweisung wie “Du musst schneller spielen” ist häufig angemessen und ausreichend.

> _Ein Spieler, der intensiv nachdenkt, ist sich vielleicht nicht bewusst, wie viel Zeit er verbraucht. Menschen sind schlecht darin, Zeiträume zu schätzen, und denken, dass nur zehn Sekunden vergangen sind, obwohl es in Wahrheit fünfzig gewesen sind. In diesen Fällen ist es okay, dem Spieler einen kleinen Schubs in Richtung schnelleren Spiels zu geben. Dieser Schubs ist kein formeller Penalty noch ein Warning, sondern eine „Ermahnung“. Trifft der Spieler schnell eine Entscheidung und fährt fort, in einem angemessenen Tempo zu spielen, ist alles in Ordnung. Einige Judges lassen diese Ermahnung aus. Auch das ist in Ordnung._

Spielt der Spieler weiterhin langsam, so sollte dies gehandet werden.

> _Wurde ein Spieler bereits ermahnt, eine Entscheidung zu treffen, und er trifft die Entscheidung nicht zügig, oder seine nächsten Entscheidungen sind auch langsam, ist der nächste Schritt, ein Warning zu geben. Wurde bereits ein Warning gegeben, wird das zweite Warning (in Übereinstimmung mit dem Head Judge) zu einem Game Loss heraufgestuft. Frage den Spieler daher, ob er im heutigen Turnier bereits ein Warning für TE – Slow Play erhalten hat, wenn du ein Warning gibst._

Zusätzliche Maßnahmen
---------------------

Für jeden Spieler wird ein zusätzlicher Extrazug vergeben, der ihnen zur Verfügung steht, falls das Match das Zeitlimit überschreitet.

> _Es gibt keinen Weg festzustellen, wie viel Zeit dadurch verloren gegangen ist, dass ein Spieler langsam gespielt hat. Anstatt Extrazeit zu geben, gibt es daher einen Extrazug für jeden Spieler, falls der Match das Zeitlimit überschreitet._

Diese Extrazüge finden statt, nachdem eine allenfalls erhaltene Extrazeit abgelaufen ist und vor den Prozeduren am Ende des Matches.

> _Irgendwann müssen diese Züge stattfinden._

Die Spieler erhalten keine Extrazüge, wenn das Match bereits in den Extrazügen ist. Das Warning wird dennoch vergeben.

> _Erreicht eine Partie das Rundenende und die Spieler befinden sich in den Extrazügen der Prozeduren am Ende des Matches, gibt es keine Extrazüge wegen TE – Slow Play, da die Uhr nicht mehr läuft._
> 
> _Für mehr Informationen zu Slow Play, siehe [diesen Artikel](http://blogs.magicjudges.org/articles/2012/12/25/slow-play/)._
> 
>