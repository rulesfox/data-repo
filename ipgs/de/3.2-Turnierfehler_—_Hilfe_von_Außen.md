Definition
----------

Ein Spieler, Zuschauer oder anderer Turnierteilnehmer begeht eine der folgenden Handlungen:

> 
> 
> **Penalty**
> 
> Match Loss
> 
> 

  

*   Er versucht von Dritten Spieltipps oder verdeckte Information über sein Match zu erlangen, nachdem er Platz genommen hat, um sein Match zu spielen.  
    > _Sobald ein Spieler Platz genommen hat, um sein Match zu spielen, darf er von anderen keine Anweisungen oder Informationen über sein Match erhalten. Belangloses Geplauder ist nicht verboten, sofern das Gespräch keine Informationen enthält, die dem Spieler in seinem Match helfen könnten. Dieser Verstoß beinhaltet keine Ratschläge am Paarungsaushang, am Stand eines Händlers oder draußen während einer Raucherpause. Spieler reden nun mal über Decks, gegen die sie gespielt haben oder erwarten zu spielen. Wir können auch nicht kontrollieren, worüber sie auf der Toilette reden._
*   Er gibt anderen Spielern Spieltipps oder verdeckte Informationen, nachdem diese Platz genommen haben, um ihr Match zu spielen.  
    > _Dieses Szenario passiert häufig einem Zuschauer oder Spieler, der einem Freund beim Spielen zuguckt, nachdem er sein eigenes Match beendet hat. Besonders bei Spielern, die noch am Turnier teilnehmen, sollte dieses Verhalten bestraft und das Match Loss gegeben werden. Denke daran, dass wir über „Spieltipps“ reden. „Vergiss nicht anzugreifen“ ist ein Spieltipp; „Gib auf, dann können wir essen gehen“ ist kein Spieltipp – auch wenn beides dazu führen kann, dass die Partie endet._
*   Er benutzt während der Partie Aufzeichnungen, die vor Beginn des aktuellen Matches angefertigt wurden (Ausnahme: ausgedruckte Oracle-Seiten).  
    > _Aufzeichnungen können verschiedene Formen annehmen, sind aber meist auf Papier. Zwischen zwei Partien dürfen diese Aufzeichnungen benutzt werden, aber nicht während einer Partie. Die Ausnahme bilden offizielle Oracle-Seiten, die zu jedem Zeitpunkt während eines Matchs abgerufen werden können. Üblicherweise rufen Spieler dafür einen Judge, aber falls ein Spieler Ausdrucke der Karten hat, kann er diese benutzen, solange er sich beeilt._

Diese Kriterien gelten auch während des Drafts und der Deckbau-Zeit bei Limited-Turnieren. Außerdem dürfen während eines Drafts keinerlei Notizen gemacht werden. Einige Teamformate haben zusätzliche Kommunikationsregeln die möglicherweise die Definition dieses Verstoßes beeinflussen.

> _Während eines Turniers in einem Limited-Format, entweder Sealed oder Draft, werden Spieler während des Deckbau-Zeit und des Drafts behandelt, als ob sie „in einem Match“ wären, und jegliches Verhalten, das die obige Definition erfüllt, wird als Tournament Error – Outside Assistance betrachtet. Beispiele sind Fragen, was man draften soll, wie viele Wälder man spielen sollte oder „Hast du Weiß gedraftet?“ In einigen Formaten wie Two-Headed Giant oder bei Teamturnieren dürfen Mitglieder eines Teams untereinander über Decks und Strategien sprechen, ohne, dass es als TE – Outside Assistance gilt._

Notizen, die außerhalb des laufenden Matches gemacht wurden, dürfen nur zwischen den Partien eines Matches genutzt werden, und auch nur dann, wenn sie sich seit Beginn des Matches im Besitz des Spielers befinden.

> _Diese Aufzeichnungen können zum Beispiel Sideboard-Strategien für unterschiedliche Matchups sein. Aufzeichnungen aus der ersten Partie können während der zweiten Partie benutzt werden. Der „Beginn des Matches“ ist die Grenze, weil es eine geben muss, und dieser Zeitpunkt am meisten Sinn ergibt. Spieler dürfen zwischen zwei Partien nicht online nach Sideboard-Strategien suchen._

Beispiele
---------

*   **A.** Während einer Partie benutzt ein Spieler Notizen, die er vor Turnierbeginn gemacht hat.
*   **B.** Eine Zuschauerin erläutert einem Spieler den stärksten Spielzug, obwohl dieser nicht um Hilfe gebeten hat.

Philosophie
-----------

Turniere sollen das Können eines Spielers auf die Probe stellen und nicht seine Fähigkeit, Hilfestellungen oder Anweisungen von außen zu folgen. Jeglicher strategischer Hinweis, Spieltipp oder Hinweis zum Deckbau von einem Außenstehenden wird als Outside Assistance gewertet.

> _Eine Partie findet zwischen zwei Spielern statt, und nicht zwischen einem Spieler und einem Spieler und dessen Kumpel, Google, oder der Twitter-Gemeinde. TE – Outside Assistance kann ein Wort sein, ein Stück Papier, oder eine Geste; es kann die Frage nach Informationen sein. Wenn es einem Spieler strategische, Spiel- oder Deckbauinformationen gibt, handelt es sich sehr wahrscheinlich um TE – Outside Assistance._
> 
> _Denke daran (und dies ist in einigen Kreisen umstritten), dass das Fragen nach oder das Bekanntgeben eines anderen Matchergebnisses nicht TE – Outside Assistance ist, selbst wenn diese Informationen benutzt werden, um die Partie zu beenden._
> 
> 

Visuelle Änderungen an Karten, einschließlich kurzer Text, welche geringe strategische Hilfe oder Tipps geben, sind akzeptabel und werden nicht als Notizen betrachtet. Detaillierte Anweisungen oder komplexe strategische Hinweise sind auf Karten nicht erlaubt. Es liegt im Ermessen des Head Judges, ob eine bestimmte Karte oder bestimmte Notizen für ein Turnier zulässig sind.

> _Einige Spieler schreiben Notizen auf Karten, die strategische Informationen beinhalten können. Einige Worte, eine Markierung oder ein Bild sind akzeptabel, aber ganze Sätze können die Grenze überschreiten. Beispiele sind Punkte auf Sideboard-Karten, ein „Greif mit mir an“ auf einem [Kugelblitz](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Kugelblitz&width=223&height=310) oder ein [Plundermagier](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Plundermagier&width=223&height=310), dessen Bild modifiziert wurde, sodass ein spezifisches Artefakt (das beabsichtigte Ziel) darauf abgebildet ist. Wende dich an den Head Judge, wenn du dir nicht sicher bist, ob eine bestimmte Modifikation zulässig ist._

Zuschauer, die sich dieses Vergehens schuldig machen, können vom Veranstaltungsort verwiesen werden, wenn sie nicht am Turnier teilnehmen.

> _Für Spieler, die noch am Turnier teilnehmen, kann TE – Outside Assistance das Damoklesschwert sein, dass sie davon abhält, Ratschläge zu geben oder von anderen einzuholen. Zuschauer hingegen müssen sich keine Sorgen über eine mögliche Bestrafung machen, da es für sie keinen „nächsten Match“ gibt, auf der das Match Loss angewandt werden kann. In diesem Fall sollten Judges die Zuschauer höflich bitten, den Veranstaltungsort zu verlassen; dies dient als Warnung an andere Zuschauer, die es nicht unterlassen können, über das beobachtete Match zu reden._
> 
> _Nicht am Turnier teilzunehmen schützt nicht vor diesem Verstoß. Begeht ein Zuschauer dieses Vergehen und nimmt er nicht am Turnier teil, wird er in das Turnier aufgenommen, sein Verstoß wird registriert, und anschließend wird der Zuschauer aus dem Turnier ausgeschieden._
> 
>