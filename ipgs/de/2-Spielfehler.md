Game Play Errors werden durch inkorrektes oder ungenaues Spielen verursacht, welches in einem Verstoß gegen das [ausführliche Magic-Regelwerk](http://blogs.magicjudges.org/rules/cr/) (Comprehensive Rules) resultiert.

> _Dies ist die erste von drei großen Kategorien von Vergehen. Diese Kategorie deckt unabsichtliche Verletzungen des Ausführlichen Regelwerks ab – Fehler, die tatsächlich während des Spiels geschehen, und nicht Verletzungen der Turnierprinzipien oder negatives Verhalten._
> 
> _Dies sind Fehler, die von mindestens einem Spieler während eines Matches durch das unbeabsichtigte Brechen einer Regel des Ausführlichen Regelwerks begangen werden. Game Play Errors (GPEs) können aus vielen Gründen passieren. Spieler werden müde, sind abgelenkt, spielen zu schnell oder kennen die Karten oder die Regeln, die bei einer komplexen Situation anwendbar sind, nicht gut genug. Diese Situationen sind nicht außergewöhnlich, daher sind GPEs häufige Fehler._
> 
> 

Es ist unmöglich, alle Vergehen aufzulisten, die sich dieser Kategorie zuordnen lassen.

> _Durch die Komplexität des Spieles ist es unmöglich alle Arten von Fehlern, die passieren könnten, aufzulisten, daher versuchen wir es als Judges gar nicht. Dieses Dokument soll verständlich und lehrreich sein. Würden wir alles auflisten und jeden Spezialfall erörtern, wäre dieses Dokument hundert Seiten lang und nutzlos. Stattdessen teilen wir diese Fehler in generelle Kategorien ein, bekannt als die 5 GPEs, definiert durch den Leitfaden zur Behandlung von Regelverstößen._

Der nachfolgende Leitfaden ist dazu da, den Judges einen Rahmen zu geben, wie Game Play Errors zu behandeln sind.

> _Auf den ersten Blick scheint es vielleicht schwierig zu erkennen, in welche Kategorie ein konkretes Vergehen fällt. Die aufmerksame Lektüre des ganzen Vergehens – Definition, Philosophie und Maßnahmen –  kann bei der Einordnung helfen._

Bei den meisten Game Play Errors wird davon ausgegangen, dass sie unabsichtlich begangen wurden.

> _Fehler, die absichtlich begangen werden, fallen manchmal in eine andere Kategorie: [Unsporting Conduct — Cheating](http://blogs.magicjudges.org/rules/ipg4-8-de/). Es ist allerdings wichtig anzumerken, dass nicht alle Game Play Errors Cheating sind. Tatsächlich sind dies sehr wenige._
> 
> _Wir nehmen gerne an, dass die Spieler nett sind und wenn wir zu einem Tisch kommen, wollen wir ihnen kein Cheating unterstellen. Dies kann sich nach einigen Fragen ändern, aber zu Beginn müssen wir annehmen, dass es sich um einen ehrlichen Fehler handelt._
> 
> 

Glaubt der Judge, dass der Verstoß absichtlich begangen wurde, sollte er zunächst überprüfen, ob es sich um [Unsporting Conduct — Cheating](http://blogs.magicjudges.org/rules/ipg4-8-de/) handelt.

> _Dies ist die andere Seite der Münze; unsere Aufgabe als Judges ist es den Spielern immer zu helfen, aber wir dürfen nie vergessen, dass sie lügen oder betrügen können um einen Vorteil zu erlangen. Die Erfahrung und der Rat eines erfahrenen Judges kann uns bei der Beurteilung der Situation helfen und bei der Untersuchung, ob der Spieler sich des Begehens eines Vergehens bewusst war oder nicht._

Mit der Ausnahme von [Failure to Maintain Game State](http://blogs.magicjudges.org/rules/ipg2-6-de/), welcher nie heraufgestuft wird, wird das dritte und jedes weitere Penalty für einen Game Play Error in derselben Kategorie zu einem Game Loss heraufgestuft. Bei mehrtägigen Turnieren wird die Anzahl der Penaltys dieser Verstöße jeden Tag zurückgesetzt.

> _Wir wollen den Spielern beibringen, aus ihren Fehlern zu lernen und in Zukunft achtsam zu sein, diese nicht wieder zu machen. Macht ein Spieler wiederholt Fehler, ist ein Warning nicht genug um die Lektion zu untermauern und daher muss dieses auf ein Game Loss heraufgestuft werden. Gibst du einem Spieler ein GPE, erkundige dich ob er dasselbe Vergehen nicht schon vorher begangen hat. Bei mehrtägigen Turnieren erscheint die Heraufstufung nach 3 Vergehen unfair, egal wie viele Runden das Turnier hat – es ist viel einfacher 3 GPEs innerhalb von 15 Runden auf einem GP anzuhäufen als innerhalb von 5 Runden auf einem GPT. Schließlich ist anzumerken, dass der dritte und darauf folgende Penalty, kein Warning ist. Hat ein Spieler 2 Game Rule Violations, die zu einem Game Loss heraufgestuft wurden und begeht er eine dritte Game Rule Violation, wird diese trotzdem auch zu einem Game Loss heraufgestuft._
> 
> _Sei dir betreffend des [Failure to Maintain Game State](http://blogs.magicjudges.org/rules/ipg2-6-de/) Vergehens bewusst, dass die Spieler nicht verstehen werden, wieso sie ein Warning erhalten. „Aber Judge, ich habe doch gar nichts falsch gemacht?” Nimm dir einige Sekunden um ihm zu erklären, wieso er ein Warning erhält und wünscht er weiter darüber zu sprechen, soll er nach dem Match zu dir kommen. Dieses Vergehen wird allerdings nicht wie andere Game Play Errors heraufgestuft.  Der Grund dafür ist, dass die Spieler es nicht fürchten sollen einen Judge zu rufen. Ein Game Loss zu erhalten, weil Gegner Game Play Errors machen und man es nicht gesehen hat, fühlt sich schlecht an. Und ist es das dritte Mal, dass mein Gegner innerhalb eines Turniers einen Game Play Error macht, den ich nicht gesehen habe, werde ich vielleicht widerwillig sein einen Judge zu rufen und meinen Failure to Maintain Game State heraufgestuft zu bekommen und werde deshalb so tun als hätte ich den Fehler nicht bemerkt. Unser Prinzip soll es nicht sein, Cheating zu ermutigen. Wenn wir diesen Penalty gar nicht heraufstufen, wieso geben wir dann überhaupt Penaltys? Dafür gibt es zwei Gründe: der erste Grund ist, dass meistens bereits ein Warning genug ist um die Spieler daran zu erinnern besser aufzupassen. Der zweite Grund ist, dass wir die Penaltys so verfolgen können. Bekommt ein Spieler häufig Failure to Maintain Game State und verschafft ihm der Fehler immer einen Vorteil, gibt das Warning dem Judge die Möglichkeit diese Vergehen nachzuverfolgen – und wird dieses der Datenbank hinzugefügt, können wir diese auch über Turniere hinweg verfolgen._
> 
>