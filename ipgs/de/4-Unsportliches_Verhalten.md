Unsportliches Verhalten ist störendes Verhalten, dass die Sicherheit, den Wettbewerbsgedanken, die Freude oder die Integrität eines Turniers auf deutlich negative Weise beeinflusst.

> _Dies ist eine allgemeine Definition dessen, was unter „Unsportliches Verhalten“ fällt, und sie ist sehr umfassend formuliert. Einige von euch denken jetzt vielleicht, dass sie viel mehr Dinge abdeckt, als ihr erwartet habt – denkt daran, dass alle diese Dinge für ein tolles Turniererlebnis wichtig sind. Als Judges müssen wir auf Dinge achten, die Spielern negative Erfahrungen bescheren können, und angemessen damit umgehen.  
> _

Unsportliches Verhalten ist nicht gleichzusetzen mit fehlendem Sportsgeist. Es gibt einen große Grauzone an “wettbewerbsorientiertem” Verhalten, dass sicherlich weder “nett” noch “sportlich” ist, aber dennoch nicht die Kriterien für “unsportlich” erfüllt.

> _Diese Unterscheidung ist wichtig. Falls ein Spieler dir gegenüber nicht höflich ist, heißt das noch nicht, dass er unsportlich ist. Du musst nicht „Gut gespielt“ nach einer erdrückenden Niederlage sagen; du musst keine Hände schütteln; dein Gegner muss dir nicht haargenau sagen, was eine Karte macht; usw. Nichts davon fällt unter Unsporting Conduct. Ein Spieler darf sein „Spielergesicht“ zeigen._

Der Head Judge hat die letzte Entscheidung darüber, was als unsportliches Verhalten zählt.

> _Ein Floor Judge kann entscheiden, einen Unsporting Conduct Penalty zu geben, aber der Spieler kann diese Entscheidung anfechten. Wie bei vielen anderen Abschnitten dieses Dokuments ist der Head Judge die höchste Autorität und entscheidet, ob etwas Unsporting Conduct ist. Es ist auch nötig, die Zustimmung des Head Judge einzuholen, bevor du einen Verstoß gibst, der als Penalty ein Game Loss oder höher hat, und im Falle einer Disqualifikation sollte der Head Judge den Penalty geben._

Die Judges sollten den Spieler darüber aufklären, auf welche Weise sein Verhalten störend wirkt. Vom Spieler wird erwartet, dass er sein Verhalten umgehend korrigiert. Auch wenn es wichtig ist sicherzustellen, dass ein Spieler die Schwere seines Vergehens versteht, sollten die Judges immer zuerst die Situation beruhigen und erst im Anschluss daran den Verstoß erklären und den Penalty vergeben.

> _Manchmal bemerken Spieler nicht, dass ihr Verhalten das Turnier stört. Sie sind mit ihren eigenen Sorgen beschäftigt; daher müssen die Judges sie darüber in Kenntnis setzen, dass ihr Verhalten Probleme verursacht. Nachdem er informiert wurde, sollte ein Spieler sein Verhalten augenblicklich korrigieren. Um eine Situation am Eskalieren zu hindern, sollten Judges aber zuerst versuchen, die Situation unter Kontrolle zu kriegen. Wenn du einen wütenden Spieler vor dir hast, machst du es wahrscheinlich nur noch schlimmer, wenn du ihm in diesem Moment einen Penalty gibst. Nachdem du die Situation unter Kontrolle hast, kannst du dir Sorgen um Verstöße machen._