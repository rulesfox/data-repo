Definition
----------

Ein Spieler lässt zu, dass ein anderer Spieler in der Partie einen [Game Play Error](http://blogs.magicjudges.org/rules/ipg2-de/) begeht und macht darauf nicht sofort aufmerksam.

> 
> 
> **Penalty**
> 
> Warning
> 
> 

  

> _Hier sind mehrere Dinge zu beachten. Das erste ist, dass ein Spieler, der einen Game Play Error begeht, dafür nie Failure to Maintain Game State (FTMGS) erhält. Der einzige Spieler, der FTMGS erhalten kann, ist der Gegner. Ein Spieler kann FTMGS erhalten, wenn er einen Fehler des Gegners nicht bemerkt. Beachte außerdem, dass ein Spieler, der sofort auf den gegnerischen Fehler hinweist, kein FTMGS begeht. Wirkt Abe zum Beispiel einen [Zorn Gottes](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Zorn+Gottes&width=223&height=310) für RRRW und Ned bemerkt dies, bevor andere Aktionen durchgeführt werden, erhält Ned kein FTMGS._

Glaubt ein Judge, dass ein Spieler absichtlich nicht auf unzulässige Aktionen seines Gegners aufmerksam macht, entweder um einen Vorteil zu erlangen Warning oder um den Fehler in einem strategisch günstigeren Moment anzusprechen, dann handelt es sich bei dem Verstoß stattdessen um [Unsporting Conduct — Cheating](http://blogs.magicjudges.org/rules/ipg4-8-de/).

> _Bei Cheating wird normalerweise an ein aktives und absichtliches Verstoßen gegen die Regeln zur Verschaffung eines Vorteils gedacht. Bemerkt ein Spieler einen Fehler des Gegners und ruft er keinen Judge, oder erst, wenn es zu seinem Vorteil ist, handelt es sich allerdings auch um Cheating. Abe kontrolliert einen [Juggernaut](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Juggernaut&width=223&height=310) und vergisst damit anzugreifen. Ned sagt nichts, da er den Schaden nicht nehmen will. Je nach Kenntnis der Regeln und der Verpflichtungen, die einen Spieler treffen, ist das Verhalten von Ned Cheating._

Einen Gegner nicht an dessen ausgelöste Fähigkeiten zu erinnern ist niemals Failure to Maintain Game State oder Cheating.

> _Dies ist eine wichtige Ausnahme der Regel „Du musst auf die gegnerischen Fehler hinweisen”. Missed Triggers werden anders behandelt. Einem Spieler ist es erlaubt, nicht auf die gegnerischen ausgelösten Fähigkeiten hinzuweisen. Da es erlaubt ist, kann es kein Regelverstoß sein. Schaue für mehr Informationen ins Kapitel zu den [Missed Triggers](http://blogs.magicjudges.org/rules/ipg2-1-de/)._

Beispiele
---------

*   **A.** Eine Spielerin vergisst, die Karte vorzuzeigen, die sie mit [Worldly Tutor](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Worldly+Tutor&width=223&height=310) (Irdischer Lehrmeister) gesucht hat. Der Fehler wird erst am Ende des Zuges bemerkt.
*   **B.** Ein Spieler bemerkt nicht, dass sein Gegner eine Kreatur mit Schutz vor Grün mit einem [Armadillo Cloak](https://blogs.magicjudges.org/rules/wp-content/plugins/lems-mtg-helper/lems-mtg-helper-cardfinder.php?find=Armadillo+Cloak&width=223&height=310) (Mantel des Gürteltiers) verzaubert hat.

> _In beiden Fällen hat der Gegner einen [Game Rule Violation](http://blogs.magicjudges.org/rules/ipg2-5-de/) begangen und der Spieler hat dies nicht sofort bemerkt._

Philosophie
-----------

Wenn ein Fehler bemerkt wird, bevor einer der Spieler dadurch einen Vorteil erlangen könnte, reduziert das die Wahrscheinlichkeit, dass der Spielzustand nachhaltig korrumpiert wird.

> _Hier wird nur statuiert, dass die Wahrscheinlichkeit, dass ein Fehler zu einem größeren Problem wird, umso geringer ist, je früher er erkannt wird. Dies sollte eine ziemlich einleuchtende Aussage sein._

Falls der Fehler nicht korrigiert wird, so liegt zumindest ein Teil der Schuld dafür auch beim Gegner, der diesen ebenfalls nicht bemerkt hat.

> _Die Kernaussage ist hier, dass beide Spieler für den Spielzustand verantwortlich sind. Ist ein Spieler aufmerksam, kann er die Fehler seines Gegners erkennen, bevor die Folgen zu erheblich sind. Es ist egal, wer dadurch einen Vorteil erlangt hat. Bedenke, dass es Situationen gibt, in denen der Gegner eine Fähigkeit kontrolliert, aber der Spieler die Aktion durchführen muss. Wird diese durch den Spieler falsch ausgeführt, handelt es sich um einen Fall, der im  Abschnitt [Game Rule Violation](http://blogs.magicjudges.org/rules/ipg2-5-de/) beschrieben wird. Hier haben beide Spieler den gleichen Regelverstoß begangen._